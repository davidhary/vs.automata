﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Automata.Finite.Engines.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Automata.Finite.Engines.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Automata.Finite.Engines.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
