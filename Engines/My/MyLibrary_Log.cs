﻿using System;
using System.Diagnostics;
using isr.Automata.Finite.Engines.ExceptionExtensions;
using isr.Core;

namespace isr.Automata.Finite.Engines.My

{
    public sealed partial class MyLibrary
    {

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public static void LogUnpublishedException(string activity, Exception exception)
        {
            LogUnpublishedMessage(new TraceMessage(TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}"));
        }

        /// <summary> Applies the given value. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        public static void Apply(Logger value)
        {
            _Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        public static void ApplyTraceLogLevel(TraceEventType value)
        {
            TraceLevel = value;
            Logger.ApplyTraceLevel(value);
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public static void ApplyTraceLogLevel()
        {
            ApplyTraceLogLevel(TraceLevel);
        }
    }
}