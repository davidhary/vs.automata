
namespace isr.Automata.Finite.Engines.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = 513;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Finite Automata Engines Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Finite Automata Engines Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Automata.Finite.Engines";

        /// <summary> Name of the test assembly strong. </summary>
        public const string TestAssemblyStrongName = "isr.Automata.FiniteTests,PublicKey=" + isr.Automata.Finite.My.SolutionInfo.PublicKey;
    }
}
