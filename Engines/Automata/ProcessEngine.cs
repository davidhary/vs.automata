using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a Process. </summary>
    /// <remarks>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-08-20 </para>
    /// </remarks>
    public class ProcessEngine : EngineBase<ProcessState, ProcessTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public ProcessEngine( string name ) : base( name, new StateMachine<ProcessState, ProcessTrigger>( ProcessState.Initial ) )
        {
            StateMachine<ProcessState, ProcessTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( ProcessState.Initial );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Initial );
            _ = stateConfiguration.Permit( ProcessTrigger.Prime, ProcessState.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Ready, ProcessState.Ready );
            _ = stateConfiguration.Ignore( ProcessTrigger.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Fail, ProcessState.Failed );
            _ = stateConfiguration.Ignore( ProcessTrigger.Complete );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Prime );
            this.EngagedStates.Add( ProcessState.Prime );
            stateConfiguration = this.StateMachine.Configure( ProcessState.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Initial, ProcessState.Initial );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Ready, ProcessState.Ready );
            _ = stateConfiguration.Ignore( ProcessTrigger.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Fail, ProcessState.Failed );
            _ = stateConfiguration.Permit( ProcessTrigger.Complete, ProcessState.Completed );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Ready );
            this.EngagedStates.Add( ProcessState.Ready );
            stateConfiguration = this.StateMachine.Configure( ProcessState.Ready );
            _ = stateConfiguration.Permit( ProcessTrigger.Initial, ProcessState.Initial );
            _ = stateConfiguration.Ignore( ProcessTrigger.Prime );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Ready );
            _ = stateConfiguration.Permit( ProcessTrigger.Busy, ProcessState.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Fail, ProcessState.Failed );
            _ = stateConfiguration.Permit( ProcessTrigger.Complete, ProcessState.Completed );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Busy );
            this.EngagedStates.Add( ProcessState.Busy );
            stateConfiguration = this.StateMachine.Configure( ProcessState.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Initial, ProcessState.Initial );
            _ = stateConfiguration.Ignore( ProcessTrigger.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Ready, ProcessState.Ready );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Fail, ProcessState.Failed );
            _ = stateConfiguration.Permit( ProcessTrigger.Complete, ProcessState.Completed );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Completed );
            stateConfiguration = this.StateMachine.Configure( ProcessState.Failed );
            _ = stateConfiguration.Permit( ProcessTrigger.Initial, ProcessState.Initial );
            _ = stateConfiguration.Ignore( ProcessTrigger.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Ready, ProcessState.Ready );
            _ = stateConfiguration.Permit( ProcessTrigger.Busy, ProcessState.Busy );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Fail );
            _ = stateConfiguration.Permit( ProcessTrigger.Complete, ProcessState.Completed );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Initial );
            stateConfiguration = this.StateMachine.Configure( ProcessState.Completed );
            _ = stateConfiguration.Permit( ProcessTrigger.Initial, ProcessState.Initial );
            _ = stateConfiguration.Permit( ProcessTrigger.Prime, ProcessState.Prime );
            _ = stateConfiguration.Permit( ProcessTrigger.Ready, ProcessState.Ready );
            _ = stateConfiguration.Permit( ProcessTrigger.Busy, ProcessState.Busy );
            _ = stateConfiguration.Permit( ProcessTrigger.Fail, ProcessState.Failed );
            _ = stateConfiguration.PermitReentry( ProcessTrigger.Complete );
            _ = stateConfiguration.Permit( ProcessTrigger.Proceed, ProcessState.Initial );
        }

        #endregion

        #region " PROGRESS "

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) ProcessState.Completed);

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> The percent progress. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get => this._PercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.PercentProgress )
                {
                    this._PercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the percent progress. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UpdatePercentProgress()
        {
            if ( this.CurrentState != ProcessState.Initial )
            {
                this.PercentProgress = this.StateProgress;
            }
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void ProcessStopRequest()
        {
            if ( this.StoppingTimeoutElapsed() )
            {
                this.Fire( ProcessTrigger.Initial );
            }
            else
            {
                switch ( this.CurrentState )
                {
                    case ProcessState.Failed:
                        {
                            this.Fire( ProcessTrigger.Initial );
                            break;
                        }

                    case ProcessState.Initial:
                        {
                            this.Fire( ProcessTrigger.Initial );
                            break;
                        }

                    case ProcessState.Prime:
                        {
                            this.Fire( ProcessTrigger.Complete );
                            break;
                        }

                    case ProcessState.Busy:
                        {
                            this.Fire( ProcessTrigger.Complete );
                            break;
                        }

                    case ProcessState.Completed:
                        {
                            this.Fire( ProcessTrigger.Initial );
                            break;
                        }

                    case ProcessState.Ready:
                        {
                            this.Fire( ProcessTrigger.Complete );
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhanded {this.StateMachine.State} state processing stop" );
                        }
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent Process states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum ProcessState
    {

        /// <summary> An enum constant representing the initial state; Part not present. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the Prime state; Prepare the process. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the Ready state; Process is Ready to start. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the Busy state; Process is active. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the failed state. Process failed. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the Completed state. Process completed. </summary>
        [Description( "Completed" )]
        Completed
    }

    /// <summary> Values that represent Process triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum ProcessTrigger
    {

        /// <summary> An enum constant representing the initial trigger. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the Prime trigger to prepare the process; go to the Prime state. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the Ready trigger to go to the ready state. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the Busy trigger to activate the Process. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the Complete trigger to complete the process. </summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the fail option. process failed. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
        [Description( "Proceed" )]
        Proceed
    }
}
