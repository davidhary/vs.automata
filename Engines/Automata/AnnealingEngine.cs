using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for the Annealing process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class AnnealingEngine : EngineBase<AnnealingState, AnnealingTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public AnnealingEngine( string name ) : base( name, new StateMachine<AnnealingState, AnnealingTrigger>( AnnealingState.Initial ) )
        {
            StateMachine<AnnealingState, AnnealingTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( AnnealingState.Initial );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.Initial );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Permit( AnnealingTrigger.LoweringProbe, AnnealingState.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Permit( AnnealingTrigger.SeebeckPartInserted, AnnealingState.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
            this.EngagedStates.Add( AnnealingState.SeebeckPartInserted );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.SeebeckPartInserted );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Permit( AnnealingTrigger.TemperatureMonitoringStarted, AnnealingState.TemperatureMonitoringStarted );
            this.EngagedStates.Add( AnnealingState.TemperatureMonitoringStarted );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.TemperatureMonitoringStarted );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.Permit( AnnealingTrigger.SeebeckMeasured, AnnealingState.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.TemperatureMonitoringStarted );
            this.EngagedStates.Add( AnnealingState.SeebeckMeasured );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.SeebeckMeasured );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Permit( AnnealingTrigger.LoweringProbe, AnnealingState.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
            this.EngagedStates.Add( AnnealingState.LoweringProbe );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.LoweringProbe );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Permit( AnnealingTrigger.LiftingProbe, AnnealingState.LiftingProbe );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.Permit( AnnealingTrigger.Probing, AnnealingState.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.Failed );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.Fail );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.Probing );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.Permit( AnnealingTrigger.LiftingProbe, AnnealingState.LiftingProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
            stateConfiguration = this.StateMachine.Configure( AnnealingState.LiftingProbe );
            _ = stateConfiguration.Permit( AnnealingTrigger.Fail, AnnealingState.Failed );
            _ = stateConfiguration.Permit( AnnealingTrigger.Initial, AnnealingState.Initial );
            _ = stateConfiguration.PermitReentry( AnnealingTrigger.LiftingProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.LoweringProbe );
            _ = stateConfiguration.Ignore( AnnealingTrigger.Probing );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckMeasured );
            _ = stateConfiguration.Ignore( AnnealingTrigger.SeebeckPartInserted );
            _ = stateConfiguration.Ignore( AnnealingTrigger.TemperatureMonitoringStarted );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( AnnealingTrigger.Initial );
        }

        /// <summary> Inserts this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Insert()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.SeebeckPartInserted );
            }
        }

        /// <summary> Removes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Remove()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.Initial );
            }
        }

        /// <summary> Probings this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Probing()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.Probing );
            }
        }

        /// <summary> Starts temperature monitoring. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void StartTemperatureMonitoring()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.TemperatureMonitoringStarted );
            }
        }

        /// <summary> Seebeck measured. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void SeebeckMeasured()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.SeebeckMeasured );
            }
        }

        /// <summary> Lifting probe. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void LiftingProbe()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.LiftingProbe );
            }
        }

        /// <summary> Lowering probe. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void LoweringProbe()
        {
            if ( this.StopRequested )
            {
                this.Fire( AnnealingTrigger.Initial );
            }
            else
            {
                this.Fire( AnnealingTrigger.LoweringProbe );
            }
        }

        /// <summary> Annealing failed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fail()
        {
            this.Fire( AnnealingTrigger.Fail );
        }

        #endregion

    }

    /// <summary> Values that represent annealing states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum AnnealingState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the seebeck part inserted option. </summary>
        [Description( "Seebeck part inserted" )]
        SeebeckPartInserted,

        /// <summary> An enum constant representing the temperature monitoring started option. </summary>
        [Description( "Temperature Monitoring Started" )]
        TemperatureMonitoringStarted,

        /// <summary> An enum constant representing the seebeck measured option. </summary>
        [Description( "Seebeck Measured" )]
        SeebeckMeasured,

        /// <summary> An enum constant representing the lowering probe option. </summary>
        [Description( "Lowering the probe" )]
        LoweringProbe,

        /// <summary> An enum constant representing the probing option. </summary>
        [Description( "Probing" )]
        Probing,

        /// <summary> An enum constant representing the failed option. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the lifting probe option. </summary>
        [Description( "Lifting the probe" )]
        LiftingProbe
    }

    /// <summary> Values that represent annealing triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum AnnealingTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the seebeck part inserted option. </summary>
        [Description( "Seebeck part inserted" )]
        SeebeckPartInserted,

        /// <summary> An enum constant representing the temperature monitoring started option. </summary>
        [Description( "Temperature Monitoring Started" )]
        TemperatureMonitoringStarted,

        /// <summary> An enum constant representing the seebeck measured option. </summary>
        [Description( "Seebeck Measured" )]
        SeebeckMeasured,

        /// <summary> An enum constant representing the lowering probe option. </summary>
        [Description( "Lowering the probe" )]
        LoweringProbe,

        /// <summary> An enum constant representing the probing option. </summary>
        [Description( "Probing" )]
        Probing,

        /// <summary> An enum constant representing the fail option. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the lifting probe option. </summary>
        [Description( "Lifting the probe" )]
        LiftingProbe
    }
}
