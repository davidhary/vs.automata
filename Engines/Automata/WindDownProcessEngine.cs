using System;
using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a Wind-Down process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class WindDownProcessEngine : EngineBase<WindDownProcessState, WindDownProcessTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public WindDownProcessEngine( string name ) : base( name, new StateMachine<WindDownProcessState, WindDownProcessTrigger>( WindDownProcessState.Initial ) )
        {
            StateMachine<WindDownProcessState, WindDownProcessTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Initial );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Initial );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Prime, WindDownProcessState.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Prime );
            this.EngagedStates.Add( WindDownProcessState.Prime );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Prime );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Ready, WindDownProcessState.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Busy, WindDownProcessState.WindDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindDown, WindDownProcessState.WindDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Ready );
            this.EngagedStates.Add( WindDownProcessState.Ready );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Ready );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Prime, WindDownProcessState.Prime );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Starting, WindDownProcessState.Starting );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Busy, WindDownProcessState.Finished );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Finish, WindDownProcessState.Finished );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindDown, WindDownProcessState.WindDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Starting );
            this.EngagedStates.Add( WindDownProcessState.Starting );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Starting );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Busy, WindDownProcessState.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Busy );
            this.EngagedStates.Add( WindDownProcessState.Busy );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Busy );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Finish, WindDownProcessState.Finished );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Finished );
            this.EngagedStates.Add( WindDownProcessState.Finished );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Finished );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindDown, WindDownProcessState.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.WindDown );
            this.EngagedStates.Add( WindDownProcessState.WindDown );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.WindDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.WindingDown );
            this.EngagedStates.Add( WindDownProcessState.WindingDown );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.WindingDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Prime, WindDownProcessState.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Complete, WindDownProcessState.WoundDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.WoundDown );
            this.EngagedStates.Add( WindDownProcessState.Failed );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.Failed );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Initial );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Prime, WindDownProcessState.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Fail );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Initial );
            this.EngagedStates.Add( WindDownProcessState.WoundDown );
            stateConfiguration = this.StateMachine.Configure( WindDownProcessState.WoundDown );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Initial, WindDownProcessState.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Prime );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Prime, WindDownProcessState.Prime );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Ready );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Starting, WindDownProcessState.Starting );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Busy );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.Finish );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindDown );
            _ = stateConfiguration.Ignore( WindDownProcessTrigger.WindingDown );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Complete );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Fail, WindDownProcessState.Failed );
            _ = stateConfiguration.PermitReentry( WindDownProcessTrigger.Cancel );
            _ = stateConfiguration.Permit( WindDownProcessTrigger.Proceed, WindDownProcessState.Initial );
        }

        #endregion

        #region " PROGRESS "

        /// <summary> Gets the state progress. </summary>
        /// <value> The state progress. </value>
        public int StateProgress => ( int ) (100 * ( int ) this.CurrentState / ( double ) WindDownProcessState.WoundDown);

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> The percent progress. </summary>
        /// <value> The percent progress. </value>
        public int PercentProgress
        {
            get => this._PercentProgress;

            set {
                value = Math.Min( 100, Math.Max( 0, value ) );
                if ( value != this.PercentProgress )
                {
                    this._PercentProgress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the percent progress. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UpdatePercentProgress()
        {
            if ( this.CurrentState != WindDownProcessState.Initial )
            {
                this.PercentProgress = this.StateProgress;
            }
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Process the stop request. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public override void ProcessStopRequest()
        {
            if ( this.StoppingTimeoutElapsed() )
            {
                this.Fire( WindDownProcessTrigger.Initial );
            }
            else
            {
                switch ( this.CurrentState )
                {
                    case WindDownProcessState.Initial:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.Prime:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.Ready:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.Starting:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.Busy:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.Finished:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.WindDown:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    case WindDownProcessState.WindingDown:
                        {
                            this.Fire( WindDownProcessTrigger.Complete );
                            break;
                        }

                    case WindDownProcessState.WoundDown:
                        {
                            this.Fire( WindDownProcessTrigger.Complete );
                            break;
                        }

                    case WindDownProcessState.Failed:
                        {
                            this.Fire( WindDownProcessTrigger.Initial );
                            break;
                        }

                    default:
                        {
                            throw new InvalidOperationException( $"Unhanded {this.StateMachine.State} state processing stop" );
                        }
                }
            }
        }

        #endregion

    }

    /// <summary> Values that represent wind-down process states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum WindDownProcessState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the prime option. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the starting option. </summary>
        [Description( "Starting" )]
        Starting,

        /// <summary> An enum constant representing the busy option. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the finished option. </summary>
        [Description( "Finished" )]
        Finished,

        /// <summary> An enum constant representing the wind down option. </summary>
        [Description( "Wind Down" )]
        WindDown,

        /// <summary> An enum constant representing the winding down option. </summary>
        [Description( "Winding Down" )]
        WindingDown,

        /// <summary> An enum constant representing the failed option. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the wound down option. </summary>
        [Description( "Wound Down" )]
        WoundDown
    }

    /// <summary> Values that represent wind-down process triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum WindDownProcessTrigger
    {

        /// <summary> . </summary>
        [Description( "Initial: Any -> Initial" )]
        Initial,

        /// <summary> . </summary>
        [Description( "Prime: Idle -> Prime" )]
        Prime,

        /// <summary> . </summary>
        [Description( "Ready: Prime -> Test Ready" )]
        Ready,

        /// <summary> . </summary>
        [Description( "Starting: Ready -> Starting" )]
        Starting,

        /// <summary> . </summary>
        [Description( "Busy: Starting -> Busy" )]
        Busy,

        /// <summary> . </summary>
        [Description( "Finish: Busy --> Finished" )]
        Finish,

        /// <summary> . </summary>
        [Description( "Wind Down: Any State -> Wind Down" )]
        WindDown,

        /// <summary> . </summary>
        [Description( "Winding Down: Wind Down -> Winding Down" )]
        WindingDown,

        /// <summary> . </summary>
        [Description( "Complete: Finished, Wind Down, Winding down -> Wound Down" )]
        Complete,

        /// <summary> An enum constant representing the cancel option. </summary>
        [Description( "Cancel testing" )]
        Cancel,

        /// <summary> An enum constant representing the fail option. Finite state machine failed. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
        [Description( "Proceed" )]
        Proceed
    }
}
