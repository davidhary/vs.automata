﻿using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a simple On/Off toggle process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class ToggleEngine : EngineBase<ToggleState, byte>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> State of the initial. </param>
        public ToggleEngine( string name, ToggleState initialState ) : base( name, new StateMachine<ToggleState, byte>( initialState ) )
        {
            _ = this.StateMachine.Configure( ToggleState.OffState ).Permit( _ToggleTrigger, ToggleState.OnState );
            _ = this.StateMachine.Configure( ToggleState.OnState ).Permit( _ToggleTrigger, ToggleState.OffState );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public ToggleEngine( string name ) : this( name, ToggleState.OffState )
        {
        }

        #endregion

        #region " TRIGGERS and STATES "

        /// <summary> The toggle trigger. </summary>
        private const byte _ToggleTrigger = 0;

        #endregion

        #region " COMMANDs "

        /// <summary> Toggles the state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Toggle()
        {
            this.Fire( _ToggleTrigger );
        }

        /// <summary> Turn on. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void TurnOn()
        {
            if ( this.CurrentState == ToggleState.OffState )
            {
                this.Toggle();
            }
        }

        /// <summary> Turn off. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void TurnOff()
        {
            if ( this.CurrentState == ToggleState.OnState )
            {
                this.Toggle();
            }
        }

        #endregion

    }

    /// <summary> Values that represent toggle states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum ToggleState
    {

        /// <summary> An enum constant representing the off state option. </summary>
        [System.ComponentModel.Description( "Off" )]
        OffState,

        /// <summary> An enum constant representing the on state option. </summary>
        [System.ComponentModel.Description( "On" )]
        OnState
    }
}