﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a single measurement. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class MeasureEngine : EngineBase<MeasureState, MeasureTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public MeasureEngine( string name ) : base( name, new StateMachine<MeasureState, MeasureTrigger>( MeasureState.Initial ) )
        {
            StateMachine<MeasureState, MeasureTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( MeasureState.Initial );
            _ = stateConfiguration.Ignore( MeasureTrigger.Initial );
            _ = stateConfiguration.Ignore( MeasureTrigger.Cancel );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Permit( MeasureTrigger.Prime, MeasureState.Priming );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
            this.EngagedStates.Add( MeasureState.Priming );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Priming );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Permit( MeasureTrigger.Cancel, MeasureState.Canceled );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Permit( MeasureTrigger.Ready, MeasureState.Primed );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
            this.EngagedStates.Add( MeasureState.Primed );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Primed );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Permit( MeasureTrigger.Cancel, MeasureState.Canceled );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Permit( MeasureTrigger.Start, MeasureState.Busy );
            this.EngagedStates.Add( MeasureState.Busy );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Busy );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Permit( MeasureTrigger.Cancel, MeasureState.Canceled );
            _ = stateConfiguration.Permit( MeasureTrigger.Complete, MeasureState.Completed );
            _ = stateConfiguration.Permit( MeasureTrigger.Fail, MeasureState.Failed );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Failed );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Permit( MeasureTrigger.Cancel, MeasureState.Canceled );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Completed );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Ignore( MeasureTrigger.Cancel );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
            stateConfiguration = this.StateMachine.Configure( MeasureState.Canceled );
            _ = stateConfiguration.Permit( MeasureTrigger.Initial, MeasureState.Initial );
            _ = stateConfiguration.Ignore( MeasureTrigger.Cancel );
            _ = stateConfiguration.Ignore( MeasureTrigger.Complete );
            _ = stateConfiguration.Ignore( MeasureTrigger.Fail );
            _ = stateConfiguration.Ignore( MeasureTrigger.Prime );
            _ = stateConfiguration.Ignore( MeasureTrigger.Ready );
            _ = stateConfiguration.Ignore( MeasureTrigger.Start );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Cancels this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Cancel()
        {
            this.Fire( MeasureTrigger.Cancel );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( MeasureTrigger.Initial );
        }

        /// <summary> Completes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Complete()
        {
            if ( this.StopRequested )
            {
                this.Fire( MeasureTrigger.Cancel );
            }
            else
            {
                this.Fire( MeasureTrigger.Complete );
            }
        }

        /// <summary> Primes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Prime()
        {
            if ( this.StopRequested )
            {
                this.Fire( MeasureTrigger.Cancel );
            }
            else
            {
                this.Fire( MeasureTrigger.Prime );
            }
        }

        /// <summary> Readies this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Ready()
        {
            if ( this.StopRequested )
            {
                this.Fire( MeasureTrigger.Cancel );
            }
            else
            {
                this.Fire( MeasureTrigger.Ready );
            }
        }

        /// <summary> Starts this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Start()
        {
            if ( this.StopRequested )
            {
                this.Fire( MeasureTrigger.Cancel );
            }
            else
            {
                this.Fire( MeasureTrigger.Start );
            }
        }

        /// <summary> Measure failed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fail()
        {
            this.Fire( MeasureTrigger.Fail );
        }

        #endregion

    }

    /// <summary> Values that represent measure states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum MeasureState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the priming option. </summary>
        [Description( "Priming" )]
        Priming,

        /// <summary> An enum constant representing the primed option. </summary>
        [Description( "Primed" )]
        Primed,

        /// <summary> An enum constant representing the busy option. </summary>
        [Description( "Busy" )]
        Busy,

        /// <summary> An enum constant representing the failed option. </summary>
        [Description( "Failed" )]
        Failed,

        /// <summary> An enum constant representing the completed option. </summary>
        [Description( "Completed" )]
        Completed,

        /// <summary> An enum constant representing the canceled option. </summary>
        [Description( "Canceled" )]
        Canceled
    }

    /// <summary> Values that represent measure triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum MeasureTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the prime option. </summary>
        [Description( "Prime" )]
        Prime,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the start option. </summary>
        [Description( "Start" )]
        Start,

        /// <summary> An enum constant representing the complete option. </summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the fail option. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the cancel option. </summary>
        [Description( "Cancel" )]
        Cancel
    }
}