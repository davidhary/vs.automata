﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary>
    /// Implements a finite automata for a Platform capable of accessing databases and instruments
    /// and selecting a lot for testing.
    /// </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class PlatformEngine : EngineBase<PlatformState, PlatformTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public PlatformEngine( string name ) : base( name, new StateMachine<PlatformState, PlatformTrigger>( PlatformState.Initial ) )
        {
            StateMachine<PlatformState, PlatformTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( PlatformState.Initial );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closed );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Ignore( PlatformTrigger.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Closed );
            _ = stateConfiguration.Ignore( PlatformTrigger.Close );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Permit( PlatformTrigger.Open, PlatformState.Opening );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Closing );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Closing );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closed );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closed );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connecting );
            _ = stateConfiguration.Permit( PlatformTrigger.Disconnect, PlatformState.Disconnecting );
            _ = stateConfiguration.Permit( PlatformTrigger.Engage, PlatformState.Busy );
            _ = stateConfiguration.PermitReentry( PlatformTrigger.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Permit( PlatformTrigger.Open, PlatformState.Opening );
            _ = stateConfiguration.Permit( PlatformTrigger.Ready, PlatformState.Ready );
            _ = stateConfiguration.Permit( PlatformTrigger.SelectLot, PlatformState.Selecting );
            this.EngagedStates.Add( PlatformState.Opening );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Opening );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closed );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Permit( PlatformTrigger.Open, PlatformState.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Open );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Open );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closing );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connecting );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Connecting );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Connecting );
            _ = stateConfiguration.Ignore( PlatformTrigger.Close );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connected );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Connected );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Connected );
            _ = stateConfiguration.Ignore( PlatformTrigger.Close );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Permit( PlatformTrigger.Disconnect, PlatformState.Disconnecting );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Permit( PlatformTrigger.SelectLot, PlatformState.Selecting );
            this.EngagedStates.Add( PlatformState.Disconnecting );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Disconnecting );
            _ = stateConfiguration.Ignore( PlatformTrigger.Close );
            _ = stateConfiguration.Ignore( PlatformTrigger.Connect );
            _ = stateConfiguration.Permit( PlatformTrigger.Disconnect, PlatformState.Disconnected );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Disconnected );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Disconnected );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closing );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connecting );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Permit( PlatformTrigger.Open, PlatformState.Open );
            _ = stateConfiguration.Ignore( PlatformTrigger.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Selecting );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Selecting );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closing );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connected );
            _ = stateConfiguration.Permit( PlatformTrigger.Disconnect, PlatformState.Disconnected );
            _ = stateConfiguration.Ignore( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Permit( PlatformTrigger.Ready, PlatformState.Ready );
            _ = stateConfiguration.PermitReentry( PlatformTrigger.SelectLot );
            this.EngagedStates.Add( PlatformState.Ready );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Ready );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connected );
            _ = stateConfiguration.Permit( PlatformTrigger.Close, PlatformState.Closing );
            _ = stateConfiguration.Permit( PlatformTrigger.Disconnect, PlatformState.Disconnecting );
            _ = stateConfiguration.Permit( PlatformTrigger.Engage, PlatformState.Busy );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.PermitReentry( PlatformTrigger.Ready );
            _ = stateConfiguration.Permit( PlatformTrigger.SelectLot, PlatformState.Selecting );
            this.EngagedStates.Add( PlatformState.Busy );
            stateConfiguration = this.StateMachine.Configure( PlatformState.Busy );
            _ = stateConfiguration.Permit( PlatformTrigger.Connect, PlatformState.Connected );
            _ = stateConfiguration.Ignore( PlatformTrigger.Close );
            _ = stateConfiguration.Ignore( PlatformTrigger.Disconnect );
            _ = stateConfiguration.PermitReentry( PlatformTrigger.Engage );
            _ = stateConfiguration.Permit( PlatformTrigger.Estop, PlatformState.Estop );
            _ = stateConfiguration.Permit( PlatformTrigger.Initial, PlatformState.Initial );
            _ = stateConfiguration.Ignore( PlatformTrigger.Open );
            _ = stateConfiguration.Permit( PlatformTrigger.Ready, PlatformState.Ready );
            _ = stateConfiguration.Ignore( PlatformTrigger.SelectLot );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> E stops this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Estop()
        {
            this.Fire( PlatformTrigger.Estop );
        }

        /// <summary> Closes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Close()
        {
            this.Fire( PlatformTrigger.Close );
        }

        /// <summary> Opens this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Open()
        {
            this.Fire( PlatformTrigger.Open );
        }

        /// <summary> Connects this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Connect()
        {
            this.Fire( PlatformTrigger.Connect );
        }

        /// <summary> Disconnects this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Disconnect()
        {
            this.Fire( PlatformTrigger.Disconnect );
        }

        /// <summary> Select lot. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void SelectLot()
        {
            this.Fire( PlatformTrigger.SelectLot );
        }

        /// <summary> Engages this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Engage()
        {
            this.Fire( PlatformTrigger.Engage );
        }

        /// <summary> Readies this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Ready()
        {
            this.Fire( PlatformTrigger.Ready );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( PlatformTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent platform states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum PlatformState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the closed option. </summary>
        [Description( "Closed" )]
        Closed,

        /// <summary> An enum constant representing the closing option. </summary>
        [Description( "Closing" )]
        Closing,

        /// <summary> An enum constant representing the estop] option. </summary>
        [Description( "Emergency Stop" )]
        Estop,

        /// <summary> An enum constant representing the opening option. </summary>
        [Description( "Opening" )]
        Opening,

        /// <summary> An enum constant representing the open option. </summary>
        [Description( "Open" )]
        Open,

        /// <summary> An enum constant representing the connecting option. </summary>
        [Description( "Connecting" )]
        Connecting,

        /// <summary> An enum constant representing the connected option. </summary>
        [Description( "Connected" )]
        Connected,

        /// <summary> An enum constant representing the disconnecting option. </summary>
        [Description( "Disconnecting" )]
        Disconnecting,

        /// <summary> An enum constant representing the disconnected option. </summary>
        [Description( "Disconnected" )]
        Disconnected,

        /// <summary> An enum constant representing the selecting option. </summary>
        [Description( "Selecting" )]
        Selecting,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the busy option. </summary>
        [Description( "Busy" )]
        Busy
    }

    /// <summary> Values that represent platform triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum PlatformTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the estop option. </summary>
        [Description( "Emergency Stop" )]
        Estop,

        /// <summary> An enum constant representing the close option. </summary>
        [Description( "Close" )]
        Close,

        /// <summary> An enum constant representing the open option. </summary>
        [Description( "Open" )]
        Open,

        /// <summary> An enum constant representing the connect option. </summary>
        [Description( "Connect" )]
        Connect,

        /// <summary> An enum constant representing the disconnect option. </summary>
        [Description( "Disconnect" )]
        Disconnect,

        /// <summary> An enum constant representing the select lot option. </summary>
        [Description( "Select Lot" )]
        SelectLot,

        /// <summary> An enum constant representing the engage option. </summary>
        [Description( "Engage" )]
        Engage,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready
    }
}