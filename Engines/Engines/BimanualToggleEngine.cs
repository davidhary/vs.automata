using System;
using System.ComponentModel;
using System.Diagnostics;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a bi-manual toggle switch. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class BimanualToggleEngine : EngineBase<BimanualToggleState, BimanualToggleTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public BimanualToggleEngine( string name ) : base( name, new StateMachine<BimanualToggleState, BimanualToggleTrigger>( BimanualToggleState.Off ) )
        {
            this.ResponseStopwatch = new Stopwatch();
            StateMachine<BimanualToggleState, BimanualToggleTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( BimanualToggleState.Off );
            _ = stateConfiguration.OnEntry( t => this.OnEntry( t ) );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Acknowledge );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Set );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.ButtonReleased );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Estop, BimanualToggleState.Estop );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Timeout );
            stateConfiguration = this.StateMachine.Configure( BimanualToggleState.Set );
            _ = stateConfiguration.OnEntry( t => this.OnEntry( t ) );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Acknowledge );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Estop, BimanualToggleState.Estop );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Go );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Timeout, BimanualToggleState.Off );
            stateConfiguration = this.StateMachine.Configure( BimanualToggleState.Go );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Acknowledge, BimanualToggleState.On );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.ButtonPressed );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Estop, BimanualToggleState.Estop );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Timeout );
            stateConfiguration = this.StateMachine.Configure( BimanualToggleState.On );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Acknowledge );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.ButtonPressed );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.Estop, BimanualToggleState.Estop );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Timeout );
            stateConfiguration = this.StateMachine.Configure( BimanualToggleState.Estop );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Acknowledge );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Off );
            _ = stateConfiguration.Permit( BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Estop );
            _ = stateConfiguration.Ignore( BimanualToggleTrigger.Timeout );
        }

        #endregion

        #region " MACHINE "

        /// <summary> Executes the entry action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnEntry( StateMachine<BimanualToggleState, BimanualToggleTrigger>.Transition transition )
        {
            if ( transition.Destination == BimanualToggleState.Off )
            {
                this.ResponseStopwatch.Reset();
            }
            else if ( transition.Destination == BimanualToggleState.Set )
            {
                this.ResponseStopwatch.Restart();
            }
        }

        #endregion

        #region " TIMEOUT MONITOR "

        /// <summary> Gets or sets the response stopwatch. </summary>
        /// <value> The response stopwatch. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private Stopwatch ResponseStopwatch { get; set; }

        /// <summary> The response timeout. </summary>
        private TimeSpan _ResponseTimeout;

        /// <summary>
        /// The required response time for closing the button. For the machine to advance to the Go state,
        /// the operator must close both buttons within the minimum Response Time seconds.
        /// </summary>
        /// <value> The response timeout. </value>
        public TimeSpan ResponseTimeout
        {
            get => this._ResponseTimeout;

            set {
                if ( value != this.ResponseTimeout )
                {
                    this._ResponseTimeout = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the is response timeout. </summary>
        /// <value> The is response timeout. </value>
        public bool IsResponseTimeout => this.ResponseStopwatch.IsRunning && this.ResponseStopwatch.Elapsed > this.ResponseTimeout;

        #endregion

        #region " COMMANDs "

        /// <summary> Button pressed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void ButtonPressed()
        {
            if ( this.IsResponseTimeout )
            {
                this.Fire( BimanualToggleTrigger.Timeout );
            }
            else
            {
                this.Fire( BimanualToggleTrigger.ButtonPressed );
            }
        }

        /// <summary> Button released. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void ButtonReleased()
        {
            this.Fire( BimanualToggleTrigger.ButtonReleased );
        }

        /// <summary> Estop pressed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void EstopPressed()
        {
            this.Fire( BimanualToggleTrigger.Estop );
        }

        /// <summary> Acknowledges this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Acknowledge()
        {
            this.Fire( BimanualToggleTrigger.Acknowledge );
        }

        #endregion

    }

    /// <summary> Values that represent bimanual toggle states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum BimanualToggleState
    {

        /// <summary> An enum constant representing the off] option. </summary>
        [Description( "Off State" )]
        Off,

        /// <summary> An enum constant representing the on] option. </summary>
        [Description( "On State" )]
        On,

        /// <summary> An enum constant representing the set] option. </summary>
        [Description( "One button pressed" )]
        Set,

        /// <summary> An enum constant representing the go] option. </summary>
        [Description( "Two buttons pressed" )]
        Go,

        /// <summary> An enum constant representing the estop] option. </summary>
        [Description( "Emergency Stop" )]
        Estop
    }

    /// <summary> Values that represent bimanual toggle triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum BimanualToggleTrigger
    {

        /// <summary> An enum constant representing the button pressed option. </summary>
        [Description( "Button Pressed" )]
        ButtonPressed,

        /// <summary> An enum constant representing the button released option. </summary>
        [Description( "Button Released" )]
        ButtonReleased,

        /// <summary> An enum constant representing the acknowledge option. </summary>
        [Description( "Acknowledge" )]
        Acknowledge,

        /// <summary> An enum constant representing the timeout option. </summary>
        [Description( "Timeout" )]
        Timeout,

        /// <summary> An enum constant representing the estop option. </summary>
        [Description( "Emergency Stop" )]
        Estop
    }
}
