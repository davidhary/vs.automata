﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for an Entity. </summary>
    /// <remarks>
    /// Describes the sequence of an entity, such as a test data entity from selection through usages
    /// and completion. <para>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-05-08 </para><para>
    /// David, 2017-04-26 </para>
    /// </remarks>
    public class EntityEngine : EngineBase<EntityState, EntityTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public EntityEngine( string name ) : base( name, new StateMachine<EntityState, EntityTrigger>( EntityState.Initial ) )
        {
            StateMachine<EntityState, EntityTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( EntityState.Initial );
            _ = stateConfiguration.Ignore( EntityTrigger.Initial );
            _ = stateConfiguration.Ignore( EntityTrigger.Complete );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Ignore( EntityTrigger.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Ignore( EntityTrigger.Finalize );
            this.EngagedStates.Add( EntityState.Finding );
            stateConfiguration = this.StateMachine.Configure( EntityState.Finding );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Ignore( EntityTrigger.Complete );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Ignore( EntityTrigger.Finalize );
            _ = stateConfiguration.PermitReentry( EntityTrigger.Find );
            this.EngagedStates.Add( EntityState.Creating );
            stateConfiguration = this.StateMachine.Configure( EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Ignore( EntityTrigger.Complete );
            _ = stateConfiguration.Ignore( EntityTrigger.Create );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Ignore( EntityTrigger.Finalize );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            this.EngagedStates.Add( EntityState.Created );
            stateConfiguration = this.StateMachine.Configure( EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Permit( EntityTrigger.Complete, EntityState.Completed );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Ignore( EntityTrigger.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Permit( EntityTrigger.Finalize, EntityState.Finalized );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            this.EngagedStates.Add( EntityState.Fetching );
            stateConfiguration = this.StateMachine.Configure( EntityState.Fetching );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Permit( EntityTrigger.Complete, EntityState.Completed );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Ignore( EntityTrigger.Fetch );
            _ = stateConfiguration.Permit( EntityTrigger.Finalize, EntityState.Finalized );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            this.EngagedStates.Add( EntityState.Engaged );
            stateConfiguration = this.StateMachine.Configure( EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Permit( EntityTrigger.Complete, EntityState.Completed );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Ignore( EntityTrigger.Engage );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Permit( EntityTrigger.Finalize, EntityState.Finalized );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            this.EngagedStates.Add( EntityState.Finalized );
            stateConfiguration = this.StateMachine.Configure( EntityState.Completed );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Ignore( EntityTrigger.Complete );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Permit( EntityTrigger.Finalize, EntityState.Finalized );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
            stateConfiguration = this.StateMachine.Configure( EntityState.Finalized );
            _ = stateConfiguration.Permit( EntityTrigger.Initial, EntityState.Initial );
            _ = stateConfiguration.Permit( EntityTrigger.Complete, EntityState.Completed );
            _ = stateConfiguration.Permit( EntityTrigger.Create, EntityState.Creating );
            _ = stateConfiguration.Permit( EntityTrigger.Created, EntityState.Created );
            _ = stateConfiguration.Permit( EntityTrigger.Engage, EntityState.Engaged );
            _ = stateConfiguration.Permit( EntityTrigger.Fetch, EntityState.Fetching );
            _ = stateConfiguration.Ignore( EntityTrigger.Finalize );
            _ = stateConfiguration.Permit( EntityTrigger.Find, EntityState.Finding );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( EntityTrigger.Initial );
        }

        /// <summary> Sends the find trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Find()
        {
            this.Fire( EntityTrigger.Find );
        }

        /// <summary> Sends the fetch trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Fetch()
        {
            this.Fire( EntityTrigger.Fetch );
        }

        /// <summary> Sends the Create trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Create()
        {
            this.Fire( EntityTrigger.Create );
        }

        /// <summary> Sends the Created trigger. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Created()
        {
            this.Fire( EntityTrigger.Created );
        }

        /// <summary> Engages this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Engage()
        {
            this.Fire( EntityTrigger.Engage );
        }

        /// <summary> Completes this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Complete()
        {
            this.Fire( EntityTrigger.Complete );
        }

        /// <summary> Finalize entity. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void FinalizeEntity()
        {
            this.Fire( EntityTrigger.Finalize );
        }

        #endregion

    }

    /// <summary> Values that represent Entity states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum EntityState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> Searching for an existing entity. </summary>
        [Description( "Finding an Entity" )]
        Finding,

        /// <summary> An entity is being created. </summary>
        [Description( "Creating an Entity" )]
        Creating,

        /// <summary> A new entity was created and saved into the database. </summary>
        [Description( "Entity Created" )]
        Created,

        /// <summary> An entity is being Fetched. </summary>
        [Description( "Fetching an Entity" )]
        Fetching,

        /// <summary> The entity is engaged. </summary>
        [Description( "Entity is engaged" )]
        Engaged,

        /// <summary> Entity is completed but perhaps not yet finalized, such as all serial number assigned. </summary>
        [Description( "Entity Completed" )]
        Completed,

        /// <summary> Entity is completed and finalized. </summary>
        [Description( "Entity Finalized" )]
        Finalized
    }

    /// <summary> Values that represent Entity triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum EntityTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the find option. </summary>
        [Description( "Find" )]
        Find,

        /// <summary> An enum constant representing the fetch option. </summary>
        [Description( "Fetch" )]
        Fetch,

        /// <summary> An enum constant representing the create option. </summary>
        [Description( "Create" )]
        Create,

        /// <summary> An enum constant representing the created option. </summary>
        [Description( "Created" )]
        Created,

        /// <summary> An enum constant representing the engage option. </summary>
        [Description( "Engage" )]
        Engage,

        /// <summary> An enum constant representing the complete option. </summary>
        [Description( "Complete" )]
        Complete,

        /// <summary> An enum constant representing the finalize option. </summary>
        [Description( "Finalize" )]
        Finalize
    }
}