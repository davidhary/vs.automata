﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a Present/Absent process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class PresentAbsentEngine : EngineBase<PresentAbsentState, PresentAbsentTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> State of the initial. </param>
        public PresentAbsentEngine( string name, PresentAbsentState initialState ) : base( name, new StateMachine<PresentAbsentState, PresentAbsentTrigger>( initialState ) )
        {
            StateMachine<PresentAbsentState, PresentAbsentTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( PresentAbsentState.Initial );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Remove, PresentAbsentState.Absent );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Insert, PresentAbsentState.Present );
            _ = stateConfiguration.Ignore( PresentAbsentTrigger.Initial );
            this.EngagedStates.Add( PresentAbsentState.Present );
            stateConfiguration = this.StateMachine.Configure( PresentAbsentState.Present );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Initial, PresentAbsentState.Initial );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Remove, PresentAbsentState.Absent );
            _ = stateConfiguration.Ignore( PresentAbsentTrigger.Insert );
            this.EngagedStates.Add( PresentAbsentState.Absent );
            stateConfiguration = this.StateMachine.Configure( PresentAbsentState.Absent );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Initial, PresentAbsentState.Initial );
            _ = stateConfiguration.Ignore( PresentAbsentTrigger.Remove );
            _ = stateConfiguration.Permit( PresentAbsentTrigger.Insert, PresentAbsentState.Present );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public PresentAbsentEngine( string name ) : this( name, PresentAbsentState.Initial )
        {
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Signal a move to the Present state. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Insert()
        {
            this.Fire( PresentAbsentTrigger.Insert );
        }

        /// <summary> Signal a move to the Absent state. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Remove()
        {
            this.Fire( PresentAbsentTrigger.Remove );
        }

        /// <summary> Signal a move to the Initial state. </summary>
        /// <remarks> David, 2020-08-22. </remarks>
        public void Initial()
        {
            this.Fire( PresentAbsentTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent present absent states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum PresentAbsentState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the present option. </summary>
        [Description( "Present" )]
        Present,

        /// <summary> An enum constant representing the absent option. </summary>
        [Description( "Absent" )]
        Absent
    }

    /// <summary> Values that represent present absent triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum PresentAbsentTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the insert option. </summary>
        [Description( "Insert" )]
        Insert,

        /// <summary> An enum constant representing the remove option. </summary>
        [Description( "Remove" )]
        Remove
    }
}