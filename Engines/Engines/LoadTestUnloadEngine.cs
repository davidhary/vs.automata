using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a Load-Test-Unload process. </summary>
    /// <remarks>
    /// This engine can be used for sequencing tests of parts on a test station where parts are
    /// loaded and unloaded. (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-04-26 </para>
    /// </remarks>
    public class LoadTestUnloadEngine : EngineBase<LoadTestUnloadState, LoadTestUnloadTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public LoadTestUnloadEngine( string name ) : base( name, new StateMachine<LoadTestUnloadState, LoadTestUnloadTrigger>( LoadTestUnloadState.Initial ) )
        {
            StateMachine<LoadTestUnloadState, LoadTestUnloadTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            // required in order to stop testing from the initial state.
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Initial );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.Estop );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.PermitReentry( LoadTestUnloadTrigger.Estop );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.ReadyToLoad );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToTest );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.ReadyToTest );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.ReadyToTest );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.TestingStarting, LoadTestUnloadState.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.TestingStarting );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.TestingActive, LoadTestUnloadState.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.TestingActive );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.SelectNext, LoadTestUnloadState.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.SelectNext );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.PermitReentry( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.TestingCompleted );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.TestingCompleted );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToUnload );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            this.EngagedStates.Add( LoadTestUnloadState.ReadyToUnload );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.ReadyToUnload );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadBad, LoadTestUnloadState.UnloadingBad );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadGood, LoadTestUnloadState.UnloadingGood );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless );
            this.EngagedStates.Add( LoadTestUnloadState.UnloadingBad );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.UnloadingBad );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadGood, LoadTestUnloadState.UnloadingGood );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless );
            this.EngagedStates.Add( LoadTestUnloadState.UnloadingGood );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.UnloadingGood );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless );
            this.EngagedStates.Add( LoadTestUnloadState.UnloadingNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.UnloadingNameless );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.UnloadingAlert );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.FinishUnloading );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
            stateConfiguration = this.StateMachine.Configure( LoadTestUnloadState.Canceled );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Alert );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Canceled );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestComplete );
            _ = stateConfiguration.Permit( LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Load );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.Ready );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.SelectNext );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingStarting );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.TestingActive );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadBad );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadGood );
            _ = stateConfiguration.Ignore( LoadTestUnloadTrigger.UnloadNameless );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Alerts this object. </summary>
        /// <remarks>
        /// UInvoke by an Part test which state entered an
        /// <see cref="TestLocationState.RemoveException"/>.
        /// </remarks>
        public void Alert()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.Alert );
            }
        }

        /// <summary> E stops this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Estop()
        {
            this.Fire( LoadTestUnloadTrigger.Estop );
        }

        /// <summary> Canceling the test. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Canceling()
        {
            this.Fire( LoadTestUnloadTrigger.Canceling );
        }

        /// <summary> Test canceled. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Canceled()
        {
            this.Fire( LoadTestUnloadTrigger.Canceled );
        }

        /// <summary> Proceed to next state. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Ready()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.Ready );
            }
        }

        /// <summary> Loads this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Load()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.Load );
            }
        }

        /// <summary> Testing starting. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void TestingStarting()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.TestingStarting );
            }
        }

        /// <summary> Testing active. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void TestingActive()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.TestingActive );
            }
        }

        /// <summary> Sends the select next trigger to select the next test location. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void SelectNext()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.SelectNext );
            }
        }

        /// <summary> Tests complete. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void TestComplete()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.TestComplete );
            }
        }

        /// <summary> Unload bad. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UnloadBad()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.UnloadBad );
            }
        }

        /// <summary> Unload good. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UnloadGood()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.UnloadGood );
            }
        }

        /// <summary> Unload nameless. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void UnloadNameless()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.UnloadGood );
            }
        }

        /// <summary> Finishes an unloading. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void FinishUnloading()
        {
            if ( this.StopRequested )
            {
                this.Fire( LoadTestUnloadTrigger.Canceled );
            }
            else
            {
                this.Fire( LoadTestUnloadTrigger.FinishUnloading );
            }
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( LoadTestUnloadTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent Load Test Unload states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum LoadTestUnloadState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the ready to load option. </summary>
        [Description( "Ready to load" )]
        ReadyToLoad,

        /// <summary> An enum constant representing the ready to test option. </summary>
        [Description( "Ready to test; Door Closed" )]
        ReadyToTest,

        /// <summary> An enum constant representing the testing starting option. </summary>
        [Description( "Testing Started" )]
        TestingStarting,

        /// <summary> An enum constant representing the testing active option. </summary>
        [Description( "Testing Active" )]
        TestingActive,

        /// <summary> An enum constant representing the select next option. </summary>
        [Description( "Select Next" )]
        SelectNext,

        /// <summary> An enum constant representing the testing completed option. </summary>
        [Description( "Testing Completed" )]
        TestingCompleted,

        /// <summary> An enum constant representing the ready to unload option. </summary>
        [Description( "Ready to unload; Door open" )]
        ReadyToUnload,

        /// <summary> An enum constant representing the unloading bad option. </summary>
        [Description( "Unloading Bad" )]
        UnloadingBad,

        /// <summary> An enum constant representing the unloading good option. </summary>
        [Description( "Unloading Good" )]
        UnloadingGood,

        /// <summary> An enum constant representing the unloading nameless option. </summary>
        [Description( "Unloading Nameless" )]
        UnloadingNameless,

        /// <summary> An enum constant representing the unloading alert option. </summary>
        [Description( "Unloading Alert" )]
        UnloadingAlert,

        /// <summary> An enum constant representing the unloading done option. </summary>
        [Description( "Unloading Done" )]
        UnloadingDone,

        /// <summary> Test canceling; removing parts. </summary>
        [Description( "Testing Canceling" )]
        Canceling,

        /// <summary> Test canceled. </summary>
        [Description( "Testing Canceled" )]
        Canceled,

        /// <summary>   An enum constant representing the estop option. </summary>
        [Description( "Emergency Stop" )]
        Estop
    }

    /// <summary> Values that represent Load Test Unload triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum LoadTestUnloadTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the estop option. </summary>
        [Description( "Emergency Stop" )]
        Estop,

        /// <summary> An enum constant representing the ready option. </summary>
        [Description( "Ready" )]
        Ready,

        /// <summary> An enum constant representing the load option. </summary>
        [Description( "Load" )]
        Load,

        /// <summary> An enum constant representing the testing starting option. </summary>
        [Description( "Testing Starting" )]
        TestingStarting,

        /// <summary> An enum constant representing the testing active option. </summary>
        [Description( "Testing Active" )]
        TestingActive,

        /// <summary> An enum constant representing the select next option. </summary>
        [Description( "SelectNext" )]
        SelectNext,

        /// <summary> An enum constant representing the test complete option. </summary>
        [Description( "Test Complete" )]
        TestComplete,

        /// <summary> An enum constant representing the unload bad option. </summary>
        [Description( "Unload Bad" )]
        UnloadBad,

        /// <summary> An enum constant representing the unload good option. </summary>
        [Description( "Unload Good" )]
        UnloadGood,

        /// <summary> An enum constant representing the unload nameless option. </summary>
        [Description( "Unload Nameless" )]
        UnloadNameless,

        /// <summary> An enum constant representing the finish unloading option. </summary>
        [Description( "Done Unloading" )]
        FinishUnloading,

        /// <summary> . </summary>
        [Description( "Alert: Unloading -> Unloading Alert" )]
        Alert,

        /// <summary> An enum constant representing the canceling option. </summary>
        [Description( "Canceling testing" )]
        Canceling,

        /// <summary> An enum constant representing the canceled option. </summary>
        [Description( "Testing canceled" )]
        Canceled
    }
}
