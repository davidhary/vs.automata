﻿using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a Removal process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class RemoveEngine : EngineBase<RemoveState, RemoveTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public RemoveEngine( string name ) : base( name, new StateMachine<RemoveState, RemoveTrigger>( RemoveState.Initial ) )
        {
            StateMachine<RemoveState, RemoveTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( RemoveState.Initial );
            _ = stateConfiguration.Permit( RemoveTrigger.Absent, RemoveState.Absent );
            _ = stateConfiguration.Ignore( RemoveTrigger.Initial );
            _ = stateConfiguration.Ignore( RemoveTrigger.Insert );
            _ = stateConfiguration.Ignore( RemoveTrigger.Release );
            _ = stateConfiguration.Ignore( RemoveTrigger.Remove );
            _ = stateConfiguration.Ignore( RemoveTrigger.RemoveFailed );
            this.EngagedStates.Add( RemoveState.Absent );
            stateConfiguration = this.StateMachine.Configure( RemoveState.Absent );
            _ = stateConfiguration.Ignore( RemoveTrigger.Absent );
            _ = stateConfiguration.Permit( RemoveTrigger.Initial, RemoveState.Initial );
            _ = stateConfiguration.Permit( RemoveTrigger.Insert, RemoveState.Present );
            _ = stateConfiguration.Ignore( RemoveTrigger.Release );
            _ = stateConfiguration.Ignore( RemoveTrigger.Remove );
            _ = stateConfiguration.Ignore( RemoveTrigger.RemoveFailed );
            this.EngagedStates.Add( RemoveState.Present );
            stateConfiguration = this.StateMachine.Configure( RemoveState.Present );
            _ = stateConfiguration.Ignore( RemoveTrigger.Absent );
            _ = stateConfiguration.Permit( RemoveTrigger.Initial, RemoveState.Initial );
            _ = stateConfiguration.Ignore( RemoveTrigger.Insert );
            _ = stateConfiguration.Permit( RemoveTrigger.Release, RemoveState.Released );
            _ = stateConfiguration.Permit( RemoveTrigger.Remove, RemoveState.Absent );
            _ = stateConfiguration.Ignore( RemoveTrigger.RemoveFailed );
            this.EngagedStates.Add( RemoveState.Released );
            stateConfiguration = this.StateMachine.Configure( RemoveState.Released );
            _ = stateConfiguration.Ignore( RemoveTrigger.Absent );
            _ = stateConfiguration.Permit( RemoveTrigger.Initial, RemoveState.Initial );
            _ = stateConfiguration.Ignore( RemoveTrigger.Insert );
            _ = stateConfiguration.Ignore( RemoveTrigger.Release );
            _ = stateConfiguration.Permit( RemoveTrigger.Remove, RemoveState.Removed );
            _ = stateConfiguration.Permit( RemoveTrigger.RemoveFailed, RemoveState.NotRemoved );
            stateConfiguration = this.StateMachine.Configure( RemoveState.Removed );
            _ = stateConfiguration.Permit( RemoveTrigger.Absent, RemoveState.Absent );
            _ = stateConfiguration.Permit( RemoveTrigger.Initial, RemoveState.Initial );
            _ = stateConfiguration.Permit( RemoveTrigger.Insert, RemoveState.Present );
            _ = stateConfiguration.Ignore( RemoveTrigger.Release );
            _ = stateConfiguration.Ignore( RemoveTrigger.Remove );
            _ = stateConfiguration.Ignore( RemoveTrigger.RemoveFailed );
            stateConfiguration = this.StateMachine.Configure( RemoveState.NotRemoved );
            _ = stateConfiguration.Permit( RemoveTrigger.Absent, RemoveState.Absent );
            _ = stateConfiguration.Permit( RemoveTrigger.Initial, RemoveState.Initial );
            _ = stateConfiguration.Permit( RemoveTrigger.Insert, RemoveState.Present );
            _ = stateConfiguration.Ignore( RemoveTrigger.Release );
            _ = stateConfiguration.Ignore( RemoveTrigger.Remove );
            _ = stateConfiguration.Ignore( RemoveTrigger.RemoveFailed );
        }

        #endregion

        #region " COMMANDs "

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( RemoveTrigger.Initial );
        }

        /// <summary> Absents this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Absent()
        {
            this.Fire( RemoveTrigger.Absent );
        }

        /// <summary> Inserts this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Insert()
        {
            this.Fire( RemoveTrigger.Insert );
        }

        /// <summary> Releases this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Release()
        {
            this.Fire( RemoveTrigger.Release );
        }

        /// <summary> Removes the part. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Remove()
        {
            this.Fire( RemoveTrigger.Remove );
        }

        /// <summary> Failures this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void RemoveFailure()
        {
            this.Fire( RemoveTrigger.RemoveFailed );
        }

        #endregion

    }

    /// <summary> Values that represent remove states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum RemoveState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the absent option. </summary>
        [Description( "Absent" )]
        Absent,

        /// <summary> An enum constant representing the present option. </summary>
        [Description( "Part Present" )]
        Present,

        /// <summary> An enum constant representing the released option. </summary>
        [Description( "Waiting to remove" )]
        Released,

        /// <summary> An enum constant representing the removed option. </summary>
        [Description( "Part Removed" )]
        Removed,

        /// <summary> An enum constant representing the not removed option. </summary>
        [Description( "Part Not Removed" )]
        NotRemoved
    }

    /// <summary> Values that represent remove triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum RemoveTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the absent option. </summary>
        [Description( "Absent" )]
        Absent,

        /// <summary> An enum constant representing the insert option. </summary>
        [Description( "Insert" )]
        Insert,

        /// <summary> An enum constant representing the release option. </summary>
        [Description( "Release" )]
        Release,

        /// <summary> An enum constant representing the remove option. </summary>
        [Description( "Remove" )]
        Remove,

        /// <summary> An enum constant representing the remove failed option. </summary>
        [Description( "Remove Failed" )]
        RemoveFailed
    }
}