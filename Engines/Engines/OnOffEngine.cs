using System.ComponentModel;

using Stateless;

namespace isr.Automata.Finite.Engines
{

    /// <summary> Implements a finite automata for a On/Off process. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public class OnOffEngine : EngineBase<OnOffState, OnOffTrigger>
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="initialState"> State of the initial. </param>
        public OnOffEngine( string name, OnOffState initialState ) : base( name, new StateMachine<OnOffState, OnOffTrigger>( initialState ) )
        {
            StateMachine<OnOffState, OnOffTrigger>.StateConfiguration stateConfiguration;
            stateConfiguration = this.StateMachine.Configure( OnOffState.Initial );
            _ = stateConfiguration.Ignore( OnOffTrigger.Initial );
            _ = stateConfiguration.Permit( OnOffTrigger.Off, OnOffState.Off );
            _ = stateConfiguration.Permit( OnOffTrigger.On, OnOffState.On );
            this.EngagedStates.Add( OnOffState.On );
            stateConfiguration = this.StateMachine.Configure( OnOffState.On );
            _ = stateConfiguration.Permit( OnOffTrigger.Initial, OnOffState.Initial );
            _ = stateConfiguration.Permit( OnOffTrigger.Off, OnOffState.Off );
            _ = stateConfiguration.Ignore( OnOffTrigger.On );
            this.EngagedStates.Add( OnOffState.Off );
            stateConfiguration = this.StateMachine.Configure( OnOffState.Off );
            _ = stateConfiguration.Permit( OnOffTrigger.Initial, OnOffState.Initial );
            _ = stateConfiguration.Ignore( OnOffTrigger.Off );
            _ = stateConfiguration.Permit( OnOffTrigger.On, OnOffState.On );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="name"> The name. </param>
        public OnOffEngine( string name ) : this( name, OnOffState.Initial )
        {
        }

        #endregion

        #region " COMMANDs "

        /// <summary>   Ons this object. </summary>
        /// <remarks>   David, 2020-10-01. </remarks>
        public void On()
        {
            this.Fire( OnOffTrigger.On );
        }

        /// <summary> Offs this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Off()
        {
            this.Fire( OnOffTrigger.Off );
        }

        /// <summary> Initials this object. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public void Initial()
        {
            this.Fire( OnOffTrigger.Initial );
        }

        #endregion

    }

    /// <summary> Values that represent on off states. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffState
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the on] option. </summary>
        [Description( "On" )]
        On,

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off
    }

    /// <summary> Values that represent on off triggers. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public enum OnOffTrigger
    {

        /// <summary> An enum constant representing the initial option. </summary>
        [Description( "Initial" )]
        Initial,

        /// <summary> An enum constant representing the on] option. </summary>
        [Description( "On" )]
        On,

        /// <summary> An enum constant representing the off option. </summary>
        [Description( "Off" )]
        Off
    }
}
