# Changelog
All notable changes to these libraries will be documented in this file. 
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [5.11.8139] - 2022-04-14
* Use Stateless package. 

## [5.1.8066] - 2022-01-31
* Change target library to 4.7.2 and from 4.0 to 2.0.

## [5.1.7626] - 2020-11-17
* Converted to C#. Uses Stateless 5.1.5. 
* Changed versions to match major and minor Stateless revisions.

## [5.1.7610] - 2020-10-31
* Converted to C#. Uses Stateless 5.1.3.

## [4.2.7177] - 2019-08-26
* Fixes a few unit tests for missing arguments or
function parentheses. All 133 unit tests passed..

## [4.2.7094] - 2019-06-04
* Updated to Stateless 4.2.1 release 2019-05-11.

## [4.0.6667] - 2018-04-03
* 2018 release.

## [4.0.6535] - 2017-11-22
* Updates to Stateless 4.0.1.

## [2.0.6320] - 2016-04-21
* Uses Stateless 2.0
* Converted from * [Moore Automata](https://www.bitbucket.org/davidhary/vs.automata.moore)

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[5.11.8139]: https://www.bitbucket.org/davidhary/vs.automata
