using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Stateless")]
[assembly: AssemblyProduct("Stateless")]
[assembly: AssemblyCopyright("Copyright © Stateless Contributors 2009-2016")]

[assembly: AssemblyVersion( "5.1.3.0" )]

[assembly: InternalsVisibleTo("Stateless.Tests, PublicKey=" +
    "0024000004800000940000000602000000240000525341310004000001000100abebe581d3e209" +
    "862aedf4c60416e3e70329c3f1a962cc7f67460d0b48449f87d19b6c54680e465f9bf6c238c71e" +
    "7bf7e14efff5c348f3f60220ad7c43cc2b9bd18f945963869a23712d61a808f9b3fc6eef26fe60" +
    "1cb5b7ce19cf788ce198362a9a49a2a3c2dd44faffd39870308ecb42d0eb4dd51e16f6accf0d37" +
    "82fbfab1")]

[assembly: InternalsVisibleTo("Stateless.XunitTests, PublicKey=" +
    "0024000004800000940000000602000000240000525341310004000001000100abebe581d3e209" +
    "862aedf4c60416e3e70329c3f1a962cc7f67460d0b48449f87d19b6c54680e465f9bf6c238c71e" +
    "7bf7e14efff5c348f3f60220ad7c43cc2b9bd18f945963869a23712d61a808f9b3fc6eef26fe60" +
    "1cb5b7ce19cf788ce198362a9a49a2a3c2dd44faffd39870308ecb42d0eb4dd51e16f6accf0d37" +
    "82fbfab1")]

[assembly: NeutralResourcesLanguageAttribute("en")]
[assembly: CLSCompliant(true)]


// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
