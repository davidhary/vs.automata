## ISR Automata Finite<sub>&trade;</sub>: Finite State Machine Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*6.2.7177 2019-08-26*  
Fixes a few unit tests for missing arguments or
function parentheses. All 133 unit tests passed.. All VB.Net examples
passed.

*6.2.7094 2019-06-04*  
Updated to Stateless 4.2.1 release 2019-05-11.

*6.2.6667 2018-04-03*  
2018 release.

*6.1.6535 2017-11-22*  
Updates to Stateless 4.0.1.

*6.0.6320 2016-04-21*  
Uses Stateless.

\(C\) 2006 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)  
[Moore State Machine in C\#](http://www.codeproject.com/KB/recipes/MooreMachine.aspx)  
[Stateless](https://github.com/dotnet-state-machine/stateless.git)
