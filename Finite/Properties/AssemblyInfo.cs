﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Automata.Finite.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Automata.Finite.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Automata.Finite.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
