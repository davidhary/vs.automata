Imports System.ComponentModel
Imports Stateless
Imports isr.Core
Imports isr.Core.Models

''' <summary> A State Machine Engine base class. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public MustInherit Class EngineBase(Of TState, TTrigger)
    Inherits ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTOR "

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="stateMachine"> The state machine. </param>
    Protected Sub New(ByVal name As String, stateMachine As StateMachine(Of TState, TTrigger))
        MyBase.New
        Me.Name = name
        Me.StateMachine = stateMachine
        Me.StateMachine.OnTransitioned(Sub(t) Me.OnTransitioned(t))
        Me.EngineFailedNotifier = New ActionNotifier(Of IO.ErrorEventArgs)
        Me._SlimLock = New Threading.ReaderWriterLockSlim()
        Me.EngagedStates = New ObjectModel.Collection(Of TState)
    End Sub

#Region " Disposable Support "

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._SlimLock IsNot Nothing Then Me._SlimLock.Dispose() : Me._SlimLock = Nothing
            End If
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region
#End Region

#Region " STATE MACHINE "

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public ReadOnly Property Name As String

    ''' <summary> Gets or sets the state machine. </summary>
    ''' <value> The state machine. </value>
    Public ReadOnly Property StateMachine As StateMachine(Of TState, TTrigger)

    ''' <summary> The slim lock. </summary>
    Private _SlimLock As Threading.ReaderWriterLockSlim

    ''' <summary> True if stop requested. </summary>
    Private _StopRequested As Boolean

    ''' <summary> Gets or sets the StopRequested. </summary>
    ''' <value> The stop requested. </value>
    Public Property StopRequested As Boolean
        Get
            Me._SlimLock.EnterReadLock()
            Try
                Return Me._StopRequested
            Finally
                Me._SlimLock.ExitReadLock()
            End Try
        End Get
        Set(ByVal value As Boolean)
            Me._SlimLock.EnterWriteLock()
            Try
                Me._StopRequested = value
            Finally
                Me._SlimLock.ExitWriteLock()
            End Try
        End Set
    End Property

    ''' <summary> The stopping stop watch. </summary>
    Private ReadOnly _StoppingStopWatch As New Stopwatch

    ''' <summary> The stopping timeout. </summary>
    Private _StoppingTimeout As TimeSpan

    ''' <summary> Begins a stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub BeginStopRequest(ByVal timeout As TimeSpan)
        Me.StopRequested = True
        Me._StoppingStopWatch.Restart()
        Me._StoppingTimeout = timeout
        ' get going 
        Me.ProcessStopRequest()
    End Sub

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Overridable Sub ProcessStopRequest()
    End Sub

    ''' <summary> Determines if we stopping timeout elapsed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function StoppingTimeoutElapsed() As Boolean
        Return Me._StoppingStopWatch.Elapsed > Me._StoppingTimeout
    End Function

    ''' <summary> Ends stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub EndStopRequest()
        Me.StopRequested = False
        Me._StoppingStopWatch.Stop()
    End Sub

#End Region

#Region " SPECIAL STATES "

    ''' <summary> Gets the engaged states. </summary>
    ''' <remarks>
    ''' Engaged states are used to tag states which require the state machine special action for
    ''' disengagement. For example, if the state machine monitors part presence, all states where the
    ''' part is present could be tagged as 'Engaged' indicating that the part must be removed before
    ''' the state machine can be declared as disengaged.
    ''' </remarks>
    ''' <value> The active states. </value>
    Public ReadOnly Property EngagedStates As ObjectModel.Collection(Of TState)

    ''' <summary>
    ''' Gets the sentinel indicating of the engine is engaged in one of the engaged states.
    ''' </summary>
    ''' <value> The engaged. </value>
    Public ReadOnly Property Engaged As Boolean
        Get
            Return Me.EngagedStates.Contains(Me.StateMachine.State)
        End Get
    End Property

#End Region

#Region " STATE MACHINE INFO "

    ''' <summary> Gets the current state. </summary>
    ''' <value> The current state. </value>
    Public ReadOnly Property CurrentState As TState
        Get
            Return Me.StateMachine.State
        End Get
    End Property

    ''' <summary> Report state change. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    Protected Overridable Sub OnTransitioned(ByVal transition As StateMachine(Of TState, TTrigger).Transition)
        Me.LastTransition = transition
        Me.NotifyPropertyChanged(NameOf(Me.CurrentState))
    End Sub

    ''' <summary> The last transition. </summary>
    Private _LastTransition As StateMachine(Of TState, TTrigger).Transition

    ''' <summary> Gets or sets the last transition. </summary>
    ''' <value> The last transition. </value>
    Public Property LastTransition As StateMachine(Of TState, TTrigger).Transition
        Get
            Return Me._LastTransition
        End Get
        Set(value As StateMachine(Of TState, TTrigger).Transition)
            Me._LastTransition = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets information describing the state machine. </summary>
    ''' <value> Information describing the state machine. </value>
    Public ReadOnly Property StateMachineInfo As StateMachineInfo
        Get
            Return If(Me.LastTransition Is Nothing,
                New StateMachineInfo(Me.CurrentState.ToString),
                New StateMachineInfo(Me.CurrentState.ToString, Me.LastTransition.Source.ToString,
                                           Me.LastTransition.Destination.ToString, Me.LastTransition.Trigger.ToString))
        End Get
    End Property


#End Region

#Region " ENGINE FAILED NOTIFICATIONS "

    ''' <summary> Asynchronously reports engine failure to all registered delegates. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="e"> Specifies the <see cref="System.EventArgs">system event arguments</see> </param>
    Public Overridable Async Sub OnEngineFailedAsync(ByVal e As System.IO.ErrorEventArgs)
        Await Me.EngineFailedNotifier.InvokeAsync(e)
    End Sub

    ''' <summary> Synchronously reports engine failure to all registered delegates. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="e"> Specifies the <see cref="System.EventArgs">system event arguments</see> </param>
    Public Overridable Sub OnEngineFailed(ByVal e As System.IO.ErrorEventArgs)
        Me.EngineFailedNotifier.SyncInvoke(e)
    End Sub

    ''' <summary> Reports engine failure to all registered delegates. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="exception"> The exception. </param>
    Public Overridable Sub OnEngineFailed(ByVal exception As Exception)
        Me.OnEngineFailed(exception, "State machine {0} failed.", Me.Name)
    End Sub

    ''' <summary> Reports engine failure to all registered delegates. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    Public Overridable Sub OnEngineFailed(ByVal exception As Exception, ByVal detailsFormat As String, ByVal ParamArray args() As Object)
        Me.OnEngineFailed(New System.IO.ErrorEventArgs(New EngineException(Me.StateMachineInfo, exception, detailsFormat, args)))
    End Sub

    ''' <summary> Raises the Failed event. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="args">          A variable-length parameters list containing arguments. </param>
    Public Overridable Sub OnEngineFailed(ByVal detailsFormat As String, ByVal ParamArray args() As Object)
        Me.OnEngineFailed(New System.IO.ErrorEventArgs(New EngineException(Me.StateMachineInfo, detailsFormat, args)))
    End Sub

#End Region

#Region " ENGINE FAILURE NOTIFIER "

    ''' <summary> Gets or sets the on engine failed notifier. </summary>
    ''' <value> The on engine failed event. </value>
    Private ReadOnly Property EngineFailedNotifier As ActionNotifier(Of IO.ErrorEventArgs)

    ''' <summary>
    ''' Registers a callback that will be invoked every time the state machine receives an engine
    ''' failed notification.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="engineFailedAction"> The action to execute, accepting the details of the event
    '''                                   arguments. </param>
    Public Sub RegisterEngineFailureAction(ByVal engineFailedAction As Action(Of System.IO.ErrorEventArgs))
        If engineFailedAction Is Nothing Then Throw New ArgumentNullException(NameOf(engineFailedAction))
        Me.EngineFailedNotifier.Register(engineFailedAction)
    End Sub

    ''' <summary> Unregisters the engine failure action described by engineFailedAction. </summary>
    ''' <remarks> David, 2020-08-26. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="engineFailedAction"> The engine failed action. </param>
    Public Sub UnregisterEngineFailureAction(ByVal engineFailedAction As Action(Of System.IO.ErrorEventArgs))
        If engineFailedAction Is Nothing Then Throw New ArgumentNullException(NameOf(engineFailedAction))
        Me.EngineFailedNotifier.Unregister(engineFailedAction)
    End Sub

#End Region

#Region " FIRE ASYNC "

    ''' <summary> Gets or sets the using fire asynchronous. </summary>
    ''' <value> The using fire asynchronous. </value>
    Public Property UsingFireAsync As Boolean

    ''' <summary> Fires the given trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="trigger"> The trigger. </param>
    Public Sub Fire(ByVal trigger As TTrigger)
        If Me.UsingFireAsync Then
            Me.StateMachine.FireAsync(trigger)
        Else
            Me.StateMachine.Fire(trigger)
        End If
    End Sub

#End Region

End Class

