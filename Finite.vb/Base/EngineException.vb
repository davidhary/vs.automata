''' <summary> State machine exception . </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2020-08-26 </para>
''' </remarks>
<Serializable()>
Public Class EngineException
    Inherits isr.Core.ExceptionBase

#Region " STANDARD CONSTRUCTORS "

    ''' <summary> Gets or sets the default message. </summary>
    ''' <value> The default message. </value>
    Private Shared ReadOnly Property DefaultMessage As String = "State machine engine exception"

    ''' <summary> Initializes a new instance of the <see cref="EngineException"/> class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub New()
        Me.New(EngineException.DefaultMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="EngineException" /> class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="EngineException" /> class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the class with serialized data. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then Return
        Me._EngineErrorInfo = CType(info.GetValue($"{NameOf(Finite.StateMachineInfo)}", GetType(StateMachineInfo)), StateMachineInfo)
    End Sub

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData" /> method to serialize custom values.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="info">    The <see cref="Runtime.Serialization.SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> The <see cref="Runtime.Serialization.StreamingContext">streaming
    '''                        context</see> for the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        If info Is Nothing Then Return
        info.AddValue($"{NameOf(Finite.StateMachineInfo)}", Me.EngineErrorInfo)
        MyBase.GetObjectData(info, context)
    End Sub


#End Region

#Region " CUSTOM CONSTRUCTORS "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="enginerErrorInfo"> Information describing the enginer error. </param>
    Public Sub New(ByVal enginerErrorInfo As StateMachineInfo)
        Me.New(EngineException.DefaultMessage)
        Me._EngineErrorInfo = enginerErrorInfo
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="message">         The message. </param>
    ''' <param name="engineErrorInfo"> Information describing the engine error. </param>
    Public Sub New(ByVal message As String, ByVal engineErrorInfo As StateMachineInfo)
        Me.New(message)
        Me._EngineErrorInfo = engineErrorInfo
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="engineErrorInfo"> Information describing the engine error. </param>
    ''' <param name="format">          Describes the format to use. </param>
    ''' <param name="args">            A variable-length parameters list containing arguments. </param>
    Public Sub New(ByVal engineErrorInfo As StateMachineInfo, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), engineErrorInfo)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="engineErrorInfo"> Information describing the engine error. </param>
    ''' <param name="innerException">  The inner exception. </param>
    Public Sub New(ByVal engineErrorInfo As StateMachineInfo, ByVal innerException As System.Exception)
        Me.New(EngineException.DefaultMessage, engineErrorInfo, innerException)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="message">         The message. </param>
    ''' <param name="engineErrorInfo"> Information describing the engine error. </param>
    ''' <param name="innerException">  The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal engineErrorInfo As StateMachineInfo, ByVal innerException As System.Exception)
        Me.New(message, innerException)
        Me._EngineErrorInfo = engineErrorInfo
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="engineErrorInfo"> Information describing the engine error. </param>
    ''' <param name="innerException">  The inner exception. </param>
    ''' <param name="format">          Describes the format to use. </param>
    ''' <param name="args">            A variable-length parameters list containing arguments. </param>
    Public Sub New(ByVal engineErrorInfo As StateMachineInfo, ByVal innerException As System.Exception, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), engineErrorInfo, innerException)
    End Sub

#End Region

#Region " ENGINE ERROR INFORMATION "

    ''' <summary> Gets or sets information describing the engine error. </summary>
    ''' <value> Information describing the engine error. </value>
    Public ReadOnly Property EngineErrorInfo As StateMachineInfo

    ''' <summary> Convert this object into a string representation. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> A String that represents this object. </returns>
    Public Overrides Function ToString() As String
        Return If(Me.EngineErrorInfo Is Nothing, MyBase.ToString, Me.EngineErrorInfo.ToString)
    End Function

    ''' <summary> Adds an exception data. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="exception"> The exception receiving the added data. </param>
    Public Sub AddExceptionData(ByVal exception As Exception)
        If exception Is Nothing Then Return
        Me.EngineErrorInfo.AddExceptionData(exception)
    End Sub


#End Region

End Class

