''' <summary> A state machine info class. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-08-26 </para>
''' </remarks>
Public Class StateMachineInfo

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    Public Sub New()
        MyBase.New
        Me.CurrentState = String.Empty
        Me.LastTransactionDestination = String.Empty
        Me.LastTransactionSource = String.Empty
        Me.LastTransactionTrigger = String.Empty
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="currentState"> The current state. </param>
    Public Sub New(ByVal currentState As String)
        Me.New()
        Me.CurrentState = currentState
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <param name="currentState">               The current state. </param>
    ''' <param name="lastTransactionSource">      The last transaction source. </param>
    ''' <param name="lastTransactionDestination"> The last transaction destination. </param>
    ''' <param name="lastTransactionTrigger">     The last transaction trigger. </param>
    Public Sub New(ByVal currentState As String, ByVal lastTransactionSource As String,
                   ByVal lastTransactionDestination As String, ByVal lastTransactionTrigger As String)
        Me.New
        Me.CurrentState = currentState
        Me.LastTransactionSource = lastTransactionSource
        Me.LastTransactionDestination = lastTransactionDestination
        Me.LastTransactionTrigger = lastTransactionTrigger
    End Sub

    ''' <summary> The success. </summary>

    Private Shared _Success As StateMachineInfo

    ''' <summary> Gets the success. </summary>
    ''' <value> The success. </value>
    Public Shared ReadOnly Property Success As StateMachineInfo
        Get
            If StateMachineInfo._Success Is Nothing Then
                StateMachineInfo._Success = New StateMachineInfo()
            End If
            Return StateMachineInfo._Success
        End Get
    End Property

    ''' <summary> Gets or sets the current state. </summary>
    ''' <value> The current state. </value>
    Public ReadOnly Property CurrentState As String

    ''' <summary> Gets or sets the last transaction source. </summary>
    ''' <value> The last transaction source. </value>
    Public ReadOnly Property LastTransactionSource As String

    ''' <summary> Gets or sets the last transaction destination. </summary>
    ''' <value> The last transaction destination. </value>
    Public ReadOnly Property LastTransactionDestination As String

    ''' <summary> Gets or sets the last transaction trigger. </summary>
    ''' <value> The last transaction trigger. </value>
    Public ReadOnly Property LastTransactionTrigger As String

    ''' <summary> Adds an exception data. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="exception"> The exception receiving the added data. </param>
    Public Sub AddExceptionData(ByVal exception As System.Exception)
        If Not (exception Is Nothing OrElse String.IsNullOrEmpty(Me.CurrentState)) Then
            Dim count As Integer = exception.Data.Count
            exception.Data.Add($"{count}-{NameOf(StateMachineInfo.CurrentState)}", Me.CurrentState)
            If Not String.IsNullOrEmpty(Me.LastTransactionSource) Then
                exception.Data.Add($"{count}-{NameOf(StateMachineInfo.LastTransactionSource)}", Me.LastTransactionSource)
                exception.Data.Add($"{count}-{NameOf(StateMachineInfo.LastTransactionDestination)}", Me.LastTransactionDestination)
                exception.Data.Add($"{count}-{NameOf(StateMachineInfo.LastTransactionTrigger)}", Me.LastTransactionTrigger)
            End If
        End If
    End Sub

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-08-27. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overloads Function ToString() As String
        If String.IsNullOrEmpty(Me.CurrentState) Then
            Return MyBase.ToString()
        Else
            Dim stringBuilder As New System.Text.StringBuilder
            Dim builder As New System.Text.StringBuilder(MyBase.ToString)
            builder.AppendLine("State machine engine Info:")
            builder.AppendLine($"{NameOf(StateMachineInfo.CurrentState)}: {Me.CurrentState}")
            If Not String.IsNullOrEmpty(Me.LastTransactionSource) Then
                builder.AppendLine($"{NameOf(StateMachineInfo.LastTransactionSource)}: {Me.LastTransactionSource}")
                builder.AppendLine($"{NameOf(StateMachineInfo.LastTransactionDestination)}: {Me.LastTransactionDestination}")
                builder.Append($"{NameOf(StateMachineInfo.LastTransactionTrigger)}: {Me.LastTransactionTrigger}")
            End If
            Return stringBuilder.ToString
        End If
    End Function


End Class
