﻿using System;
using System.Diagnostics;
using isr.Core;
using isr.Finite.Automata.Engines.ExceptionExtensions;

namespace isr.Finite.Automata.Engines.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    internal sealed partial class MyApplication
    {

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Finite Automata Bimanual Toggle";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Finite Automata Bimanual Toggle";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Automata.Finite.Bimanual.Toggle";

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 516;

        /// <summary> Gets or sets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public static TraceMessagesQueue UnpublishedTraceMessages { get; private set; } = new TraceMessagesQueue();

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public static void LogUnpublishedException(string activity, Exception exception)
        {
            LogUnpublishedException(new TraceMessage(TraceEventType.Error, TraceEventId, $@"Exception {activity};
logged to: {MyProject.Application.Log.DefaultFileLogWriter.FullLogFileName};
{exception.ToFullBlownString()}"));
        }

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="message"> The message. </param>
        public static void LogUnpublishedException(TraceMessage message)
        {
            if (message is object)
            {
                UnpublishedTraceMessages.Enqueue(message);
                MyProject.Application.Log.DefaultFileLogWriter.WriteLine(message.ToString());
                MyProject.Application.Log.DefaultFileLogWriter.Flush();
            }
        }
    }
}