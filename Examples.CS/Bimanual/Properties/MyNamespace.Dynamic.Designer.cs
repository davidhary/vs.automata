﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Finite.Automata.Engines.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public MyForm m_MyForm;

            public MyForm MyForm
            {
                [DebuggerHidden]
                get
                {
                    m_MyForm = Create__Instance__(m_MyForm);
                    return m_MyForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_MyForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_MyForm);
                }
            }
        }
    }
}