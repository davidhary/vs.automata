﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Finite.Automata.Engines.My.MyApplication.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Finite.Automata.Engines.My.MyApplication.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Finite.Automata.Engines.My.MyApplication.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
