﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneCallExample
{
    partial class Program
    {
        static void Main( string[] args )
        {
            Program.RunPhoneCall();
            Console.WriteLine( "Done; Press any key..." );
            Console.ReadKey( true );

        }
    }
}
