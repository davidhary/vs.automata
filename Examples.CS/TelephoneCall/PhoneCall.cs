using System;

using Stateless;
using Stateless.Graph;

namespace PhoneCallExample
{
    public class PhoneCall
    {
        enum Trigger
        {
            CallDialed,
            CallConnected,
            LeftMessage,
            PlacedOnHold,
            TakenOffHold,
            PhoneHurledAgainstWall,
            MuteMicrophone,
            UnmuteMicrophone,
            SetVolume
        }

        enum State
        {
            OffHook,
            Ringing,
            Connected,
            OnHold,
            PhoneDestroyed
        }

        State _State = State.OffHook;

        StateMachine<State, Trigger> _Machine;
        StateMachine<State, Trigger>.TriggerWithParameters<int> _SetVolumeTrigger;

        StateMachine<State, Trigger>.TriggerWithParameters<string> _SetCalleeTrigger;

        string _Caller;

        string _Callee;

        public PhoneCall( string caller )
        {
            this._Caller = caller;
            this._Machine = new StateMachine<State, Trigger>( () => this._State, s => this._State = s );

            this._SetVolumeTrigger = this._Machine.SetTriggerParameters<int>( Trigger.SetVolume );
            this._SetCalleeTrigger = this._Machine.SetTriggerParameters<string>( Trigger.CallDialed );

            this._Machine.Configure( State.OffHook )
                .Permit( Trigger.CallDialed, State.Ringing );

            this._Machine.Configure( State.Ringing )
                .OnEntryFrom( this._SetCalleeTrigger, callee => this.OnDialed( callee ), "Caller number to call" )
                .Permit( Trigger.CallConnected, State.Connected );

            this._Machine.Configure( State.Connected )
                .OnEntry( t => this.StartCallTimer() )
                .OnExit( t => this.StopCallTimer() )
                .InternalTransition( Trigger.MuteMicrophone, t => this.OnMute() )
                .InternalTransition( Trigger.UnmuteMicrophone, t => this.OnUnmute() )
                .InternalTransition<int>( this._SetVolumeTrigger, ( volume, t ) => this.OnSetVolume( volume ) )
                .Permit( Trigger.LeftMessage, State.OffHook )
                .Permit( Trigger.PlacedOnHold, State.OnHold );

            this._Machine.Configure( State.OnHold )
                .SubstateOf( State.Connected )
                .Permit( Trigger.TakenOffHold, State.Connected )
                .Permit( Trigger.PhoneHurledAgainstWall, State.PhoneDestroyed );
        }

        void OnSetVolume( int volume )
        {
            Console.WriteLine( "Volume set to " + volume + "!" );
        }

        void OnUnmute()
        {
            Console.WriteLine( "Microphone unmuted!" );
        }

        void OnMute()
        {
            Console.WriteLine( "Microphone muted!" );
        }

        void OnDialed( string callee )
        {
            this._Callee = callee;
            Console.WriteLine( "[Phone Call] placed for : [{0}]", this._Callee );
        }

        void StartCallTimer()
        {
            Console.WriteLine( "[Timer:] Call started at {0}", DateTimeOffset.Now );
        }

        void StopCallTimer()
        {
            Console.WriteLine( "[Timer:] Call ended at {0}", DateTimeOffset.Now );
        }

        public void Mute()
        {
            this._Machine.Fire( Trigger.MuteMicrophone );
        }

        public void Unmute()
        {
            this._Machine.Fire( Trigger.UnmuteMicrophone );
        }

        public void SetVolume( int volume )
        {
            this._Machine.Fire( this._SetVolumeTrigger, volume );
        }

        public void Print()
        {
            Console.WriteLine( "[{1}] placed call and [Status:] {0}", this._Machine.State, this._Caller );
        }

        public void Dialed( string callee )
        {
            this._Machine.Fire( this._SetCalleeTrigger, callee );
        }

        public void Connected()
        {
            this._Machine.Fire( Trigger.CallConnected );
        }

        public void Hold()
        {
            this._Machine.Fire( Trigger.PlacedOnHold );
        }

        public void Resume()
        {
            this._Machine.Fire( Trigger.TakenOffHold );
        }

        public string ToDotGraph()
        {
            return UmlDotGraph.Format( this._Machine.GetInfo() );
        }
    }
}
