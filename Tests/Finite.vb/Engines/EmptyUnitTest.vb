''' <summary> An empty unit test. </summary>
''' <remarks> David, 2020-10-01. </remarks>
<TestClass()> Public Class EmptyUnitTest

    ''' <summary> (Unit Test Method) empty test method. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()> Public Sub EmptyTestMethod()
    End Sub

End Class
