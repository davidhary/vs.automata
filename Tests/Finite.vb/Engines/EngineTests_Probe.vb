
Imports FluentAssertions

Imports isr.Automata.Finite.Engines

Imports Stateless

Partial Public Class EngineTests

    ''' <summary> Gets or sets the probe engine. </summary>
    ''' <value> The probe engine. </value>
    Public ReadOnly Property ProbeEngine As isr.Automata.Finite.Engines.ProbeEngine

    ''' <summary> Assign Probe automaton. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignProbeAutomaton(ByVal value As isr.Automata.Finite.Engines.ProbeEngine)
        If Me.ProbeEngine IsNot Nothing Then
            Me._ProbeEngine = Nothing
        End If
        If value IsNot Nothing Then
            Me._ProbeEngine = value
            If Me.ProbeEngine.UsingFireAsync Then
                Me.ProbeEngine.StateMachine.OnTransitionedAsync(Function(t As StateMachine(Of ProbeState, ProbeTrigger).Transition)
                                                                    Return Task.Run(Sub() Me.OnTransitioned(t))
                                                                End Function)
            Else
                Me.ProbeEngine.StateMachine.OnTransitioned(Sub(t) Me.OnTransitioned(t))
            End If
        End If
    End Sub

    ''' <summary> Handles the state entry actions. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTransitioned(ByVal transition As StateMachine(Of ProbeState, ProbeTrigger).Transition)
        Dim activity As String
        If transition Is Nothing Then
            activity = $"Entering the {Me.ProbeEngine.StateMachine.State} state"
            TestInfo.TraceMessage(activity)
            Me.OnActivated(Me.ProbeEngine.StateMachine.State)
        Else
            activity = $"transitioning {Me.ProbeEngine.Name}: {transition.Source} --> {transition.Destination}"
            TestInfo.TraceMessage(activity)
            If transition.IsReentry Then
                Me.OnReentry(transition.Destination)
            Else
                Me.OnExit(transition.Source)
                Me.OnActivated(transition.Destination)
            End If
        End If
    End Sub

    ''' <summary> Executes the 'exit' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="source"> Source for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnExit(ByVal source As ProbeState)
        Select Case source
            Case ProbeState.Lifting
        End Select
    End Sub

    ''' <summary> Executes the 'reentry' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnReentry(ByVal destination As ProbeState)
        Select Case destination
            Case ProbeState.Lifting
                ' read temperatures
        End Select
    End Sub

    ''' <summary> Executes the 'activated' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnActivated(ByVal destination As ProbeState)
        Select Case destination
            Case ProbeState.Lifting
                ' start monitoring temperatures
        End Select
    End Sub

    ''' <summary> (Unit Test Method) tests probe engine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()>
    Public Sub ProbeEngineTest()

        TestInfo.TraceMessage("Begin Probe engine test")
        Using Automata As New ProbeEngine("Probe Sequencer")

            Me.AssignProbeAutomaton(Automata)
            Me.ProbeEngine.Should.NotBeNull("Engine was assigned")
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Initial, "this is the expected initial state")

            TestInfo.TraceMessage("Lowering the probe; engine should start monitoring resistance")
            Me.ProbeEngine.Lower()
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Lowering, "this is the lowering state")

            TestInfo.TraceMessage("Contact detected, engine should start probing")
            Me.ProbeEngine.Probe()
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Probing, "this is the expected at temp state")

            TestInfo.TraceMessage("Part measured, engine should start lifting")
            Me.ProbeEngine.Lift()
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Lifting, "this is the expected state after part is measured")

            TestInfo.TraceMessage("Probe lifted, engine should go to initial state")
            Me.ProbeEngine.Initial()
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Initial, "this is the expected state after lifting is done")

        End Using

    End Sub

    ''' <summary> (Unit Test Method) tests probe engine asynchronous. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()>
    Public Sub ProbeEngineAsyncTest()

        TestInfo.TraceMessage("Begin Probe engine test")
        Using automata As New ProbeEngine("Probe Sequencer")

            automata.UsingFireAsync = True
            Me.AssignProbeAutomaton(automata)
            Me.ProbeEngine.Should.NotBeNull("Engine was assigned")
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Initial, "this is the expected initial state")

            TestInfo.TraceMessage("Lowering the probe; engine should start monitoring resistance")
            Me.ProbeEngine.Lower()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Lowering, "this is the lowering state")

            TestInfo.TraceMessage("Contact detected, engine should start probing")
            Me.ProbeEngine.Probe()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Probing, "this is the expected at temp state")

            TestInfo.TraceMessage("Part measured, engine should start lifting")
            Me.ProbeEngine.Lift()
            isr.Automata.Finite.My.MyLibrary.Delay(100)

            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Lifting, "this is the expected state after part is measured")

            TestInfo.TraceMessage("Probe lifted, engine should go to initial state")
            Me.ProbeEngine.Initial()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.ProbeEngine.StateMachine.State.Should.Be(ProbeState.Initial, "this is the expected state after lifting is done")

        End Using

    End Sub

End Class

