
Imports FluentAssertions

Imports isr.Automata.Finite.Engines

Imports Stateless

Partial Public Class EngineTests

    ''' <summary> Gets or sets the seebeck engine. </summary>
    ''' <value> The seebeck engine. </value>
    Public ReadOnly Property SeebeckEngine As isr.Automata.Finite.Engines.SeebeckEngine

    ''' <summary> Assign Seebeck automaton. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignSeebeckAutomaton(ByVal value As isr.Automata.Finite.Engines.SeebeckEngine)
        If Me.SeebeckEngine IsNot Nothing Then
            Me._SeebeckEngine = Nothing
        End If
        If value IsNot Nothing Then
            Me._SeebeckEngine = value
            If Me.SeebeckEngine.UsingFireAsync Then
                Me.SeebeckEngine.StateMachine.OnTransitionedAsync(Function(t As StateMachine(Of SeebeckState, SeebeckTrigger).Transition)
                                                                      Return Task.Run(Sub() Me.OnTransitioned(t))
                                                                  End Function)
            Else
                Me.SeebeckEngine.StateMachine.OnTransitioned(Sub(t) Me.OnTransitioned(t))
            End If
        End If
    End Sub

    ''' <summary> Handles the state entry actions. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTransitioned(ByVal transition As StateMachine(Of SeebeckState, SeebeckTrigger).Transition)
        Dim activity As String
        If transition Is Nothing Then
            activity = $"Entering the {Me.SeebeckEngine.StateMachine.State} state"
            TestInfo.TraceMessage(activity)
            Me.OnActivated(Me.SeebeckEngine.StateMachine.State)
        Else
            activity = $"transitioning {Me.SeebeckEngine.Name}: {transition.Source} --> {transition.Destination}"
            TestInfo.TraceMessage(activity)
            If transition.IsReentry Then
                Me.OnReentry(transition.Destination)
            Else
                Me.OnExit(transition.Source)
                Me.OnActivated(transition.Destination)
            End If
        End If
    End Sub

    ''' <summary> Executes the 'exit' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="source"> Source for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnExit(ByVal source As SeebeckState)
        Select Case source
            Case SeebeckState.Soaking
                ' stop monitoring temperatures
        End Select
    End Sub

    ''' <summary> Executes the 'reentry' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnReentry(ByVal destination As SeebeckState)
        Select Case destination
            Case SeebeckState.Soaking
                ' read temperatures
        End Select
    End Sub

    ''' <summary> Executes the 'activated' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="destination"> Destination for the. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnActivated(ByVal destination As SeebeckState)
        Select Case destination
            Case SeebeckState.Soaking
                ' start monitoring temperatures
        End Select
    End Sub

    ''' <summary> (Unit Test Method) tests Seebeck engine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()>
    Public Sub SeebeckEngineTest()

        TestInfo.TraceMessage("Begin Seebeck engine test")
        Using automata As New SeebeckEngine("Seebeck Sequencer")

            Me.AssignSeebeckAutomaton(automata)

            Me.SeebeckEngine.Should.NotBeNull("Engine was assigned")
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Initial, "this is the expected initial state")

            TestInfo.TraceMessage("Waiting for substrate present; engine should start monitoring part present")
            Me.SeebeckEngine.AwaitPartPresent()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Inserting, "this is the expected awaiting for part present state")

            TestInfo.TraceMessage("Inserting a substrate; engine should start monitoring temperature")
            Me.SeebeckEngine.SoakPart()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Soaking, "this is the expected part present state")

            TestInfo.TraceMessage("Part at temp, engine should start contacting")
            Me.SeebeckEngine.ConnectPart()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Contacting, "this is the expected at temp state")

            TestInfo.TraceMessage("Part contacted, engine should start measuring")
            Me.SeebeckEngine.MeasurePart()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Measuring, "this is the expected state after part is contacted")

            TestInfo.TraceMessage("Part measured, engine should removing the part")
            Me.SeebeckEngine.RemovePart()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Removing, "this is the expected state after part is measured")

            TestInfo.TraceMessage("Part removed, engine should show party not present")
            Me.SeebeckEngine.Initial()
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Initial, "this is the expected state after part is removed")

        End Using

    End Sub

    ''' <summary> (Unit Test Method) tests seebeck engine asynchronous. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()>
    Public Sub SeebeckEngineAsyncTest()

        TestInfo.TraceMessage("Begin Seebeck engine test")
        Using automata As New SeebeckEngine("Seebeck Sequencer")

            automata.UsingFireAsync = True
            Me.AssignSeebeckAutomaton(automata)

            Me.SeebeckEngine.Should.NotBeNull("Engine was assigned")
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Initial, "this is the expected initial state")

            TestInfo.TraceMessage("Waiting for substrate present; engine should start monitoring part present")
            Me.SeebeckEngine.AwaitPartPresent()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Inserting, "this is the expected awaiting for part present state")

            TestInfo.TraceMessage("Inserting a substrate; engine should start monitoring temperature")
            Me.SeebeckEngine.SoakPart()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Soaking, "this is the expected part present state")

            TestInfo.TraceMessage("Part at temp, engine should start contacting")
            Me.SeebeckEngine.ConnectPart()
            isr.Automata.Finite.My.MyLibrary.Delay(100)

            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Contacting, "this is the expected at temp state")

            TestInfo.TraceMessage("Part contacted, engine should start measuring")
            Me.SeebeckEngine.MeasurePart()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Measuring, "this is the expected state after part is contacted")

            TestInfo.TraceMessage("Part measured, engine should removing the part")
            Me.SeebeckEngine.RemovePart()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Removing, "this is the expected state after part is measured")

            TestInfo.TraceMessage("Part removed, engine should show party not present")
            Me.SeebeckEngine.Initial()
            isr.Automata.Finite.My.MyLibrary.Delay(100)
            Me.SeebeckEngine.StateMachine.State.Should.Be(SeebeckState.Initial, "this is the expected state after part is removed")

        End Using


    End Sub

End Class
