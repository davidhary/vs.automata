Imports FluentAssertions

Imports isr.Automata.Finite.Engines

Imports Stateless

Partial Public Class EngineTests

    ''' <summary> Gets or sets the soak engine. </summary>
    ''' <value> The soak engine. </value>
    Private ReadOnly Property SoakEngine As isr.Automata.Finite.Engines.SoakEngine

    ''' <summary> Assign soak engine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignSoakEngine(ByVal value As isr.Automata.Finite.Engines.SoakEngine)
        If Me.SoakEngine IsNot Nothing Then
            Me._SoakEngine = Nothing
        End If
        If value IsNot Nothing Then
            Me._SoakEngine = value
            Me.SoakEngine.RampTimeout = TimeSpan.Zero
            Me.SoakEngine.SoakDuration = TimeSpan.Zero
            Me.SoakEngine.AllowedOffStateDuration = TimeSpan.Zero
            Me.SoakEngine.AllowedOffStateCount = 0
            Me.SoakEngine.SoakCount = SoakEngineTestInfo.Get.SoakDurationCount
            Me.SoakEngine.Window = SoakEngineTestInfo.Get.Window
            Me.SoakEngine.Hysteresis = SoakEngineTestInfo.Get.Hysteresis
            Me.SoakEngine.SampleInterval = SoakEngineTestInfo.Get.SampleInterval
            Me.SoakEngine.TemperatureResolution = SoakEngineTestInfo.Get.Resolution
            Me.SoakEngine.StateMachine.OnTransitioned(Sub(t) Me.OnTransitioned(t))
            Me.OnEntered(Me.SoakEngine.StateMachine.State)
        End If
    End Sub

    ''' <summary> Handles the state entry actions. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTransitioned(ByVal transition As StateMachine(Of SoakState, SoakTrigger).Transition)
        If transition Is Nothing Then
            Me.OnEntered(Me.SoakEngine.CurrentState)
        Else
            TestInfo.TraceMessage($"{transition.Trigger} Triggered transition from {transition.Source} to {transition.Destination} state")
            Me.OnTransitioned(transition.Source, transition.Destination)
        End If
    End Sub

    ''' <summary> Handles the state transition actions. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="source">      Source for the. </param>
    ''' <param name="destination"> The state. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnTransitioned(ByVal source As SoakState, ByVal destination As SoakState)
        Select Case source
            Case SoakState.Engaged
                If destination = SoakState.HasSetpoint Then
                End If
        End Select
        Me.OnEntered(destination)
    End Sub

    ''' <summary> Executes the entered action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="destination"> The state. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Private Sub OnEntered(ByVal destination As SoakState)
        TestInfo.TraceMessage($"Entered the {destination} state")
        Select Case destination
            Case SoakState.AtTemp
        End Select
    End Sub

    ''' <summary> (Unit Test Method) tests soak engine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    <TestMethod()>
    Public Sub SoakEngineTest()

        TestInfo.TraceMessage("Begin soak engine test")
        Using Automata As New SoakEngine("Soak Sequencer")
            Me.AssignSoakEngine(Automata)

            Me.SoakEngine.Should.NotBeNull("Engine was assigned")
            Me.SoakEngine.CurrentState.Should.Be(SoakState.Idle, "this is the expected initial state")

            TestInfo.TraceMessage("Engaging the soak Engine")
            Me.SoakEngine.Engage()
            Me.SoakEngine.CurrentState.Should.Be(SoakState.Engaged, "this is the expected engaged state")

            TestInfo.TraceMessage("Setting the first setpoint")
            Me.SoakEngine.ApplySetpoint(SoakEngineTestInfo.Get.FirstSetpoint)
            Me.SoakEngine.IsAtSetpoint(SoakEngineTestInfo.Get.FirstSetpoint).Should.BeTrue(
                                                    $"{SoakEngineTestInfo.Get.FirstSetpoint} equals {Me.SoakEngine.Setpoint}")
            Me.SoakEngine.CurrentState.Should.Be(SoakState.HasSetpoint, "this is the expected setpoint state")

            Dim rnd As New Random(SoakEngineTestInfo.Get.Seed)
            Dim soakCount As Integer = 0

            Dim expectedState As SoakState = SoakState.AtTemp
            TestInfo.TraceMessage("Going from Ramp to At Temp")
            Do
                soakCount += 1
                expectedState = If(soakCount >= Me.SoakEngine.SoakCount, SoakState.AtTemp, SoakState.Soak)
                Me.SoakEngine.ApplyTemperature(SoakEngineTestInfo.Get.FirstSetpoint + 0.5 * rnd.NextDouble * SoakEngineTestInfo.Get.Window)
                Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")
            Loop While soakCount <= Me.SoakEngine.SoakCount + 2 AndAlso Me.SoakEngine.CurrentState = SoakState.Soak
            expectedState = SoakState.AtTemp
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")

            Dim offStateCount As Integer = 0
            offStateCount += 1
            TestInfo.TraceMessage("Going from At Temp to Ramp")
            expectedState = SoakState.Ramp
            Me.SoakEngine.ApplyTemperature(SoakEngineTestInfo.Get.FirstSetpoint +
                                              0.5 * (1.01 + rnd.NextDouble) * (SoakEngineTestInfo.Get.Window + SoakEngineTestInfo.Get.Hysteresis))
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {offStateCount } times")


            TestInfo.TraceMessage("Going from Ramp to Off Soak and ramp")
            soakCount = 1
            expectedState = SoakState.Soak
            Me.SoakEngine.ApplyTemperature(SoakEngineTestInfo.Get.FirstSetpoint + 0.5 * rnd.NextDouble * SoakEngineTestInfo.Get.Window)
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")

            soakCount += 1
            expectedState = SoakState.Ramp
            Me.SoakEngine.ApplyTemperature(SoakEngineTestInfo.Get.FirstSetpoint +
                                              0.5 * (1.01 + rnd.NextDouble) * (SoakEngineTestInfo.Get.Window + SoakEngineTestInfo.Get.Hysteresis))
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")

            soakCount = 0
            TestInfo.TraceMessage("Going from Ramp to at temp")
            Do
                soakCount += 1
                expectedState = If(soakCount >= Me.SoakEngine.SoakCount, SoakState.AtTemp, SoakState.Soak)
                Me.SoakEngine.ApplyTemperature(SoakEngineTestInfo.Get.FirstSetpoint + 0.5 * rnd.NextDouble * SoakEngineTestInfo.Get.Window)
                Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")
            Loop While soakCount <= Me.SoakEngine.SoakCount + 2 AndAlso Me.SoakEngine.CurrentState = SoakState.Soak
            expectedState = SoakState.AtTemp
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint applied {soakCount} times")

            TestInfo.TraceMessage("changing setpoint")
            expectedState = SoakState.Ramp
            Me.SoakEngine.ChangeSetpoint(SoakEngineTestInfo.Get.SecondSetpoint)
            Me.SoakEngine.CurrentState.Should.Be(expectedState,
                $"temperature {Me.SoakEngine.Temperature} {If(Me.SoakEngine.IsAtTemp, "within", "outside")} setpoint changed")

            TestInfo.TraceMessage("Terminating")
            expectedState = SoakState.Terminal
            Me.SoakEngine.Terminate()
            Me.SoakEngine.CurrentState.Should.Be(expectedState, $"soak sequence terminated")

            TestInfo.TraceMessage("Engaging")
            expectedState = SoakState.Engaged
            Me.SoakEngine.Engage()
            Me.SoakEngine.CurrentState.Should.Be(expectedState, $"soak sequence engaged")

            TestInfo.TraceMessage("Idling")
            expectedState = SoakState.Idle
            Me.SoakEngine.Disengage()
            Me.SoakEngine.CurrentState.Should.Be(expectedState, $"soak sequence idled")

        End Using

    End Sub

End Class
