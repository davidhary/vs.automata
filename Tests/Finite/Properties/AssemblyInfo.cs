﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Automata.FiniteTests.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Automata.FiniteTests.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Automata.FiniteTests.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
