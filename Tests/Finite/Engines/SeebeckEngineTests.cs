
using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using isr.Core;
using isr.Automata.Finite.Engines;
using Stateless;

namespace isr.Automata.FiniteTests
{
    /// <summary> This is a test class for Seebeck state machine automata. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    [TestClass()]
    public class SeebeckEngineTests
    {


        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Finite.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Gets or sets the seebeck engine. </summary>
        /// <value> The seebeck engine. </value>
        public SeebeckEngine SeebeckEngine { get; private set; }

        /// <summary> Assign Seebeck automaton. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignSeebeckAutomaton(SeebeckEngine value)
        {
            if ( this.SeebeckEngine is object)
            {
                this.SeebeckEngine = null;
            }

            if (value is object)
            {
                this.SeebeckEngine = value;
                if ( this.SeebeckEngine.UsingFireAsync)
                {
                    this.SeebeckEngine.StateMachine.OnTransitionedAsync((t) => Task.Run(() => this.OnTransitioned(t)));
                }
                else
                {
                    this.SeebeckEngine.StateMachine.OnTransitioned(t => this.OnTransitioned(t));
                }
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned(StateMachine<SeebeckState, SeebeckTrigger>.Transition transition)
        {
            string activity;
            if (transition is null)
            {
                activity = $"Entering the {this.SeebeckEngine.StateMachine.State} state";
                TestInfo.TraceMessage(activity);
                this.OnActivated( this.SeebeckEngine.StateMachine.State);
            }
            else
            {
                activity = $"transitioning {this.SeebeckEngine.Name}: {transition.Source} --> {transition.Destination}";
                TestInfo.TraceMessage(activity);
                if (transition.IsReentry)
                {
                    this.OnReentry(transition.Destination);
                }
                else
                {
                    this.OnExit(transition.Source);
                    this.OnActivated(transition.Destination);
                }
            }
        }

        /// <summary> Executes the 'exit' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="source"> Source for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private void OnExit(SeebeckState source)
        {
            switch (source)
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // stop monitoring temperatures
            }
        }

        /// <summary> Executes the 'reentry' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private void OnReentry(SeebeckState destination)
        {
            switch (destination)
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // read temperatures
            }
        }

        /// <summary> Executes the 'activated' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> Destination for the. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private void OnActivated(SeebeckState destination)
        {
            switch (destination)
            {
                case SeebeckState.Soaking:
                    {
                        break;
                    }
                    // start monitoring temperatures
            }
        }

        /// <summary> (Unit Test Method) tests Seebeck engine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void SeebeckEngineTest()
        {
            TestInfo.TraceMessage("Begin Seebeck engine test");
            using var automata = new SeebeckEngine( "Seebeck Sequencer" );
            this.AssignSeebeckAutomaton( automata );
            _ = this.SeebeckEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected initial state" );
            TestInfo.TraceMessage( "Waiting for substrate present; engine should start monitoring part present" );
            this.SeebeckEngine.AwaitPartPresent();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Inserting, "this is the expected awaiting for part present state" );
            TestInfo.TraceMessage( "Inserting a substrate; engine should start monitoring temperature" );
            this.SeebeckEngine.SoakPart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Soaking, "this is the expected part present state" );
            TestInfo.TraceMessage( "Part at temp, engine should start contacting" );
            this.SeebeckEngine.ConnectPart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Contacting, "this is the expected at temp state" );
            TestInfo.TraceMessage( "Part contacted, engine should start measuring" );
            this.SeebeckEngine.MeasurePart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Measuring, "this is the expected state after part is contacted" );
            TestInfo.TraceMessage( "Part measured, engine should removing the part" );
            this.SeebeckEngine.RemovePart();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Removing, "this is the expected state after part is measured" );
            TestInfo.TraceMessage( "Part removed, engine should show party not present" );
            this.SeebeckEngine.Initial();
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected state after part is removed" );
        }

        /// <summary> (Unit Test Method) tests seebeck engine asynchronous. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void SeebeckEngineAsyncTest()
        {
            TestInfo.TraceMessage("Begin Seebeck engine test");
            using var automata = new SeebeckEngine( "Seebeck Sequencer" ) {
                UsingFireAsync = true
            };
            this.AssignSeebeckAutomaton( automata );
            _ = this.SeebeckEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected initial state" );
            TestInfo.TraceMessage( "Waiting for substrate present; engine should start monitoring part present" );
            this.SeebeckEngine.AwaitPartPresent();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Inserting, "this is the expected awaiting for part present state" );
            TestInfo.TraceMessage( "Inserting a substrate; engine should start monitoring temperature" );
            this.SeebeckEngine.SoakPart();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Soaking, "this is the expected part present state" );
            TestInfo.TraceMessage( "Part at temp, engine should start contacting" );
            this.SeebeckEngine.ConnectPart();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Contacting, "this is the expected at temp state" );
            TestInfo.TraceMessage( "Part contacted, engine should start measuring" );
            this.SeebeckEngine.MeasurePart();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Measuring, "this is the expected state after part is contacted" );
            TestInfo.TraceMessage( "Part measured, engine should removing the part" );
            this.SeebeckEngine.RemovePart();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Removing, "this is the expected state after part is measured" );
            TestInfo.TraceMessage( "Part removed, engine should show party not present" );
            this.SeebeckEngine.Initial();
            ApplianceBase.Delay( 100d );
            _ = this.SeebeckEngine.StateMachine.State.Should().Be( SeebeckState.Initial, "this is the expected state after part is removed" );
        }
    }
}
