using System;

namespace isr.Automata.FiniteTests
{

    /// <summary> Test information for the Soak Engine Tests. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    public class SoakEngineTestInfo : Core.ApplicationSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
        /// class to its default state.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private SoakEngineTestInfo() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(SoakEngineTestInfo)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static SoakEngineTestInfo _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static SoakEngineTestInfo Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (SoakEngineTestInfo)Synchronized(new SoakEngineTestInfo());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " CONFIGURATION INFORMATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Exists
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("False")]
        public bool Verbose
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool Enabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool All
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " SOAK ENGINE CONDITIONS "

        /// <summary> Gets or sets the random number generator seed. </summary>
        /// <value> The random number generator seed. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public int Seed
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the first setpoint. </summary>
        /// <value> The first setpoint. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("60")]
        public double FirstSetpoint
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the second setpoint. </summary>
        /// <value> The second setpoint. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("80")]
        public double SecondSetpoint
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the window. </summary>
        /// <value> The window. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1.0")]
        public double Window
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the resolution. </summary>
        /// <value> The resolution. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.2")]
        public double Resolution
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the hysteresis. </summary>
        /// <value> The hysteresis. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.2")]
        public double Hysteresis
        {
            get
            {
                return (double)AppSettingGetter(0m);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of soak durations. </summary>
        /// <value> The number of soak durations. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("2")]
        public int SoakDurationCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the sample interval. </summary>
        /// <value> The sample interval. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:01")]
        public TimeSpan SampleInterval
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
