using System;
using FluentAssertions;
using isr.Automata.Finite.Engines;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stateless;

namespace isr.Automata.FiniteTests
{
    /// <summary> This is a test class for Soak state machine automata. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-08-28 </para>
    /// </remarks>
    [TestClass()]
    public class SoakEngineTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( Finite.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            if ( TestInfo is object )
            {
                TestInfo.Dispose();
                TestInfo = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( SoakEngineTestInfo.Get().Exists, $"{typeof( SoakEngineTestInfo )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        /// <summary> Gets or sets the soak engine. </summary>
        /// <value> The soak engine. </value>
        private SoakEngine SoakEngine { get; set; }

        /// <summary> Assign soak engine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignSoakEngine(SoakEngine value)
        {
            if ( this.SoakEngine is object)
            {
                this.SoakEngine = null;
            }

            if (value is object)
            {
                this.SoakEngine = value;
                this.SoakEngine.RampTimeout = TimeSpan.Zero;
                this.SoakEngine.SoakDuration = TimeSpan.Zero;
                this.SoakEngine.AllowedOffStateDuration = TimeSpan.Zero;
                this.SoakEngine.AllowedOffStateCount = 0;
                this.SoakEngine.SoakCount = SoakEngineTestInfo.Get().SoakDurationCount;
                this.SoakEngine.Window = SoakEngineTestInfo.Get().Window;
                this.SoakEngine.Hysteresis = SoakEngineTestInfo.Get().Hysteresis;
                this.SoakEngine.SampleInterval = SoakEngineTestInfo.Get().SampleInterval;
                this.SoakEngine.TemperatureResolution = SoakEngineTestInfo.Get().Resolution;
                this.SoakEngine.StateMachine.OnTransitioned(t => this.OnTransitioned(t));
                this.OnEntered( this.SoakEngine.StateMachine.State);
            }
        }

        /// <summary> Handles the state entry actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnTransitioned(StateMachine<SoakState, SoakTrigger>.Transition transition)
        {
            if (transition is null)
            {
                this.OnEntered( this.SoakEngine.CurrentState);
            }
            else
            {
                TestInfo.TraceMessage($"{transition.Trigger} Triggered transition from {transition.Source} to {transition.Destination} state");
                this.OnTransitioned(transition.Source, transition.Destination);
            }
        }

        /// <summary> Handles the state transition actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="source">      Source for the. </param>
        /// <param name="destination"> The state. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private void OnTransitioned(SoakState source, SoakState destination)
        {
            switch (source)
            {
                case SoakState.Engaged:
                    {
                        if (destination == SoakState.HasSetpoint)
                        {
                        }

                        break;
                    }
            }

            this.OnEntered(destination);
        }

        /// <summary> Executes the entered action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="destination"> The state. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private void OnEntered(SoakState destination)
        {
            TestInfo.TraceMessage($"Entered the {destination} state");
            switch (destination)
            {
                case SoakState.AtTemp:
                    {
                        break;
                    }
            }
        }

        /// <summary> (Unit Test Method) tests soak engine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [TestMethod()]
        public void SoakEngineTest()
        {
            TestInfo.TraceMessage("Begin soak engine test");
            using var Automata = new SoakEngine( "Soak Sequencer" );
            this.AssignSoakEngine( Automata );
            _ = this.SoakEngine.Should().NotBeNull( "Engine was assigned" );
            _ = this.SoakEngine.CurrentState.Should().Be( SoakState.Idle, "this is the expected initial state" );
            TestInfo.TraceMessage( "Engaging the soak Engine" );
            this.SoakEngine.Engage();
            _ = this.SoakEngine.CurrentState.Should().Be( SoakState.Engaged, "this is the expected engaged state" );
            TestInfo.TraceMessage( "Setting the first setpoint" );
            this.SoakEngine.ApplySetpoint( SoakEngineTestInfo.Get().FirstSetpoint );
            _ = this.SoakEngine.IsAtSetpoint( SoakEngineTestInfo.Get().FirstSetpoint ).Should().BeTrue( $"{SoakEngineTestInfo.Get().FirstSetpoint} equals {this.SoakEngine.Setpoint}" );
            _ = this.SoakEngine.CurrentState.Should().Be( SoakState.HasSetpoint, "this is the expected setpoint state" );
            var rnd = new Random( SoakEngineTestInfo.Get().Seed );
            int soakCount = 0;
            var expectedState = SoakState.AtTemp;
            TestInfo.TraceMessage( "Going from Ramp to At Temp" );
            do
            {
                soakCount += 1;
                expectedState = soakCount >= this.SoakEngine.SoakCount ? SoakState.AtTemp : SoakState.Soak;
                this.SoakEngine.ApplyTemperature( SoakEngineTestInfo.Get().FirstSetpoint + 0.5d * rnd.NextDouble() * SoakEngineTestInfo.Get().Window );
                _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            }
            while ( soakCount <= this.SoakEngine.SoakCount + 2 && this.SoakEngine.CurrentState == SoakState.Soak );
            expectedState = SoakState.AtTemp;
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            int offStateCount = 0;
            offStateCount += 1;
            TestInfo.TraceMessage( "Going from At Temp to Ramp" );
            expectedState = SoakState.Ramp;
            this.SoakEngine.ApplyTemperature( SoakEngineTestInfo.Get().FirstSetpoint + 0.5d * (1.01d + rnd.NextDouble()) * (SoakEngineTestInfo.Get().Window + SoakEngineTestInfo.Get().Hysteresis) );
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {offStateCount} times" );
            TestInfo.TraceMessage( "Going from Ramp to Off Soak and ramp" );
            soakCount = 1;
            expectedState = SoakState.Soak;
            this.SoakEngine.ApplyTemperature( SoakEngineTestInfo.Get().FirstSetpoint + 0.5d * rnd.NextDouble() * SoakEngineTestInfo.Get().Window );
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            soakCount += 1;
            expectedState = SoakState.Ramp;
            this.SoakEngine.ApplyTemperature( SoakEngineTestInfo.Get().FirstSetpoint + 0.5d * (1.01d + rnd.NextDouble()) * (SoakEngineTestInfo.Get().Window + SoakEngineTestInfo.Get().Hysteresis) );
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            soakCount = 0;
            TestInfo.TraceMessage( "Going from Ramp to at temp" );
            do
            {
                soakCount += 1;
                expectedState = soakCount >= this.SoakEngine.SoakCount ? SoakState.AtTemp : SoakState.Soak;
                this.SoakEngine.ApplyTemperature( SoakEngineTestInfo.Get().FirstSetpoint + 0.5d * rnd.NextDouble() * SoakEngineTestInfo.Get().Window );
                _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            }
            while ( soakCount <= this.SoakEngine.SoakCount + 2 && this.SoakEngine.CurrentState == SoakState.Soak );
            expectedState = SoakState.AtTemp;
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint applied {soakCount} times" );
            TestInfo.TraceMessage( "changing setpoint" );
            expectedState = SoakState.Ramp;
            this.SoakEngine.ChangeSetpoint( SoakEngineTestInfo.Get().SecondSetpoint );
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"temperature {this.SoakEngine.Temperature} {(this.SoakEngine.IsAtTemp() ? "within" : "outside")} setpoint changed" );
            TestInfo.TraceMessage( "Terminating" );
            expectedState = SoakState.Terminal;
            this.SoakEngine.Terminate();
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"soak sequence terminated" );
            TestInfo.TraceMessage( "Engaging" );
            expectedState = SoakState.Engaged;
            this.SoakEngine.Engage();
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"soak sequence engaged" );
            TestInfo.TraceMessage( "Idling" );
            expectedState = SoakState.Idle;
            this.SoakEngine.Disengage();
            _ = this.SoakEngine.CurrentState.Should().Be( expectedState, $"soak sequence idled" );
        }
    }
}
