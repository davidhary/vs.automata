﻿
namespace isr.Automata.FiniteTests.My
{

    /// <summary> Provides assembly information for the class library. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Finite Automata Unit Test Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Finite Automata Unit Test Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Automata.Finite.Unit.Tests";
    }
}