using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using isr.Automata.Finite.Engines;
using isr.Automata.Finite.Forms.ExceptionExtensions;
using Microsoft.VisualBasic;
using Stateless;

namespace isr.Automata.Finite.Forms
{

    /// <summary> A bi-manual toggle switch implementation. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-09-14 </para>
    /// </remarks>
    public partial class BimanualToggle : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructor for this class. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public BimanualToggle() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Create a new state machine
            this.Engine = new BimanualToggleEngine("Bi-Manual Toggle") { ResponseTimeout = TimeSpan.FromSeconds(0.25d) };
            this._ResponseTimeLabel.Text = this.Engine.ResponseTimeout.TotalSeconds.ToString(@"#.00#\s", System.Globalization.CultureInfo.CurrentCulture);
            Application.DoEvents();
            this.__AcknowledgeButton.Name = "_AcknowledgeButton";
            this.__RightPushButton.Name = "_RightPushButton";
            this.__LeftPushButton.Name = "_LeftPushButton";
            this.__EStopButton.Name = "_EStopButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-08-19. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && this.components is object)
                {
                    this.components.Dispose();
                    if ( this._Engine is object)
                    {
                        this._Engine.Dispose();
                        this._Engine = null;
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion

        #region " FORM EVENTS HANDLERS "

        /// <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
        /// event data. </param>
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            int keyAscii = Strings.Asc(e.KeyChar);
            switch (keyAscii)
            {
                case (int)Keys.A:
                case (int)Keys.A + 32:
                    {
                        SendKeys.Send("%A");
                        break;
                    }

                case (int)Keys.E:
                case (int)Keys.E + 32:
                    {
                        SendKeys.Send("%E");
                        break;
                    }

                case (int)Keys.L:
                case (int)Keys.L + 32:
                    {
                        SendKeys.Send("%L");
                        break;
                    }

                case (int)Keys.R:
                case (int)Keys.R + 32:
                    {
                        SendKeys.Send("%R");
                        break;
                    }

                case (int)Keys.S:
                case (int)Keys.S + 32:
                    {
                        SendKeys.Send("%S");
                        break;
                    }

                case var @case when @case == (int)Keys.S:
                case (int)Keys.O + 32:
                    {
                        SendKeys.Send("%O");
                        break;
                    }

                case (int)Keys.Y:
                case (int)Keys.Y + 32:
                    {
                        SendKeys.Send("%Y");
                        break;
                    }
            }

            e.KeyChar = (char)keyAscii;
            if (keyAscii == 0)
            {
                e.Handled = true;
            }

            base.OnKeyPress(e);
        }

        /// <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                this.Engine.StateMachine.Activate();
            }
            catch
            {
            }
            finally
            {
                base.OnLoad(e);
            }
        }

        #endregion

        #region " MACHINE BEHAVIOR "

        /// <summary>   The engine. </summary>
        private BimanualToggleEngine _Engine;

        /// <summary> Gets or sets the engine. </summary>
        /// <value> The engine. </value>
        private BimanualToggleEngine Engine
        {
            get => this._Engine;

            set {
                if ( this._Engine is object )
                {
                    this._Engine.PropertyChanged -= this.BimanualEnabler_PropertyChanged;
                }

                this._Engine = value;
                if ( value is null )
                {
                }
                else
                {
                    this._Engine.PropertyChanged += this.BimanualEnabler_PropertyChanged;
                    StateMachine<BimanualToggleState, BimanualToggleTrigger>.StateConfiguration stateConfiguration;
                    stateConfiguration = this._Engine.StateMachine.Configure( BimanualToggleState.Off );
                    _ = stateConfiguration.OnEntry( t => this.OnEntry( t ) );
                    _ = stateConfiguration.OnActivate( () => this.OnOffActivate() );
                }
            }
        }

        /// <summary> Executes the 'property changed' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged(BimanualToggleEngine sender, string propertyName)
        {
            if (sender is null || string.IsNullOrWhiteSpace(propertyName))
            {
                return;
            }

            if ( propertyName == nameof( isr.Automata.Finite.Engines.BimanualToggleEngine.LastTransition ) )
            {
                this._CurrentStateLabel.Text = sender.LastTransition?.Destination.ToString();
                if ( sender.LastTransition != null )
                {
                    this.SetStateColor( sender.LastTransition.Source, System.Drawing.SystemColors.InactiveBorder );
                    this.SetStateColor( sender.LastTransition.Destination, System.Drawing.SystemColors.ActiveBorder );
                }
            }
        }

        /// <summary> Platform property changed. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void BimanualEnabler_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.BimanualEnabler_PropertyChanged ), new object[] { sender, e } );
            }
            else
            {
                if ( !(sender is BimanualToggleEngine automaton) || e is null )
                {
                    return;
                }

                string activity = $"handling {automaton.Name}.{e.PropertyName} change";
                try
                {
                    this.OnPropertyChanged(automaton, e.PropertyName);
                }
                catch (Exception ex)
                {
                    this._ErrorLable.Text = $"{this.Name} exception {activity} at {automaton.Name}.{automaton.LastTransition?.Destination} state;. {ex.ToFullBlownString()}";
                }
            }
        }

        /// <summary> Executes the 'off activate' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private void OnOffActivate()
        {
            this._LeftPushButton.CheckState = CheckState.Unchecked;
            this._RightPushButton.CheckState = CheckState.Unchecked;
            this.SetStateColor(BimanualToggleState.Off, SystemColors.ActiveBorder);
        }

        /// <summary> Executes the 'entry' action. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        private void OnEntry(StateMachine<BimanualToggleState, BimanualToggleTrigger>.Transition transition)
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<StateMachine<BimanualToggleState, BimanualToggleTrigger>.Transition>( this.OnEntry ), new object[] { transition } );
            }
            else if (transition is object)
            {
                if (transition.Destination == BimanualToggleState.Off)
                {
                    this.OnOffActivate();
                }
                else if (transition.Destination == BimanualToggleState.Set)
                {
                }
            }
        }

        #endregion

        #region " MACHINE APPERARANCE "

        /// <summary> Gets state color. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="stateId"> Identifier for the state. </param>
        /// <returns> The state color. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private Color GetStateColor(BimanualToggleState stateId)
        {
            return this.StateShape(stateId).BackColor;
        }

        /// <summary> Sets state color. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="stateId"> Identifier for the state. </param>
        /// <param name="value">   The value. </param>
        private void SetStateColor(BimanualToggleState stateId, Color value)
        {
            this.StateShape(stateId).BackColor = value;
            this.StateLabel(stateId).BackColor = value;
        }

        /// <summary> Selects the state shape. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="stateId"> Identifier for the state. </param>
        /// <returns> A Windows.Forms.Label. </returns>
        private Label StateLabel(BimanualToggleState stateId)
        {
            switch (stateId)
            {
                case BimanualToggleState.On:
                    {
                        return this._OnStateLabelLabel;
                    }

                case BimanualToggleState.Estop:
                    {
                        return this._EStopStateLabelLabel;
                    }

                case BimanualToggleState.Off:
                    {
                        return this._OffStateLabelLabel;
                    }

                case BimanualToggleState.Set:
                    {
                        return this._SetStateLabelLabel;
                    }

                case BimanualToggleState.Go:
                    {
                        return this._GoStateLabeLabel;
                    }

                default:
                    {
                        Debug.Assert(false);
                        return this._OffStateLabelLabel;
                    }
            }
        }

        /// <summary> Selects the state shape. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="stateId"> Identifier for the state. </param>
        /// <returns> A Windows.Forms.Label. </returns>
        private Label StateShape(BimanualToggleState stateId)
        {
            switch (stateId)
            {
                case BimanualToggleState.On:
                    {
                        return this._OnStateLabel;
                    }

                case BimanualToggleState.Estop:
                    {
                        return this._EStopStateLabel;
                    }

                case BimanualToggleState.Off:
                    {
                        return this._OffStateLabel;
                    }

                case BimanualToggleState.Set:
                    {
                        return this._SetStateLabel;
                    }

                case BimanualToggleState.Go:
                    {
                        return this._GoStateLabel;
                    }

                default:
                    {
                        Debug.Assert(false);
                        return this._OffStateLabel;
                    }
            }
        }

        #endregion

        #region " CONTROL EVENTS HANDLERS "

        /// <summary> Sets the acknowledge input. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="eventSender"> . </param>
        /// <param name="eventArgs">   . </param>
        private void AcknowledgeButton_Click(object eventSender, EventArgs eventArgs)
        {
            if ( this._AcknowledgeButton.Enabled && this.Engine is object)
            {
                // Acknowledge receipt of the go signal.
                this.Engine.Acknowledge();
            }
        }

        /// <summary> Sends an EStop signal. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="eventSender"> . </param>
        /// <param name="eventArgs">   . </param>
        private void EStopButton_Click(object eventSender, EventArgs eventArgs)
        {
            if ( this._EStopButton.Enabled && this.Engine is object)
            {
                // Send the state machine to its idle state
                this.Engine.EstopPressed();
            }
        }

        /// <summary> Inputs the left button state to the state machine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="eventSender"> . </param>
        /// <param name="eventArgs">   . </param>
        private void PushButton_CheckStateChanged(object eventSender, EventArgs eventArgs)
        {
            if ( this.InitializingComponents )
            {
                return;
            }

            CheckBox button = (CheckBox)eventSender;
            if (button is null)
            {
                return;
            }

            if (button.Checked)
            {
                // calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonPressed));
                thread.Start();
                button.BackColor = SystemColors.GradientActiveCaption;
            }
            else
            {
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonReleased));
                thread.Start();
                button.BackColor = SystemColors.GradientInactiveCaption;
            }
        }

        /// <summary> Inputs the left button state to the state machine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="eventSender"> . </param>
        /// <param name="eventArgs">   . </param>
        private void LeftPushButton_CheckStateChanged(object eventSender, EventArgs eventArgs)
        {
            if ( this.InitializingComponents )
            {
                return;
            }

            if ( this._LeftPushButton.Checked)
            {
                // calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonPressed));
                thread.Start();
                this._LeftPushButton.BackColor = SystemColors.GradientActiveCaption;
            }
            else
            {
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonReleased));
                thread.Start();
                this._LeftPushButton.BackColor = SystemColors.GradientInactiveCaption;
            }
        }

        /// <summary> Inputs the right button state to the state machine. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="eventSender"> . </param>
        /// <param name="eventArgs">   . </param>
        private void RightPushButton_CheckStateChanged(object eventSender, EventArgs eventArgs)
        {
            if ( this.InitializingComponents )
            {
                return;
            }

            if ( this._RightPushButton.Checked)
            {
                // calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonPressed));
                thread.Start();
                this._RightPushButton.BackColor = SystemColors.GradientActiveCaption;
            }
            else
            {
                var thread = new System.Threading.Thread(new System.Threading.ThreadStart( this.Engine.ButtonReleased));
                thread.Start();
                this._RightPushButton.BackColor = SystemColors.GradientInactiveCaption;
            }
        }

        #endregion

    }
}
