﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Automata.Finite.Forms
{
    [DesignerGenerated()]
    public partial class BimanualToggle
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ErrorLable = new System.Windows.Forms.Label();
            _InfoLabel = new System.Windows.Forms.Label();
            _ToolTip = new System.Windows.Forms.ToolTip(components);
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            Panel1 = new System.Windows.Forms.Panel();
            _SetStateLabelLabel = new System.Windows.Forms.Label();
            _OffStateLabelLabel = new System.Windows.Forms.Label();
            _OnStateLabel = new System.Windows.Forms.Label();
            _ResponseTimeLabelLabel = new System.Windows.Forms.Label();
            _OnStateLabelLabel = new System.Windows.Forms.Label();
            _GoStateLabeLabel = new System.Windows.Forms.Label();
            _ResponseTimeLabel = new System.Windows.Forms.Label();
            __AcknowledgeButton = new System.Windows.Forms.Button();
            __AcknowledgeButton.Click += new EventHandler(AcknowledgeButton_Click);
            _EStopStateLabel = new System.Windows.Forms.Label();
            __RightPushButton = new System.Windows.Forms.CheckBox();
            __RightPushButton.Click += new EventHandler(PushButton_CheckStateChanged);
            __LeftPushButton = new System.Windows.Forms.CheckBox();
            __LeftPushButton.Click += new EventHandler(PushButton_CheckStateChanged);
            _GoStateLabel = new System.Windows.Forms.Label();
            _SetStateLabel = new System.Windows.Forms.Label();
            _OffStateLabel = new System.Windows.Forms.Label();
            _EStopStateLabelLabel = new System.Windows.Forms.Label();
            _CurrentStateLabel = new System.Windows.Forms.Label();
            __EStopButton = new System.Windows.Forms.Button();
            __EStopButton.Click += new EventHandler(EStopButton_Click);
            _Layout.SuspendLayout();
            Panel1.SuspendLayout();
            SuspendLayout();
            // 
            // _ErrorLable
            // 
            _ErrorLable.BackColor = System.Drawing.SystemColors.Control;
            _ErrorLable.Cursor = System.Windows.Forms.Cursors.Default;
            _ErrorLable.Dock = System.Windows.Forms.DockStyle.Left;
            _ErrorLable.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ErrorLable.ForeColor = System.Drawing.SystemColors.ControlText;
            _ErrorLable.Location = new System.Drawing.Point(19, 286);
            _ErrorLable.Name = "_ErrorLable";
            _ErrorLable.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ErrorLable.Size = new System.Drawing.Size(368, 82);
            _ErrorLable.TabIndex = 35;
            _ErrorLable.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _InfoLabel
            // 
            _InfoLabel.BackColor = System.Drawing.SystemColors.Control;
            _InfoLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _InfoLabel.Dock = System.Windows.Forms.DockStyle.Left;
            _InfoLabel.Font = new System.Drawing.Font("Arial", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _InfoLabel.Location = new System.Drawing.Point(19, 368);
            _InfoLabel.Name = "_InfoLabel";
            _InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _InfoLabel.Size = new System.Drawing.Size(368, 62);
            _InfoLabel.TabIndex = 28;
            _InfoLabel.Text = "An OSHA-compliant Bi-Manual Enabler control with E-Stop.  This control is enabled" + " upon receiving an acknowledge signal after both push buttons are pressed within" + " a preset time interval.";
            _InfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(Panel1, 1, 1);
            _Layout.Controls.Add(_ErrorLable, 1, 2);
            _Layout.Controls.Add(_InfoLabel, 1, 3);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 5;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(406, 447);
            _Layout.TabIndex = 36;
            // 
            // Panel1
            // 
            Panel1.Controls.Add(_SetStateLabelLabel);
            Panel1.Controls.Add(_OffStateLabelLabel);
            Panel1.Controls.Add(_ResponseTimeLabelLabel);
            Panel1.Controls.Add(_OnStateLabelLabel);
            Panel1.Controls.Add(_GoStateLabeLabel);
            Panel1.Controls.Add(_ResponseTimeLabel);
            Panel1.Controls.Add(__AcknowledgeButton);
            Panel1.Controls.Add(__RightPushButton);
            Panel1.Controls.Add(__LeftPushButton);
            Panel1.Controls.Add(_GoStateLabel);
            Panel1.Controls.Add(_SetStateLabel);
            Panel1.Controls.Add(_OffStateLabel);
            Panel1.Controls.Add(_EStopStateLabelLabel);
            Panel1.Controls.Add(_CurrentStateLabel);
            Panel1.Controls.Add(__EStopButton);
            Panel1.Controls.Add(_OnStateLabel);
            Panel1.Controls.Add(_EStopStateLabel);
            Panel1.Location = new System.Drawing.Point(19, 19);
            Panel1.Name = "Panel1";
            Panel1.Size = new System.Drawing.Size(368, 264);
            Panel1.TabIndex = 37;
            // 
            // _SetStateLabelLabel
            // 
            _SetStateLabelLabel.AutoSize = true;
            _SetStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            _SetStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _SetStateLabelLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SetStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _SetStateLabelLabel.Location = new System.Drawing.Point(308, 115);
            _SetStateLabelLabel.Name = "_SetStateLabelLabel";
            _SetStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _SetStateLabelLabel.Size = new System.Drawing.Size(26, 14);
            _SetStateLabelLabel.TabIndex = 42;
            _SetStateLabelLabel.Text = "SET";
            _SetStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _OffStateLabelLabel
            // 
            _OffStateLabelLabel.AutoSize = true;
            _OffStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            _OffStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _OffStateLabelLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OffStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _OffStateLabelLabel.Location = new System.Drawing.Point(227, 68);
            _OffStateLabelLabel.Name = "_OffStateLabelLabel";
            _OffStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _OffStateLabelLabel.Size = new System.Drawing.Size(27, 14);
            _OffStateLabelLabel.TabIndex = 41;
            _OffStateLabelLabel.Text = "OFF";
            _OffStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _OnStateLabel
            // 
            _OnStateLabel.BackColor = System.Drawing.Color.Transparent;
            _OnStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _OnStateLabel.Location = new System.Drawing.Point(119, 101);
            _OnStateLabel.Name = "_OnStateLabel";
            _OnStateLabel.Size = new System.Drawing.Size(75, 42);
            _OnStateLabel.TabIndex = 34;
            // 
            // _ResponseTimeLabelLabel
            // 
            _ResponseTimeLabelLabel.BackColor = System.Drawing.Color.Transparent;
            _ResponseTimeLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ResponseTimeLabelLabel.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ResponseTimeLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ResponseTimeLabelLabel.Location = new System.Drawing.Point(6, 54);
            _ResponseTimeLabelLabel.Name = "_ResponseTimeLabelLabel";
            _ResponseTimeLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ResponseTimeLabelLabel.Size = new System.Drawing.Size(122, 17);
            _ResponseTimeLabelLabel.TabIndex = 48;
            _ResponseTimeLabelLabel.Text = "Bi-Manual Timeout";
            // 
            // _OnStateLabelLabel
            // 
            _OnStateLabelLabel.AutoSize = true;
            _OnStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            _OnStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _OnStateLabelLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OnStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _OnStateLabelLabel.Location = new System.Drawing.Point(148, 115);
            _OnStateLabelLabel.Name = "_OnStateLabelLabel";
            _OnStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _OnStateLabelLabel.Size = new System.Drawing.Size(22, 14);
            _OnStateLabelLabel.TabIndex = 45;
            _OnStateLabelLabel.Text = "ON";
            _OnStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _GoStateLabeLabel
            // 
            _GoStateLabeLabel.AutoSize = true;
            _GoStateLabeLabel.BackColor = System.Drawing.Color.Transparent;
            _GoStateLabeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _GoStateLabeLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _GoStateLabeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _GoStateLabeLabel.Location = new System.Drawing.Point(229, 162);
            _GoStateLabeLabel.Name = "_GoStateLabeLabel";
            _GoStateLabeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _GoStateLabeLabel.Size = new System.Drawing.Size(23, 14);
            _GoStateLabeLabel.TabIndex = 43;
            _GoStateLabeLabel.Text = "GO";
            _GoStateLabeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _ResponseTimeLabel
            // 
            _ResponseTimeLabel.BackColor = System.Drawing.SystemColors.Control;
            _ResponseTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            _ResponseTimeLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ResponseTimeLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ResponseTimeLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ResponseTimeLabel.Location = new System.Drawing.Point(7, 73);
            _ResponseTimeLabel.Name = "_ResponseTimeLabel";
            _ResponseTimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ResponseTimeLabel.Size = new System.Drawing.Size(51, 18);
            _ResponseTimeLabel.TabIndex = 49;
            _ResponseTimeLabel.Text = "0.2500";
            // 
            // _AcknowledgeButton
            // 
            __AcknowledgeButton.BackColor = System.Drawing.SystemColors.Control;
            __AcknowledgeButton.Cursor = System.Windows.Forms.Cursors.Default;
            __AcknowledgeButton.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __AcknowledgeButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __AcknowledgeButton.Location = new System.Drawing.Point(8, 159);
            __AcknowledgeButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            __AcknowledgeButton.Name = "__AcknowledgeButton";
            __AcknowledgeButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __AcknowledgeButton.Size = new System.Drawing.Size(102, 39);
            __AcknowledgeButton.TabIndex = 50;
            __AcknowledgeButton.Text = "&ACKNOWLEDGE";
            _ToolTip.SetToolTip(__AcknowledgeButton, "Must acknowledge (handshake) to move on.");
            __AcknowledgeButton.UseVisualStyleBackColor = false;
            // 
            // _EStopStateLabel
            // 
            _EStopStateLabel.BackColor = System.Drawing.Color.Transparent;
            _EStopStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _EStopStateLabel.Location = new System.Drawing.Point(287, 7);
            _EStopStateLabel.Name = "_EStopStateLabel";
            _EStopStateLabel.Size = new System.Drawing.Size(75, 42);
            _EStopStateLabel.TabIndex = 38;
            // 
            // _RightPushButton
            // 
            __RightPushButton.Appearance = System.Windows.Forms.Appearance.Button;
            __RightPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            __RightPushButton.Cursor = System.Windows.Forms.Cursors.Default;
            __RightPushButton.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __RightPushButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __RightPushButton.Location = new System.Drawing.Point(247, 206);
            __RightPushButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            __RightPushButton.Name = "__RightPushButton";
            __RightPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __RightPushButton.Size = new System.Drawing.Size(108, 52);
            __RightPushButton.TabIndex = 47;
            __RightPushButton.Text = "&RIGHT";
            __RightPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _ToolTip.SetToolTip(__RightPushButton, "Check left and right buttons quickly to enable.");
            __RightPushButton.UseVisualStyleBackColor = false;
            // 
            // _LeftPushButton
            // 
            __LeftPushButton.Appearance = System.Windows.Forms.Appearance.Button;
            __LeftPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            __LeftPushButton.Cursor = System.Windows.Forms.Cursors.Default;
            __LeftPushButton.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __LeftPushButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __LeftPushButton.Location = new System.Drawing.Point(125, 206);
            __LeftPushButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            __LeftPushButton.Name = "__LeftPushButton";
            __LeftPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __LeftPushButton.Size = new System.Drawing.Size(108, 52);
            __LeftPushButton.TabIndex = 46;
            __LeftPushButton.Text = "&LEFT";
            __LeftPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            _ToolTip.SetToolTip(__LeftPushButton, "Check left and right buttons quickly to enable.");
            __LeftPushButton.UseVisualStyleBackColor = false;
            // 
            // _GoStateLabel
            // 
            _GoStateLabel.BackColor = System.Drawing.Color.Transparent;
            _GoStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _GoStateLabel.Location = new System.Drawing.Point(201, 147);
            _GoStateLabel.Name = "_GoStateLabel";
            _GoStateLabel.Size = new System.Drawing.Size(75, 42);
            _GoStateLabel.TabIndex = 35;
            // 
            // _SetStateLabel
            // 
            _SetStateLabel.BackColor = System.Drawing.Color.Transparent;
            _SetStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _SetStateLabel.Location = new System.Drawing.Point(281, 102);
            _SetStateLabel.Name = "_SetStateLabel";
            _SetStateLabel.Size = new System.Drawing.Size(80, 40);
            _SetStateLabel.TabIndex = 36;
            // 
            // _OffStateLabel
            // 
            _OffStateLabel.BackColor = System.Drawing.Color.Transparent;
            _OffStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _OffStateLabel.Location = new System.Drawing.Point(201, 54);
            _OffStateLabel.Name = "_OffStateLabel";
            _OffStateLabel.Size = new System.Drawing.Size(75, 42);
            _OffStateLabel.TabIndex = 37;
            // 
            // _EStopStateLabelLabel
            // 
            _EStopStateLabelLabel.AutoSize = true;
            _EStopStateLabelLabel.BackColor = System.Drawing.Color.Transparent;
            _EStopStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _EStopStateLabelLabel.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EStopStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _EStopStateLabelLabel.Location = new System.Drawing.Point(305, 21);
            _EStopStateLabelLabel.Name = "_EStopStateLabelLabel";
            _EStopStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _EStopStateLabelLabel.Size = new System.Drawing.Size(40, 14);
            _EStopStateLabelLabel.TabIndex = 40;
            _EStopStateLabelLabel.Text = "ESTOP";
            _EStopStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // _CurrentStateLabel
            // 
            _CurrentStateLabel.BackColor = System.Drawing.Color.Transparent;
            _CurrentStateLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _CurrentStateLabel.Font = new System.Drawing.Font("Arial", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _CurrentStateLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _CurrentStateLabel.Location = new System.Drawing.Point(5, 6);
            _CurrentStateLabel.Name = "_CurrentStateLabel";
            _CurrentStateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CurrentStateLabel.Size = new System.Drawing.Size(129, 27);
            _CurrentStateLabel.TabIndex = 39;
            _CurrentStateLabel.Text = "<state>";
            _ToolTip.SetToolTip(_CurrentStateLabel, "Current State");
            // 
            // _EStopButton
            // 
            __EStopButton.BackColor = System.Drawing.Color.Red;
            __EStopButton.Cursor = System.Windows.Forms.Cursors.Default;
            __EStopButton.Font = new System.Drawing.Font("Arial", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __EStopButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __EStopButton.Location = new System.Drawing.Point(8, 204);
            __EStopButton.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            __EStopButton.Name = "__EStopButton";
            __EStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __EStopButton.Size = new System.Drawing.Size(100, 59);
            __EStopButton.TabIndex = 44;
            __EStopButton.Text = "&E-STOP";
            _ToolTip.SetToolTip(__EStopButton, "Click to go to the EStop state");
            __EStopButton.UseVisualStyleBackColor = false;
            // 
            // BimanualToggle
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_Layout);
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "BimanualToggle";
            Size = new System.Drawing.Size(406, 447);
            _Layout.ResumeLayout(false);
            Panel1.ResumeLayout(false);
            Panel1.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Label _ErrorLable;
        private System.Windows.Forms.Label _InfoLabel;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel Panel1;
        private System.Windows.Forms.Label _SetStateLabelLabel;
        private System.Windows.Forms.Label _OffStateLabelLabel;
        private System.Windows.Forms.Label _ResponseTimeLabelLabel;
        private System.Windows.Forms.Label _OnStateLabelLabel;
        private System.Windows.Forms.Label _GoStateLabeLabel;
        private System.Windows.Forms.Label _ResponseTimeLabel;
        private System.Windows.Forms.Button __AcknowledgeButton;

        private System.Windows.Forms.Button _AcknowledgeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcknowledgeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcknowledgeButton != null)
                {
                    __AcknowledgeButton.Click -= AcknowledgeButton_Click;
                }

                __AcknowledgeButton = value;
                if (__AcknowledgeButton != null)
                {
                    __AcknowledgeButton.Click += AcknowledgeButton_Click;
                }
            }
        }

        private System.Windows.Forms.CheckBox __RightPushButton;

        private System.Windows.Forms.CheckBox _RightPushButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RightPushButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RightPushButton != null)
                {
                    __RightPushButton.Click -= PushButton_CheckStateChanged;
                }

                __RightPushButton = value;
                if (__RightPushButton != null)
                {
                    __RightPushButton.Click += PushButton_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox __LeftPushButton;

        private System.Windows.Forms.CheckBox _LeftPushButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LeftPushButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LeftPushButton != null)
                {
                    __LeftPushButton.Click -= PushButton_CheckStateChanged;
                }

                __LeftPushButton = value;
                if (__LeftPushButton != null)
                {
                    __LeftPushButton.Click += PushButton_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.Label _GoStateLabel;
        private System.Windows.Forms.Label _SetStateLabel;
        private System.Windows.Forms.Label _OffStateLabel;
        private System.Windows.Forms.Label _EStopStateLabelLabel;
        private System.Windows.Forms.Label _CurrentStateLabel;
        private System.Windows.Forms.Button __EStopButton;

        private System.Windows.Forms.Button _EStopButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EStopButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EStopButton != null)
                {
                    __EStopButton.Click -= EStopButton_Click;
                }

                __EStopButton = value;
                if (__EStopButton != null)
                {
                    __EStopButton.Click += EStopButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _OnStateLabel;
        private System.Windows.Forms.Label _EStopStateLabel;
    }
}