using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using isr.Automata.Finite.Forms.ExceptionExtensions;

namespace isr.Automata.Finite.Forms
{

    /// <summary> A Engine monitor. </summary>
    /// <remarks> David, 2020-10-01. </remarks>
    public partial class EngineMonitor<TState, TTrigger> : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTORES "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        public EngineMonitor()
        {

            // This call is required by the designer.
            this.InitializeComponent();
            this.DefaultInfoProviderButton = this._EngineInfoButton;
            this.Enabled = false;
            // https://stackoverflow.com/questions/2646606/how-do-i-reclaim-the-space-from-the-grip
            // The padding is broken. MS won't fix this.
            this._TriggerStatusStrip.SizingGrip = false;
            this._TriggerStatusStrip.Padding = new Padding( this._TriggerStatusStrip.Padding.Left, this._TriggerStatusStrip.Padding.Top, this._TriggerStatusStrip.Padding.Left, this._TriggerStatusStrip.Padding.Bottom);
            this._NameStatusStrip.SizingGrip = false;
            this._NameStatusStrip.Padding = new Padding( this._NameStatusStrip.Padding.Left, this._NameStatusStrip.Padding.Top, this._NameStatusStrip.Padding.Left, this._NameStatusStrip.Padding.Bottom);
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    this.Engine = null;
                    if ( this.components is object)
                    {
                        this.components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion

        #region " ENGINE LISTENER "

        /// <summary> Gets or sets the Engine. </summary>
        /// <value> The Engine. </value>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public EngineBase<TState, TTrigger> Engine { get; private set; }

        /// <summary> Assigns an engine. </summary>
        /// <remarks> David, 2020-08-26. </remarks>
        /// <param name="engine"> The Engine. </param>
        public void AssignEngine(EngineBase<TState, TTrigger> engine)
        {
            if ( this.Engine is object)
            {
                this.Engine.UnregisterEngineFailureAction(e => this.Annunciate(e));
                this.Engine.Dispose();
                this.Engine = null;
            }

            this.Engine = engine;
            this.Enabled = engine is object;
            if (engine is object)
            {
                this.Engine.StateMachine.OnTransitionedAsync((t) => Task.Run(() => this.OnStateTransitioned(t)));
                engine.RegisterEngineFailureAction(e => this.Annunciate(e));
                if ( this.Engine.LastTransition is null)
                {
                    this._EngineInfoButton.Text = this.Engine.CurrentState.ToString();
                }
                else
                {
                    this.OnStateTransitioned( this.Engine.LastTransition);
                }

                this._EngineNameLabel.Text = this.Engine.Name;
            }
        }

        /// <summary> Handles the Stream Reading Engine state transition actions. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="transition"> The transition. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void OnStateTransitioned(Stateless.StateMachine<TState, TTrigger>.Transition transition)
        {
            string activity = string.Empty;
            try
            {
                if (transition is null)
                {
                    this._LastTriggerLabel.Text = "<trigger>";
                    this._PreviousStateTextBox.Text = "<previous state>";
                    this._EngineInfoButton.Text = "<state>";
                }
                else
                {
                    this._LastTriggerLabel.Text = transition.Trigger.ToString();
                    this._PreviousStateTextBox.Text = transition.Source.ToString();
                    this._EngineInfoButton.Text = transition.Destination.ToString();
                }
            }
            catch (Exception ex)
            {
                this.Annunciate(Core.Forma.InfoProviderLevel.Error, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
        }

        /// <summary> Gets or sets the default information provider button. </summary>
        /// <value> The default information provider button. </value>
        private ToolStripItem DefaultInfoProviderButton { get; set; }


        /// <summary> Refresh display. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void RefreshDisplay()
        {
            this.DisplayTransitionInfo();
        }

        /// <summary> Displays a transition information. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private void DisplayTransitionInfo()
        {
            if ( this.Engine?.LastTransition is null)
            {
                this._LastTriggerLabel.Text = "<trigger>";
                this._PreviousStateTextBox.Text = "<previous state>";
                this._EngineInfoButton.Text = "<state>";
            }
            else
            {
                this._LastTriggerLabel.Text = this.Engine.LastTransition.Trigger.ToString();
                this._PreviousStateTextBox.Text = this.Engine.LastTransition.Source.ToString();
                this._EngineInfoButton.Text = this.Engine.LastTransition.Destination.ToString();
            }
        }

        /// <summary> The nothing to report. </summary>
        private const string _NothingToReport = "Nothing to report";

        /// <summary> Message describing the annunciated. </summary>
        private string _AnnunciatedMessage;

        /// <summary> Annunciates. </summary>
        /// <remarks> David, 2020-08-26. </remarks>
        /// <param name="infoLevel"> The information level. </param>
        /// <param name="message">   The message. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        private void Annunciate(Core.Forma.InfoProviderLevel infoLevel, string message)
        {
            try
            {
                if (!string.Equals(message, this._AnnunciatedMessage ))
                {
                    this._AnnunciatedMessage = message;
                    _ = this.InfoProvider.Annunciate( this.DefaultInfoProviderButton, infoLevel, message );
                    this.DefaultInfoProviderButton.ToolTipText = "Observed";
                    if (!string.Equals(message, _NothingToReport))
                    {
                        Clipboard.SetText(message);
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary> Annunciates. </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        /// <param name="e"> Error event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        private void Annunciate(System.IO.ErrorEventArgs e)
        {
            if (e is null || e.GetException() is null)
            {
                return;
            }

            try
            {
                if (!string.Equals(e.GetException().Message, this._AnnunciatedMessage ))
                {
                    this._AnnunciatedMessage = e.GetException().ToFullBlownString();
                    _ = this.InfoProvider.Annunciate( this.DefaultInfoProviderButton, Core.Forma.InfoProviderLevel.Error, this._AnnunciatedMessage );
                    this.DefaultInfoProviderButton.ToolTipText = "Observed";
                    if (!string.Equals( this._AnnunciatedMessage, _NothingToReport))
                    {
                        Clipboard.SetText( this._AnnunciatedMessage );
                    }
                }
            }
            catch
            {
            }
        }

        /// <summary> Annunciates. </summary>
        /// <remarks> David, 2020-08-26. </remarks>
        /// <param name="infoLevel"> The information level. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void Annunciate(Core.Forma.InfoProviderLevel infoLevel, string format, params object[] args)
        {
            try
            {
                this.Annunciate(infoLevel, string.Format(format, args));
            }
            catch
            {
            }
        }

        #endregion

    }
}
