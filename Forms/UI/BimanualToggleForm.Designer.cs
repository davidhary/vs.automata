using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Automata.Finite.Forms
{
    [DesignerGenerated()]
    public partial class BimanualToggleForm
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _BimanualToggle = new BimanualToggle();
            SuspendLayout();
            // 
            // BimanualToggle1
            // 
            _BimanualToggle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _BimanualToggle.Dock = System.Windows.Forms.DockStyle.Fill;
            _BimanualToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BimanualToggle.Location = new System.Drawing.Point(0, 0);
            _BimanualToggle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _BimanualToggle.Name = "BimanualToggle1";
            _BimanualToggle.Size = new System.Drawing.Size(406, 411);
            _BimanualToggle.TabIndex = 0;
            // 
            // BimanualToggleForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(406, 411);
            Controls.Add(_BimanualToggle);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "BimanualToggleForm";
            Text = "BimanualToggleForm";
            ResumeLayout(false);
        }

        private BimanualToggle _BimanualToggle;
    }
}
