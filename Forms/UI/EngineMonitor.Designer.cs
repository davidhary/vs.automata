using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Automata.Finite.Forms
{
    [DesignerGenerated()]
    public partial class EngineMonitor<TState, TTrigger>
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _TriggerStatusStrip = new System.Windows.Forms.StatusStrip();
            _LastTriggerLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _EngineToolStrip = new System.Windows.Forms.ToolStrip();
            _EngineInfoButton = new System.Windows.Forms.ToolStripSplitButton();
            _PreviousStateTextBox = new System.Windows.Forms.ToolStripTextBox();
            _NameStatusStrip = new System.Windows.Forms.StatusStrip();
            _EngineNameLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _TriggerStatusStrip.SuspendLayout();
            _EngineToolStrip.SuspendLayout();
            _NameStatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _TriggerStatusStrip
            // 
            _TriggerStatusStrip.AutoSize = false;
            _TriggerStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            _TriggerStatusStrip.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerStatusStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _TriggerStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _LastTriggerLabel });
            _TriggerStatusStrip.Location = new System.Drawing.Point(0, 16);
            _TriggerStatusStrip.Name = "_TriggerStatusStrip";
            _TriggerStatusStrip.ShowItemToolTips = true;
            _TriggerStatusStrip.Size = new System.Drawing.Size(163, 16);
            _TriggerStatusStrip.SizingGrip = false;
            _TriggerStatusStrip.TabIndex = 1;
            _TriggerStatusStrip.Text = "Trigger Status Strip";
            // 
            // _LastTriggerLabel
            // 
            _LastTriggerLabel.Margin = new System.Windows.Forms.Padding(0);
            _LastTriggerLabel.Name = "_LastTriggerLabel";
            _LastTriggerLabel.Size = new System.Drawing.Size(148, 16);
            _LastTriggerLabel.Spring = true;
            _LastTriggerLabel.Text = "<trigger>";
            _LastTriggerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            _LastTriggerLabel.ToolTipText = "Last trigger";
            // 
            // _EngineToolStrip
            // 
            _EngineToolStrip.AutoSize = false;
            _EngineToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            _EngineToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _EngineToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _EngineInfoButton });
            _EngineToolStrip.Location = new System.Drawing.Point(0, 33);
            _EngineToolStrip.Name = "_EngineToolStrip";
            _EngineToolStrip.Padding = new System.Windows.Forms.Padding(0);
            _EngineToolStrip.Size = new System.Drawing.Size(163, 20);
            _EngineToolStrip.TabIndex = 2;
            _EngineToolStrip.Text = "Engine Tool Strip";
            // 
            // _EngineInfoButton
            // 
            _EngineInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _EngineInfoButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _PreviousStateTextBox });
            _EngineInfoButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EngineInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _EngineInfoButton.Margin = new System.Windows.Forms.Padding(1);
            _EngineInfoButton.Name = "_EngineInfoButton";
            _EngineInfoButton.Size = new System.Drawing.Size(26, 18);
            _EngineInfoButton.Text = "i";
            _EngineInfoButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            _EngineInfoButton.ToolTipText = "Current state + Additional info";
            // 
            // _PreviousStateTextBox
            // 
            _PreviousStateTextBox.Name = "_PreviousStateTextBox";
            _PreviousStateTextBox.Size = new System.Drawing.Size(100, 23);
            // 
            // _NameStatusStrip
            // 
            _NameStatusStrip.AutoSize = false;
            _NameStatusStrip.Dock = System.Windows.Forms.DockStyle.Top;
            _NameStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _EngineNameLabel });
            _NameStatusStrip.Location = new System.Drawing.Point(0, 0);
            _NameStatusStrip.Name = "_NameStatusStrip";
            _NameStatusStrip.ShowItemToolTips = true;
            _NameStatusStrip.Size = new System.Drawing.Size(163, 16);
            _NameStatusStrip.SizingGrip = false;
            _NameStatusStrip.TabIndex = 3;
            _NameStatusStrip.Text = "Name Status Strip";
            // 
            // _EngineNameLabel
            // 
            _EngineNameLabel.Margin = new System.Windows.Forms.Padding(0);
            _EngineNameLabel.Name = "_EngineNameLabel";
            _EngineNameLabel.Size = new System.Drawing.Size(148, 16);
            _EngineNameLabel.Spring = true;
            _EngineNameLabel.Text = "<name>";
            _EngineNameLabel.ToolTipText = "Automaton name";
            // 
            // EngineMonitor
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 15.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_EngineToolStrip);
            Controls.Add(_TriggerStatusStrip);
            Controls.Add(_NameStatusStrip);
            Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            Name = "EngineMonitor";
            Size = new System.Drawing.Size(163, 53);
            _TriggerStatusStrip.ResumeLayout(false);
            _TriggerStatusStrip.PerformLayout();
            _EngineToolStrip.ResumeLayout(false);
            _EngineToolStrip.PerformLayout();
            _NameStatusStrip.ResumeLayout(false);
            _NameStatusStrip.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.StatusStrip _TriggerStatusStrip;
        private System.Windows.Forms.ToolStrip _EngineToolStrip;
        private System.Windows.Forms.ToolStripSplitButton _EngineInfoButton;
        private System.Windows.Forms.ToolStripStatusLabel _LastTriggerLabel;
        private System.Windows.Forms.ToolStripTextBox _PreviousStateTextBox;
        private System.Windows.Forms.StatusStrip _NameStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _EngineNameLabel;
    }
}
