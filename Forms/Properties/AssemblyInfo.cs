﻿using System;
using System.Reflection;

[assembly: AssemblyTitle(isr.Automata.Finite.Forms.My.MyLibrary.AssemblyTitle)]
[assembly: AssemblyDescription(isr.Automata.Finite.Forms.My.MyLibrary.AssemblyDescription)]
[assembly: AssemblyProduct(isr.Automata.Finite.Forms.My.MyLibrary.AssemblyProduct)]
[assembly: CLSCompliant(true)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
