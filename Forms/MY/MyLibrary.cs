
namespace isr.Automata.Finite.Forms.My
{
    /// <summary>   my library. This class cannot be inherited. </summary>
    /// <remarks>   David, 2020-10-01. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-01. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 514;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Finite Automata Dashboards Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Finite Automata Dashboards Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Automata.Finite.Dashboards";

        /// <summary> Name of the test assembly strong. </summary>
        public const string TestAssemblyStrongName = "isr.Automata.FiniteTests,PublicKey=" + Finite.My.SolutionInfo.PublicKey;
    }
}
