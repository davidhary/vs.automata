Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a On/Off process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class OnOffEngine
    Inherits EngineBase(Of OnOffState, OnOffTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="initialState"> State of the initial. </param>
    Public Sub New(ByVal name As String, ByVal initialState As OnOffState)
        MyBase.New(name, New StateMachine(Of OnOffState, OnOffTrigger)(initialState))

        Dim stateConfiguration As StateMachine(Of OnOffState, OnOffTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(OnOffState.Initial)
        stateConfiguration.Ignore(OnOffTrigger.Initial)
        stateConfiguration.Permit(OnOffTrigger.Off, OnOffState.Off)
        stateConfiguration.Permit(OnOffTrigger.On, OnOffState.On)

        Me.EngagedStates.Add(OnOffState.On)
        stateConfiguration = Me.StateMachine.Configure(OnOffState.On)
        stateConfiguration.Permit(OnOffTrigger.Initial, OnOffState.Initial)
        stateConfiguration.Permit(OnOffTrigger.Off, OnOffState.Off)
        stateConfiguration.Ignore(OnOffTrigger.On)

        Me.EngagedStates.Add(OnOffState.Off)
        stateConfiguration = Me.StateMachine.Configure(OnOffState.Off)
        stateConfiguration.Permit(OnOffTrigger.Initial, OnOffState.Initial)
        stateConfiguration.Ignore(OnOffTrigger.Off)
        stateConfiguration.Permit(OnOffTrigger.On, OnOffState.On)

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        Me.New(name, OnOffState.Initial)
    End Sub

#End Region

#Region " COMMANDs "

    Public Sub [On]()
        Me.Fire(OnOffTrigger.On)
    End Sub

    ''' <summary> Offs this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Off()
        Me.Fire(OnOffTrigger.Off)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(OnOffTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent on off states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum OnOffState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the on] option. </summary>
    <Description("On")> [On]

    ''' <summary> An enum constant representing the off option. </summary>
    <Description("Off")> Off
End Enum

''' <summary> Values that represent on off triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum OnOffTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the on] option. </summary>
    <Description("On")> [On]

    ''' <summary> An enum constant representing the off option. </summary>
    <Description("Off")> Off
End Enum
