Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a bi-manual toggle switch. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class BimanualToggleEngine
    Inherits EngineBase(Of BimanualToggleState, BimanualToggleTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of BimanualToggleState, BimanualToggleTrigger)(BimanualToggleState.Off))

        Me._ResponseStopwatch = New Stopwatch

        Dim stateConfiguration As StateMachine(Of BimanualToggleState, BimanualToggleTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(BimanualToggleState.Off)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntry(t))
        stateConfiguration.Ignore(BimanualToggleTrigger.Acknowledge)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Set)
        stateConfiguration.Ignore(BimanualToggleTrigger.ButtonReleased)
        stateConfiguration.Permit(BimanualToggleTrigger.Estop, BimanualToggleState.Estop)
        stateConfiguration.Ignore(BimanualToggleTrigger.Timeout)

        stateConfiguration = Me.StateMachine.Configure(BimanualToggleState.Set)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntry(t))
        stateConfiguration.Ignore(BimanualToggleTrigger.Acknowledge)
        stateConfiguration.Permit(BimanualToggleTrigger.Estop, BimanualToggleState.Estop)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Go)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off)
        stateConfiguration.Permit(BimanualToggleTrigger.Timeout, BimanualToggleState.Off)

        stateConfiguration = Me.StateMachine.Configure(BimanualToggleState.Go)
        stateConfiguration.Permit(BimanualToggleTrigger.Acknowledge, BimanualToggleState.On)
        stateConfiguration.Ignore(BimanualToggleTrigger.ButtonPressed)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off)
        stateConfiguration.Permit(BimanualToggleTrigger.Estop, BimanualToggleState.Estop)
        stateConfiguration.Ignore(BimanualToggleTrigger.Timeout)

        stateConfiguration = Me.StateMachine.Configure(BimanualToggleState.On)
        stateConfiguration.Ignore(BimanualToggleTrigger.Acknowledge)
        stateConfiguration.Ignore(BimanualToggleTrigger.ButtonPressed)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off)
        stateConfiguration.Permit(BimanualToggleTrigger.Estop, BimanualToggleState.Estop)
        stateConfiguration.Ignore(BimanualToggleTrigger.Timeout)

        stateConfiguration = Me.StateMachine.Configure(BimanualToggleState.Estop)
        stateConfiguration.Ignore(BimanualToggleTrigger.Acknowledge)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonPressed, BimanualToggleState.Off)
        stateConfiguration.Permit(BimanualToggleTrigger.ButtonReleased, BimanualToggleState.Off)
        stateConfiguration.Ignore(BimanualToggleTrigger.Estop)
        stateConfiguration.Ignore(BimanualToggleTrigger.Timeout)

    End Sub

#End Region

#Region " MACHINE "

    ''' <summary> Executes the entry action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    Private Sub OnEntry(transition As StateMachine(Of BimanualToggleState, BimanualToggleTrigger).Transition)
        If transition.Destination = BimanualToggleState.Off Then
            Me.ResponseStopwatch.Reset()
        ElseIf transition.Destination = BimanualToggleState.Set Then
            Me.ResponseStopwatch.Restart()
        End If
    End Sub

#End Region

#Region " TIMEOUT MONITOR "

    ''' <summary> Gets or sets the response stopwatch. </summary>
    ''' <value> The response stopwatch. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Property ResponseStopwatch As Stopwatch

    ''' <summary> The response timeout. </summary>
    Private _ResponseTimeout As TimeSpan

    ''' <summary>
    ''' The required response time for closing the button. For the machine to advance to the Go state,
    ''' the operator must close both buttons within the minimum Response Time seconds.
    ''' </summary>
    ''' <value> The response timeout. </value>
    Public Property ResponseTimeout() As TimeSpan
        Get
            Return Me._ResponseTimeout
        End Get
        Set(value As TimeSpan)
            If value <> Me.ResponseTimeout Then
                Me._ResponseTimeout = value
                Me.AsyncNotifyPropertyChanged()
            End If

        End Set
    End Property

    ''' <summary> Gets the is response timeout. </summary>
    ''' <value> The is response timeout. </value>
    Public ReadOnly Property IsResponseTimeout As Boolean
        Get
            Return Me.ResponseStopwatch.IsRunning AndAlso Me.ResponseStopwatch.Elapsed > Me.ResponseTimeout
        End Get
    End Property

#End Region

#Region " COMMANDs "

    ''' <summary> Button pressed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub ButtonPressed()
        If Me.IsResponseTimeout Then
            Me.Fire(BimanualToggleTrigger.Timeout)
        Else
            Me.Fire(BimanualToggleTrigger.ButtonPressed)
        End If
    End Sub

    ''' <summary> Button released. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub ButtonReleased()
        Me.Fire(BimanualToggleTrigger.ButtonReleased)
    End Sub

    ''' <summary> Estop pressed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub EstopPressed()
        Me.Fire(BimanualToggleTrigger.Estop)
    End Sub

    ''' <summary> Acknowledges this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Acknowledge()
        Me.Fire(BimanualToggleTrigger.Acknowledge)
    End Sub

#End Region

End Class

''' <summary> Values that represent bimanual toggle states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum BimanualToggleState

    ''' <summary> An enum constant representing the off] option. </summary>
    <Description("Off State")> [Off]

    ''' <summary> An enum constant representing the on] option. </summary>
    <Description("On State")> [On]

    ''' <summary> An enum constant representing the set] option. </summary>
    <Description("One button pressed")> [Set]

    ''' <summary> An enum constant representing the go] option. </summary>
    <Description("Two buttons pressed")> [Go]

    ''' <summary> An enum constant representing the estop] option. </summary>
    <Description("Emergency Stop")> [Estop]
End Enum

''' <summary> Values that represent bimanual toggle triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum BimanualToggleTrigger

    ''' <summary> An enum constant representing the button pressed option. </summary>
    <Description("Button Pressed")> ButtonPressed

    ''' <summary> An enum constant representing the button released option. </summary>
    <Description("Button Released")> ButtonReleased

    ''' <summary> An enum constant representing the acknowledge option. </summary>
    <Description("Acknowledge")> Acknowledge

    ''' <summary> An enum constant representing the timeout option. </summary>
    <Description("Timeout")> Timeout

    ''' <summary> An enum constant representing the estop option. </summary>
    <Description("Emergency Stop")> Estop
End Enum
