Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a Removal process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class RemoveEngine
    Inherits EngineBase(Of RemoveState, RemoveTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of RemoveState, RemoveTrigger)(RemoveState.Initial))

        Dim stateConfiguration As StateMachine(Of RemoveState, RemoveTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(RemoveState.Initial)
        stateConfiguration.Permit(RemoveTrigger.Absent, RemoveState.Absent)
        stateConfiguration.Ignore(RemoveTrigger.Initial)
        stateConfiguration.Ignore(RemoveTrigger.Insert)
        stateConfiguration.Ignore(RemoveTrigger.Release)
        stateConfiguration.Ignore(RemoveTrigger.Remove)
        stateConfiguration.Ignore(RemoveTrigger.RemoveFailed)

        Me.EngagedStates.Add(RemoveState.Absent)
        stateConfiguration = Me.StateMachine.Configure(RemoveState.Absent)
        stateConfiguration.Ignore(RemoveTrigger.Absent)
        stateConfiguration.Permit(RemoveTrigger.Initial, RemoveState.Initial)
        stateConfiguration.Permit(RemoveTrigger.Insert, RemoveState.Present)
        stateConfiguration.Ignore(RemoveTrigger.Release)
        stateConfiguration.Ignore(RemoveTrigger.Remove)
        stateConfiguration.Ignore(RemoveTrigger.RemoveFailed)

        Me.EngagedStates.Add(RemoveState.Present)
        stateConfiguration = Me.StateMachine.Configure(RemoveState.Present)
        stateConfiguration.Ignore(RemoveTrigger.Absent)
        stateConfiguration.Permit(RemoveTrigger.Initial, RemoveState.Initial)
        stateConfiguration.Ignore(RemoveTrigger.Insert)
        stateConfiguration.Permit(RemoveTrigger.Release, RemoveState.Released)
        stateConfiguration.Permit(RemoveTrigger.Remove, RemoveState.Absent)
        stateConfiguration.Ignore(RemoveTrigger.RemoveFailed)

        Me.EngagedStates.Add(RemoveState.Released)
        stateConfiguration = Me.StateMachine.Configure(RemoveState.Released)
        stateConfiguration.Ignore(RemoveTrigger.Absent)
        stateConfiguration.Permit(RemoveTrigger.Initial, RemoveState.Initial)
        stateConfiguration.Ignore(RemoveTrigger.Insert)
        stateConfiguration.Ignore(RemoveTrigger.Release)
        stateConfiguration.Permit(RemoveTrigger.Remove, RemoveState.Removed)
        stateConfiguration.Permit(RemoveTrigger.RemoveFailed, RemoveState.NotRemoved)

        stateConfiguration = Me.StateMachine.Configure(RemoveState.Removed)
        stateConfiguration.Permit(RemoveTrigger.Absent, RemoveState.Absent)
        stateConfiguration.Permit(RemoveTrigger.Initial, RemoveState.Initial)
        stateConfiguration.Permit(RemoveTrigger.Insert, RemoveState.Present)
        stateConfiguration.Ignore(RemoveTrigger.Release)
        stateConfiguration.Ignore(RemoveTrigger.Remove)
        stateConfiguration.Ignore(RemoveTrigger.RemoveFailed)

        stateConfiguration = Me.StateMachine.Configure(RemoveState.NotRemoved)
        stateConfiguration.Permit(RemoveTrigger.Absent, RemoveState.Absent)
        stateConfiguration.Permit(RemoveTrigger.Initial, RemoveState.Initial)
        stateConfiguration.Permit(RemoveTrigger.Insert, RemoveState.Present)
        stateConfiguration.Ignore(RemoveTrigger.Release)
        stateConfiguration.Ignore(RemoveTrigger.Remove)
        stateConfiguration.Ignore(RemoveTrigger.RemoveFailed)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(RemoveTrigger.Initial)
    End Sub

    ''' <summary> Absents this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Absent()
        Me.Fire(RemoveTrigger.Absent)
    End Sub

    ''' <summary> Inserts this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Insert()
        Me.Fire(RemoveTrigger.Insert)
    End Sub

    ''' <summary> Releases this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Release()
        Me.Fire(RemoveTrigger.Release)
    End Sub

    ''' <summary> Removes the part. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Remove()
        Me.Fire(RemoveTrigger.Remove)
    End Sub

    ''' <summary> Failures this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub RemoveFailure()
        Me.Fire(RemoveTrigger.RemoveFailed)
    End Sub

#End Region

End Class

''' <summary> Values that represent remove states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum RemoveState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the absent option. </summary>
    <Description("Absent")> Absent

    ''' <summary> An enum constant representing the present option. </summary>
    <Description("Part Present")> Present

    ''' <summary> An enum constant representing the released option. </summary>
    <Description("Waiting to remove")> Released

    ''' <summary> An enum constant representing the removed option. </summary>
    <Description("Part Removed")> Removed

    ''' <summary> An enum constant representing the not removed option. </summary>
    <Description("Part Not Removed")> NotRemoved
End Enum

''' <summary> Values that represent remove triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum RemoveTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the absent option. </summary>
    <Description("Absent")> Absent

    ''' <summary> An enum constant representing the insert option. </summary>
    <Description("Insert")> Insert

    ''' <summary> An enum constant representing the release option. </summary>
    <Description("Release")> Release

    ''' <summary> An enum constant representing the remove option. </summary>
    <Description("Remove")> Remove

    ''' <summary> An enum constant representing the remove failed option. </summary>
    <Description("Remove Failed")> RemoveFailed
End Enum
