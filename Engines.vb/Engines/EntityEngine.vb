Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for an Entity. </summary>
''' <remarks>
''' Describes the sequence of an entity, such as a test data entity from selection through usages
''' and completion. <para>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 5/8/2017 </para><para>
''' David, 4/26/2017 </para>
''' </remarks>
Public Class EntityEngine
    Inherits EngineBase(Of EntityState, EntityTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of EntityState, EntityTrigger)(EntityState.Initial))

        Dim stateConfiguration As StateMachine(Of EntityState, EntityTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(EntityState.Initial)
        stateConfiguration.Ignore(EntityTrigger.Initial)
        stateConfiguration.Ignore(EntityTrigger.Complete)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Ignore(EntityTrigger.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Ignore(EntityTrigger.Finalize)

        Me.EngagedStates.Add(EntityState.Finding)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Finding)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Ignore(EntityTrigger.Complete)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Ignore(EntityTrigger.Finalize)
        stateConfiguration.PermitReentry(EntityTrigger.Find)

        Me.EngagedStates.Add(EntityState.Creating)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Ignore(EntityTrigger.Complete)
        stateConfiguration.Ignore(EntityTrigger.Create)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Ignore(EntityTrigger.Finalize)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

        Me.EngagedStates.Add(EntityState.Created)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Permit(EntityTrigger.Complete, EntityState.Completed)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Ignore(EntityTrigger.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Permit(EntityTrigger.Finalize, EntityState.Finalized)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

        Me.EngagedStates.Add(EntityState.Fetching)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Fetching)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Permit(EntityTrigger.Complete, EntityState.Completed)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Ignore(EntityTrigger.Fetch)
        stateConfiguration.Permit(EntityTrigger.Finalize, EntityState.Finalized)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

        Me.EngagedStates.Add(EntityState.Engaged)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Permit(EntityTrigger.Complete, EntityState.Completed)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Ignore(EntityTrigger.Engage)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Permit(EntityTrigger.Finalize, EntityState.Finalized)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

        Me.EngagedStates.Add(EntityState.Finalized)
        stateConfiguration = Me.StateMachine.Configure(EntityState.Completed)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Ignore(EntityTrigger.Complete)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Permit(EntityTrigger.Finalize, EntityState.Finalized)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

        stateConfiguration = Me.StateMachine.Configure(EntityState.Finalized)
        stateConfiguration.Permit(EntityTrigger.Initial, EntityState.Initial)
        stateConfiguration.Permit(EntityTrigger.Complete, EntityState.Completed)
        stateConfiguration.Permit(EntityTrigger.Create, EntityState.Creating)
        stateConfiguration.Permit(EntityTrigger.Created, EntityState.Created)
        stateConfiguration.Permit(EntityTrigger.Engage, EntityState.Engaged)
        stateConfiguration.Permit(EntityTrigger.Fetch, EntityState.Fetching)
        stateConfiguration.Ignore(EntityTrigger.Finalize)
        stateConfiguration.Permit(EntityTrigger.Find, EntityState.Finding)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(EntityTrigger.Initial)
    End Sub

    ''' <summary> Sends the find trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Find()
        Me.Fire(EntityTrigger.Find)
    End Sub

    ''' <summary> Sends the fetch trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fetch()
        Me.Fire(EntityTrigger.Fetch)
    End Sub

    ''' <summary> Sends the Create trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Create()
        Me.Fire(EntityTrigger.Create)
    End Sub

    ''' <summary> Sends the Created trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Created()
        Me.Fire(EntityTrigger.Created)
    End Sub

    ''' <summary> Engages this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Engage()
        Me.Fire(EntityTrigger.Engage)
    End Sub

    ''' <summary> Completes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Complete()
        Me.Fire(EntityTrigger.Complete)
    End Sub

    ''' <summary> Finalize entity. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub FinalizeEntity()
        Me.Fire(EntityTrigger.Finalize)
    End Sub

#End Region

End Class

''' <summary> Values that represent Entity states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum EntityState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> Searching for an existing entity. </summary>
    <Description("Finding an Entity")> Finding

    ''' <summary> An entity is being created. </summary>
    <Description("Creating an Entity")> Creating

    ''' <summary> A new entity was created and saved into the database. </summary>
    <Description("Entity Created")> Created

    ''' <summary> An entity is being Fetched. </summary>
    <Description("Fetching an Entity")> Fetching

    ''' <summary> The entity is engaged. </summary>
    <Description("Entity is engaged")> Engaged

    ''' <summary> Entity is completed but perhaps not yet finalized, such as all serial number assigned. </summary>
    <Description("Entity Completed")> Completed

    ''' <summary> Entity is completed and finalized. </summary>
    <Description("Entity Finalized")> Finalized
End Enum

''' <summary> Values that represent Entity triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum EntityTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the find option. </summary>
    <Description("Find")> Find

    ''' <summary> An enum constant representing the fetch option. </summary>
    <Description("Fetch")> Fetch

    ''' <summary> An enum constant representing the create option. </summary>
    <Description("Create")> Create

    ''' <summary> An enum constant representing the created option. </summary>
    <Description("Created")> Created

    ''' <summary> An enum constant representing the engage option. </summary>
    <Description("Engage")> Engage

    ''' <summary> An enum constant representing the complete option. </summary>
    <Description("Complete")> Complete

    ''' <summary> An enum constant representing the finalize option. </summary>
    <Description("Finalize")> Finalize
End Enum
