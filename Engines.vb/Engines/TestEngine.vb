Imports System.ComponentModel

Imports Stateless

''' <summary>
''' Implements a finite automata for a Test process capable of monitoring a set of measurements.
''' </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class TestEngine
    Inherits EngineBase(Of TestState, TestTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of TestState, TestTrigger)(TestState.Initial))

        Dim stateConfiguration As StateMachine(Of TestState, TestTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Cancel)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Ignore(TestTrigger.Initial)
        stateConfiguration.Permit(TestTrigger.Prime, TestState.Priming)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        Me.EngagedStates.Add(TestState.Priming)
        stateConfiguration = Me.StateMachine.Configure(TestState.Priming)
        stateConfiguration.Permit(TestTrigger.Cancel, TestState.Canceled)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Prime)
        stateConfiguration.Permit(TestTrigger.Ready, TestState.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        Me.EngagedStates.Add(TestState.Ready)
        stateConfiguration = Me.StateMachine.Configure(TestState.Ready)
        stateConfiguration.Permit(TestTrigger.Cancel, TestState.Canceled)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Prime)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Permit(TestTrigger.Start, TestState.Busy)

        Me.EngagedStates.Add(TestState.Busy)
        stateConfiguration = Me.StateMachine.Configure(TestState.Busy)
        stateConfiguration.Permit(TestTrigger.Cancel, TestState.Canceled)
        stateConfiguration.Permit(TestTrigger.Complete, TestState.Completed)
        stateConfiguration.Permit(TestTrigger.Fail, TestState.Failed)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Prime)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(TestState.Failed)
        stateConfiguration.Permit(TestTrigger.Cancel, TestState.Canceled)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Prime)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        Me.EngagedStates.Add(TestState.Completed)
        stateConfiguration = Me.StateMachine.Configure(TestState.Completed)
        stateConfiguration.Ignore(TestTrigger.Cancel)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Permit(TestTrigger.Finalize, TestState.Finalized)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Permit(TestTrigger.Prime, TestState.Priming)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(TestState.Finalized)
        stateConfiguration.Ignore(TestTrigger.Cancel)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Permit(TestTrigger.Prime, TestState.Priming)
        stateConfiguration.Permit(TestTrigger.Ready, TestState.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(TestState.Canceled)
        stateConfiguration.Ignore(TestTrigger.Cancel)
        stateConfiguration.Ignore(TestTrigger.Complete)
        stateConfiguration.Ignore(TestTrigger.Fail)
        stateConfiguration.Ignore(TestTrigger.Finalize)
        stateConfiguration.Permit(TestTrigger.Initial, TestState.Initial)
        stateConfiguration.Ignore(TestTrigger.Prime)
        stateConfiguration.Ignore(TestTrigger.Ready)
        stateConfiguration.Ignore(TestTrigger.Start)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Cancels this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Cancel()
        Me.Fire(TestTrigger.Cancel)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(TestTrigger.Initial)
    End Sub

    ''' <summary> Completes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Complete()
        If Me.StopRequested Then
            Me.Fire(TestTrigger.Cancel)
        Else
            Me.Fire(TestTrigger.Complete)
        End If
    End Sub

    ''' <summary> Tests finalize. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub FinalizeTest()
        If Me.StopRequested Then
            Me.Fire(TestTrigger.Cancel)
        Else
            Me.Fire(TestTrigger.Finalize)
        End If
    End Sub

    ''' <summary> Primes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Prime()
        If Me.StopRequested Then
            Me.Fire(TestTrigger.Cancel)
        Else
            Me.Fire(TestTrigger.Prime)
        End If
    End Sub

    ''' <summary> Readies this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Ready()
        If Me.StopRequested Then
            Me.Fire(TestTrigger.Cancel)
        Else
            Me.Fire(TestTrigger.Ready)
        End If
    End Sub

    ''' <summary> Starts this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Start()
        If Me.StopRequested Then
            Me.Fire(TestTrigger.Cancel)
        Else
            Me.Fire(TestTrigger.Start)
        End If
    End Sub

    ''' <summary> Test failed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fail()
        Me.Fire(TestTrigger.Fail)
    End Sub

#End Region

End Class

''' <summary> Values that represent test states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum TestState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the priming option. </summary>
    <Description("Priming")> Priming

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the busy option. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the completed option. </summary>
    <Description("Completed")> Completed

    ''' <summary> An enum constant representing the finalized option. </summary>
    <Description("Finalized")> Finalized

    ''' <summary> An enum constant representing the canceled option. </summary>
    <Description("Testing Canceled")> Canceled
End Enum

''' <summary> Values that represent test triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum TestTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the prime option. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the start option. </summary>
    <Description("Start")> Start

    ''' <summary> An enum constant representing the complete option. </summary>
    <Description("Complete")> Complete

    ''' <summary> An enum constant representing the finalize option. </summary>
    <Description("Finalize")> Finalize

    ''' <summary> An enum constant representing the fail option. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the cancel option. </summary>
    <Description("Cancel test")> Cancel
End Enum
