﻿Imports System.ComponentModel
Imports Stateless
''' <summary> A loading parts engine. </summary>
''' <remarks> This engine can be used for sequencing the loading and unloading process. (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2017 </para></remarks>
Public Class LoadingEngine
    Inherits EngineBase(Of LoadingState, LoadingTrigger)

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of LoadingState, LoadingTrigger)(LoadingState.Initial))

        With Me.Engine.Configure(LoadingState.Initial)
            .Ignore(LoadingTrigger.Initial)
            .Ignore(LoadingTrigger.Loaded)
            .Permit(LoadingTrigger.Loading, LoadingState.Loading)
            .Permit(LoadingTrigger.Ready, LoadingState.ReadyToLoad)
        End With

        With Me.Engine.Configure(LoadingState.ReadyToLoad)
            .Permit(LoadingTrigger.Initial, LoadingState.Initial)
            .Permit(LoadingTrigger.Loading, LoadingState.Loading)
            .Permit(LoadingTrigger.Loaded, LoadingState.Loaded)
            .Ignore(LoadingTrigger.Ready)
        End With

        With Me.Engine.Configure(LoadingState.Loading)
            .Permit(LoadingTrigger.Initial, LoadingState.Initial)
            .Ignore(LoadingTrigger.Loading)
            .Permit(LoadingTrigger.Loaded, LoadingState.Loaded)
            .Permit(LoadingTrigger.Ready, LoadingState.ReadyToLoad)
        End With

        With Me.Engine.Configure(LoadingState.Loaded)
            .Permit(LoadingTrigger.Initial, LoadingState.Initial)
            .Permit(LoadingTrigger.Loading, LoadingState.Loading)
            .Ignore(LoadingTrigger.Loaded)
            .Permit(LoadingTrigger.Ready, LoadingState.ReadyToLoad)
        End With

    End Sub

#End Region

#Region " COMMANDs "

    Public Sub Initial()
        Me.Engine.Fire(LoadingTrigger.Initial)
    End Sub

    Public Sub Ready()
        Me.Engine.Fire(LoadingTrigger.Ready)
    End Sub

    Public Sub Loading()
        Me.Engine.Fire(LoadingTrigger.Loading)
    End Sub

    Public Sub Loaded()
        Me.Engine.Fire(LoadingTrigger.Loaded)
    End Sub

#End Region

End Class

''' <summary> Values that represent Loading states. </summary>
Public Enum LoadingState
    <Description("Initial")> Initial
    <Description("Ready to Load")> ReadyToLoad
    <Description("Loading")> Loading
    <Description("Loaded")> Loaded
End Enum

''' <summary> Values that represent Loading triggers. </summary>
Public Enum LoadingTrigger
    <Description("Initial: Any -> Initial")> Initial
    <Description("Ready: Initial; Loaded --> Ready")> Ready
    <Description("Loading: Ready --> Loading")> Loading
    <Description("Loaded: Loading -> Loaded")> Loaded
End Enum