﻿Imports System.ComponentModel
Imports Stateless
''' <summary> An unloading parts engine. </summary>
''' <remarks> This engine can be used for sequencing the loading and unloading process. (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2017 </para></remarks>
Public Class UnloadingEngine
    Inherits EngineBase(Of UnloadingState, UnloadingTrigger)

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of UnloadingState, UnloadingTrigger)(UnloadingState.Initial))

        With Me.Engine.Configure(UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Alert)
            .Ignore(UnloadingTrigger.Initial)
            .Permit(UnloadingTrigger.Ready, UnloadingState.Ready)
            .Ignore(UnloadingTrigger.UnloadBad)
            .Ignore(UnloadingTrigger.UnloadDone)
            .Ignore(UnloadingTrigger.UnloadError)
            .Ignore(UnloadingTrigger.UnloadGood)
        End With

        With Me.Engine.Configure(UnloadingState.Ready)
            .Permit(UnloadingTrigger.Alert, UnloadingState.UnloadingBadAlert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Ready)
            .Permit(UnloadingTrigger.UnloadBad, UnloadingState.UnloadingBad)
            .Ignore(UnloadingTrigger.UnloadDone)
            .Permit(UnloadingTrigger.UnloadError, UnloadingState.UnloadingBadError)
            .Permit(UnloadingTrigger.UnloadGood, UnloadingState.UnloadingGood)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingBad)
            .Permit(UnloadingTrigger.Alert, UnloadingState.UnloadingBadAlert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Ready)
            .Ignore(UnloadingTrigger.UnloadBad)
            .Permit(UnloadingTrigger.UnloadDone, UnloadingState.UnloadingDone)
            .Permit(UnloadingTrigger.UnloadError, UnloadingState.UnloadingBadError)
            .Permit(UnloadingTrigger.UnloadGood, UnloadingState.UnloadingGood)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingBadAlert)
            .Ignore(UnloadingTrigger.Alert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Ready)
            .Permit(UnloadingTrigger.UnloadBad, UnloadingState.UnloadingBad)
            .Permit(UnloadingTrigger.UnloadDone, UnloadingState.UnloadingDone)
            .Permit(UnloadingTrigger.UnloadError, UnloadingState.UnloadingBadError)
            .Permit(UnloadingTrigger.UnloadGood, UnloadingState.UnloadingGood)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingBadError)
            .Ignore(UnloadingTrigger.Alert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Ready)
            .Permit(UnloadingTrigger.UnloadBad, UnloadingState.UnloadingBad)
            .Permit(UnloadingTrigger.UnloadDone, UnloadingState.UnloadingDone)
            .Ignore(UnloadingTrigger.UnloadError)
            .Permit(UnloadingTrigger.UnloadGood, UnloadingState.UnloadingGood)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingGood)
            .Permit(UnloadingTrigger.Alert, UnloadingState.UnloadingGoodAlert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Ignore(UnloadingTrigger.Ready)
            .Ignore(UnloadingTrigger.UnloadBad)
            .Permit(UnloadingTrigger.UnloadDone, UnloadingState.UnloadingDone)
            .Ignore(UnloadingTrigger.UnloadError)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingGoodAlert)
            .Ignore(UnloadingTrigger.Alert)
            .Ignore(UnloadingTrigger.Ready)
            .Ignore(UnloadingTrigger.UnloadBad)
            .Permit(UnloadingTrigger.UnloadDone, UnloadingState.UnloadingDone)
            .Ignore(UnloadingTrigger.UnloadError)
            .Permit(UnloadingTrigger.UnloadGood, UnloadingState.UnloadingGood)
        End With

        With Me.Engine.Configure(UnloadingState.UnloadingDone)
            .Ignore(UnloadingTrigger.Alert)
            .Permit(UnloadingTrigger.Initial, UnloadingState.Initial)
            .Permit(UnloadingTrigger.Ready, UnloadingState.Ready)
            .Ignore(UnloadingTrigger.UnloadBad)
            .Ignore(UnloadingTrigger.UnloadDone)
            .Ignore(UnloadingTrigger.UnloadError)
            .Ignore(UnloadingTrigger.UnloadGood)
        End With

    End Sub

#End Region

#Region " COMMANDs "

    Public Sub Alert()
        Me.Engine.Fire(UnloadingTrigger.Alert)
    End Sub

    Public Sub Initial()
        Me.Engine.Fire(UnloadingTrigger.Initial)
    End Sub

    Public Sub Ready()
        Me.Engine.Fire(UnloadingTrigger.Ready)
    End Sub

    Public Sub UnloadBad()
        Me.Engine.Fire(UnloadingTrigger.UnloadBad)
    End Sub

    Public Sub UnloadGood()
        Me.Engine.Fire(UnloadingTrigger.UnloadGood)
    End Sub

    Public Sub UnloadDone()
        Me.Engine.Fire(UnloadingTrigger.UnloadDone)
    End Sub

    Public Sub UnloadError()
        Me.Engine.Fire(UnloadingTrigger.UnloadError)
    End Sub

#End Region

End Class

''' <summary> Values that represent unloading states. </summary>
Public Enum UnloadingState
    <Description("Initial")> Initial
    <Description("Ready to Unload")> Ready
    <Description("Unloading Failed Parts")> UnloadingBad
    <Description("Unloading Bad Parts Alert")> UnloadingBadAlert
    <Description("Unloading Bad Parts Error")> UnloadingBadError
    <Description("Unloading Good Parts")> UnloadingGood
    <Description("Unloading Good Parts Alert")> UnloadingGoodAlert
    <Description("Unloading Done")> UnloadingDone
End Enum

''' <summary> Values that represent unloading triggers. </summary>
Public Enum UnloadingTrigger
    <Description("Initial: Any -> Initial")> Initial
    <Description("Ready: Any --> Ready")> Ready
    <Description("Unload Bad: Ready -> Unload bad")> UnloadBad
    <Description("Unload Done: Ready, Unloading Bad, Unloading Bad Error -> Unloading done")> UnloadDone
    <Description("Unload Good: Ready, Unloading Bad -> Unloading Good")> UnloadGood

    ''' <summary> Operator validation requested while unloading bad and bad present or while unloading good and good part still present. </summary>
    <Description("Alert: Unloading Bad/Good -> Unload Bad/Good Alert")> Alert

    ''' <summary> A good part was removed during the unloading of bad parts. </summary>
    <Description("Error: Unloading Bad -> Unload Bad Error")> UnloadError
End Enum