﻿Imports System.ComponentModel
Imports Stateless
''' <summary> A device under test engine for sequencing device loading, testing and unloading. </summary>
''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2017 </para></remarks>
Public Class DeviceUnderTestEngine
    Inherits EngineBase(Of DeviceUnderTestState, DeviceUnderTestTrigger)

#Region " CONSTRUCTOR "


    ''' <summary> Constructor. </summary>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New DeviceUnderTestStateMachine)

        With Me.Engine.Configure(DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Initial)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Absent)
        With Me.Engine.Configure(DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Permit(DeviceUnderTestTrigger.TestEnd, DeviceUnderTestState.Skipped)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Present)
        With Me.Engine.Configure(DeviceUnderTestState.Present)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .Permit(DeviceUnderTestTrigger.Ready, DeviceUnderTestState.Ready)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Permit(DeviceUnderTestTrigger.TestEnd, DeviceUnderTestState.Skipped)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Ready)
        With Me.Engine.Configure(DeviceUnderTestState.Ready)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .PermitReentry(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Permit(DeviceUnderTestTrigger.TestStart, DeviceUnderTestState.Testing)
            .Permit(DeviceUnderTestTrigger.TestEnd, DeviceUnderTestState.Skipped)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Testing)
        With Me.Engine.Configure(DeviceUnderTestState.Testing)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Permit(DeviceUnderTestTrigger.TestEnd, DeviceUnderTestState.Tested)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Tested)
        With Me.Engine.Configure(DeviceUnderTestState.Tested)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Removing)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Removing)
        With Me.Engine.Configure(DeviceUnderTestState.Removing)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Removed)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Removed)
        With Me.Engine.Configure(DeviceUnderTestState.Removed)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Ignore(DeviceUnderTestTrigger.Insert)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Permit(DeviceUnderTestTrigger.Save, DeviceUnderTestState.Saving)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        With Me.Engine.Configure(DeviceUnderTestState.RemoveException)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Initial)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Absent)
            .Ignore(DeviceUnderTestTrigger.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        Me.EngagedStates.Add(DeviceUnderTestState.Saving)
        With Me.Engine.Configure(DeviceUnderTestState.Saving)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Ignore(DeviceUnderTestTrigger.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Permit(DeviceUnderTestTrigger.SavingDone, DeviceUnderTestState.Saved)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        With Me.Engine.Configure(DeviceUnderTestState.Saved)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Ignore(DeviceUnderTestTrigger.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

        With Me.Engine.Configure(DeviceUnderTestState.Skipped)
            .Permit(DeviceUnderTestTrigger.Initial, DeviceUnderTestState.Absent)
            .Permit(DeviceUnderTestTrigger.Insert, DeviceUnderTestState.Present)
            .Ignore(DeviceUnderTestTrigger.Ready)
            .Ignore(DeviceUnderTestTrigger.Remove)
            .Permit(DeviceUnderTestTrigger.Remove, DeviceUnderTestState.Removing)
            .Permit(DeviceUnderTestTrigger.RemoveException, DeviceUnderTestState.RemoveException)
            .Ignore(DeviceUnderTestTrigger.Save)
            .Ignore(DeviceUnderTestTrigger.SavingDone)
            .Ignore(DeviceUnderTestTrigger.TestStart)
            .Ignore(DeviceUnderTestTrigger.TestEnd)
        End With

    End Sub

#End Region

#Region " MACHINE "

    Private _PresenceStatus As PresenceStatus

    ''' <summary> Gets or sets the presence status when testing status. </summary>
    ''' <value> The presence status. </value>
    Public Property PresenceStatus As PresenceStatus
        Get
            Return Me._PresenceStatus
        End Get
        Set(value As PresenceStatus)
            If value <> Me.PresenceStatus Then
                Me._PresenceStatus = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Report state change. </summary>
    ''' <param name="transition"> The transition. </param>
    Protected Overrides Sub OnTransitioned(transition As StateMachine(Of DeviceUnderTestState, DeviceUnderTestTrigger).Transition)
        MyBase.OnTransitioned(transition)
        If transition IsNot Nothing Then Me.OnTransitioned(transition.Destination)
    End Sub

    ''' <summary> Executes the transitioned action. </summary>
    ''' <param name="state"> The state. </param>
    Private Overloads Sub OnTransitioned(ByVal state As DeviceUnderTestState)
        Select Case state
            Case DeviceUnderTestState.Absent
                Me.PresenceStatus = PresenceStatus.Absent
                Me.AsyncNotifyPropertyChanged(NameOf(DeviceUnderTestState.PresenceStatus))
            Case DeviceUnderTestState.Present
                Me.PresenceStatus = PresenceStatus.Present
            Case DeviceUnderTestState.Testing
                Me.PresenceStatus = PresenceStatus.Present
            Case DeviceUnderTestState.Saved

            Case DeviceUnderTestState.Removed
                Me.PresenceStatus = PresenceStatus.Removed
            Case DeviceUnderTestState.RemoveException
                Me.PresenceStatus = PresenceStatus.RemovalException
            Case Else
                Me.OnEngineFailed($"Unhandled {Me.Name} on entry state {state}")
        End Select
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Sends the initial trigger. </summary>
    Public Sub Initial()
        Me.Engine.Fire(DeviceUnderTestTrigger.Initial)
    End Sub

    ''' <summary> Sends the insert trigger. </summary>
    Public Sub Insert()
        Me.Engine.Fire(DeviceUnderTestTrigger.Insert)
    End Sub

    ''' <summary> Sends the ready trigger. </summary>
    Public Sub Ready()
        Me.Engine.Fire(DeviceUnderTestTrigger.Ready)
    End Sub

    Public Sub TestStart()
        Me.Engine.Fire(DeviceUnderTestTrigger.TestStart)
    End Sub

    ''' <summary> Sends the test end trigger. </summary>
    Public Sub TestEnd()
        Me.Engine.Fire(DeviceUnderTestTrigger.TestEnd)
    End Sub

    ''' <summary> Sends the remove trigger. </summary>
    Public Sub Remove()
        Me.Engine.Fire(DeviceUnderTestTrigger.Remove)
    End Sub

    ''' <summary> Sends the remove exception trigger. </summary>
    Public Sub RemoveException()
        Me.Engine.Fire(DeviceUnderTestTrigger.RemoveException)
    End Sub

    ''' <summary> Sends the save trigger. </summary>
    Public Sub Save()
        Me.Engine.Fire(DeviceUnderTestTrigger.Save)
    End Sub

    Public Sub SavingDone()
        Me.Engine.Fire(DeviceUnderTestTrigger.SavingDone)
    End Sub

#End Region

End Class

''' <summary> A device under test state machine. </summary>
''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/28/2017 </para></remarks>
Public Class DeviceUnderTestStateMachine
    Inherits StateMachine(Of DeviceUnderTestState, DeviceUnderTestTrigger)

    Public Sub New()
        MyBase.New(DeviceUnderTestState.Initial)
    End Sub
End Class

''' <summary> Values that represent device under test states. </summary>
Public Enum DeviceUnderTestState
    <Description("Initial")> Initial
    <Description("Absent")> Absent
    <Description("Present")> Present
    <Description("Ready")> Ready
    <Description("Testing")> Testing
    <Description("Tested")> Tested
    <Description("Removing")> Removing
    <Description("Removed")> Removed
    <Description("Remove Exception")> RemoveException
    <Description("Saving Data")> Saving
    <Description("Data Saved")> Saved
    <Description("Test Skipped")> Skipped
End Enum

''' <summary> Values that represent device under test triggers. </summary>
Public Enum DeviceUnderTestTrigger
    <Description("Initial: Any -> Initial")> Initial
    <Description("Absent: Any -> Absent")> Remove
    <Description("Insert")> Insert
    <Description("Ready")> Ready
    <Description("Test Start")> TestStart
    <Description("Test End")> TestEnd
    <Description("Remove Exception")> RemoveException
    <Description("Save")> Save
    <Description("Saving Done")> SavingDone
End Enum

''' <summary> Values that represent presence states. </summary>
Public Enum PresenceStatus
    <Description("Absent")> Absent
    <Description("Present")> Present
    <Description("Removed")> Removed
    <Description("Removal Exception")> RemovalException
End Enum
