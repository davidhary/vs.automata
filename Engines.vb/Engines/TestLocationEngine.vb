Imports System.ComponentModel

Imports Stateless

''' <summary>
''' Implements a finite automata for a Test Location process sequencing the loading, testing and
''' unloading of a single item under test.
''' </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class TestLocationEngine
    Inherits EngineBase(Of TestLocationState, TestLocationTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New TestLocationStateMachine)

        Dim stateConfiguration As StateMachine(Of TestLocationState, TestLocationTrigger).StateConfiguration

        Me._StatusInfo = New TestLocationStatusInfo()
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Cancel)
        stateConfiguration.Ignore(TestLocationTrigger.Initial)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Permit(TestLocationTrigger.Extract, TestLocationState.Absent)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        Me.EngagedStates.Add(TestLocationState.Absent)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Absent)
        stateConfiguration.Permit(TestLocationTrigger.Cancel, TestLocationState.Removed)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Permit(TestLocationTrigger.Removed, TestLocationState.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Permit(TestLocationTrigger.TestEnd, TestLocationState.Removed)

        Me.EngagedStates.Add(TestLocationState.Present)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Present)
        stateConfiguration.Permit(TestLocationTrigger.Cancel, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Insert)
        stateConfiguration.Permit(TestLocationTrigger.Ready, TestLocationState.Ready)
        stateConfiguration.Permit(TestLocationTrigger.Extract, TestLocationState.Absent)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Permit(TestLocationTrigger.Removing, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Permit(TestLocationTrigger.TestEnd, TestLocationState.Removing)

        Me.EngagedStates.Add(TestLocationState.Ready)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Ready)
        stateConfiguration.Permit(TestLocationTrigger.Cancel, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.PermitReentry(TestLocationTrigger.Ready)
        stateConfiguration.Permit(TestLocationTrigger.Extract, TestLocationState.Absent)
        stateConfiguration.Permit(TestLocationTrigger.Removed, TestLocationState.Removed)
        stateConfiguration.Permit(TestLocationTrigger.Removing, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Permit(TestLocationTrigger.TestStart, TestLocationState.Testing)
        stateConfiguration.Permit(TestLocationTrigger.TestEnd, TestLocationState.Removing)

        Me.EngagedStates.Add(TestLocationState.Testing)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Testing)
        stateConfiguration.Permit(TestLocationTrigger.Cancel, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Insert)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Permit(TestLocationTrigger.Removed, TestLocationState.Removed)
        stateConfiguration.Permit(TestLocationTrigger.Removing, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Permit(TestLocationTrigger.TestEnd, TestLocationState.Tested)

        Me.EngagedStates.Add(TestLocationState.Tested)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Tested)
        stateConfiguration.Permit(TestLocationTrigger.Cancel, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Insert)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Permit(TestLocationTrigger.Removed, TestLocationState.Removed)
        stateConfiguration.Permit(TestLocationTrigger.Removing, TestLocationState.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        Me.EngagedStates.Add(TestLocationState.Removing)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Removing)
        stateConfiguration.PermitReentry(TestLocationTrigger.Cancel)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Insert)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Permit(TestLocationTrigger.Removed, TestLocationState.Removed)
        stateConfiguration.PermitReentry(TestLocationTrigger.Removing)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        Me.EngagedStates.Add(TestLocationState.Removed)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Cancel)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Ignore(TestLocationTrigger.Insert)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Permit(TestLocationTrigger.RemoveException, TestLocationState.RemoveException)
        stateConfiguration.Permit(TestLocationTrigger.Save, TestLocationState.Saving)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        stateConfiguration = Me.StateMachine.Configure(TestLocationState.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Cancel)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Initial)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Removing)
        stateConfiguration.Ignore(TestLocationTrigger.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        Me.EngagedStates.Add(TestLocationState.Saving)
        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Saving)
        stateConfiguration.Ignore(TestLocationTrigger.Cancel)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Absent)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Removing)
        stateConfiguration.Ignore(TestLocationTrigger.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Permit(TestLocationTrigger.SavingDone, TestLocationState.Saved)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

        stateConfiguration = Me.StateMachine.Configure(TestLocationState.Saved)
        stateConfiguration.Ignore(TestLocationTrigger.Cancel)
        stateConfiguration.Permit(TestLocationTrigger.Initial, TestLocationState.Absent)
        stateConfiguration.Permit(TestLocationTrigger.Insert, TestLocationState.Present)
        stateConfiguration.Ignore(TestLocationTrigger.Ready)
        stateConfiguration.Ignore(TestLocationTrigger.Extract)
        stateConfiguration.Ignore(TestLocationTrigger.Removed)
        stateConfiguration.Ignore(TestLocationTrigger.Removing)
        stateConfiguration.Ignore(TestLocationTrigger.RemoveException)
        stateConfiguration.Ignore(TestLocationTrigger.Save)
        stateConfiguration.Ignore(TestLocationTrigger.SavingDone)
        stateConfiguration.Ignore(TestLocationTrigger.TestStart)
        stateConfiguration.Ignore(TestLocationTrigger.TestEnd)

    End Sub

#End Region

#Region " MACHINE "

    ''' <summary> Gets or sets information describing the status. </summary>
    ''' <value> Information describing the status. </value>
    Public ReadOnly Property StatusInfo As TestLocationStatusInfo

    ''' <summary> Gets or sets the test location status. </summary>
    ''' <value> The presence status. </value>
    Public Property Statuses As TestLocationStatuses
        Get
            Return Me.StatusInfo.Statuses
        End Get
        Set(value As TestLocationStatuses)
            If value <> Me.Statuses OrElse value = TestLocationStatuses.None Then
                Me._StatusInfo.Statuses = value
                Me.AsyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Report state change. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    Protected Overrides Sub OnTransitioned(transition As StateMachine(Of TestLocationState, TestLocationTrigger).Transition)
        MyBase.OnTransitioned(transition)
        If transition IsNot Nothing Then Me.OnTransitioned(transition.Destination)
    End Sub

    ''' <summary> Executes the transitioned action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="state"> The state. </param>
    Private Overloads Sub OnTransitioned(ByVal state As TestLocationState)
        Select Case state
            Case TestLocationState.Initial
                Me.Statuses = TestLocationStatuses.None
            Case TestLocationState.Absent
                Me.Statuses = TestLocationStatuses.Vacant
            Case TestLocationState.Present
                ' this clears the absent status
                Me.Statuses = TestLocationStatuses.Full
            Case TestLocationState.Testing
                ' if the test location is used for testing, it means that the test location was full when 
                ' testing started. The test location is then said to be engaged.
                Me.Statuses = Me.Statuses Or TestLocationStatuses.Engaged
            Case TestLocationState.Tested
                Me.Statuses = Me.Statuses Or TestLocationStatuses.Tested
            Case TestLocationState.Saved
                Me.Statuses = Me.Statuses Or TestLocationStatuses.Saved
            Case TestLocationState.Removed
                ' once the device is removed, it is marked as removed.  
                Me.Statuses = Me.Statuses Or TestLocationStatuses.Removed
            Case TestLocationState.RemoveException
                Me.Statuses = Me.Statuses Or TestLocationStatuses.RemovalException
        End Select
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Sends the initial trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(TestLocationTrigger.Initial)
    End Sub

    ''' <summary> Sends the insert trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Insert()
        Me.Fire(TestLocationTrigger.Insert)
    End Sub

    ''' <summary> Sends the ready trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub ReadyToTest()
        Me.Fire(TestLocationTrigger.Ready)
    End Sub

    ''' <summary> Tests start. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub TestStart()
        Me.Fire(TestLocationTrigger.TestStart)
    End Sub

    ''' <summary> Sends the test end trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub TestEnd()
        Me.Fire(TestLocationTrigger.TestEnd)
    End Sub

    ''' <summary> Sends the ready to remove trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Extract()
        Me.Fire(TestLocationTrigger.Extract)
    End Sub

    ''' <summary> Removings this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Removing()
        Me.Fire(TestLocationTrigger.Removing)
    End Sub

    ''' <summary> Sends the remove trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Removed()
        Me.Fire(TestLocationTrigger.Removed)
    End Sub

    ''' <summary> Sends the remove exception trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub RemoveException()
        Me.Fire(TestLocationTrigger.RemoveException)
    End Sub

    ''' <summary> Sends the save trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Save()
        Me.Fire(TestLocationTrigger.Save)
    End Sub

    ''' <summary> Saving done. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub SavingDone()
        Me.Fire(TestLocationTrigger.SavingDone)
    End Sub

    ''' <summary> Cancels testing. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Cancel()
        Me.Statuses = Me.Statuses Or TestLocationStatuses.Canceled
        Me.Fire(TestLocationTrigger.Cancel)
    End Sub

#End Region

End Class

''' <summary> A test location state machine. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class TestLocationStateMachine
    Inherits StateMachine(Of TestLocationState, TestLocationTrigger)

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub New()
        MyBase.New(TestLocationState.Initial)
    End Sub
End Class

''' <summary> Values that represent test location states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum TestLocationState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the absent option. </summary>
    <Description("Absent")> Absent

    ''' <summary> An enum constant representing the present option. </summary>
    <Description("Present")> Present

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the testing option. </summary>
    <Description("Testing")> Testing

    ''' <summary> An enum constant representing the tested option. </summary>
    <Description("Tested")> Tested

    ''' <summary> An enum constant representing the removing option. </summary>
    <Description("Removing")> Removing

    ''' <summary> An enum constant representing the removed option. </summary>
    <Description("Removed")> Removed

    ''' <summary> An enum constant representing the remove exception option. </summary>
    <Description("Remove Exception")> RemoveException

    ''' <summary> An enum constant representing the saving option. </summary>
    <Description("Saving Data")> Saving

    ''' <summary> An enum constant representing the saved option. </summary>
    <Description("Data Saved")> Saved
End Enum

''' <summary> Values that represent test location triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum TestLocationTrigger

    ''' <summary> . </summary>
    <Description("Initial: Any -> Initial")> Initial

    ''' <summary> . </summary>
    <Description("Extract: Present; Ready -> Absent")> Extract

    ''' <summary> An enum constant representing the insert option. </summary>
    <Description("Insert")> Insert

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the test start option. </summary>
    <Description("Test Start")> TestStart

    ''' <summary> An enum constant representing the test end option. </summary>
    <Description("Test End")> TestEnd

    ''' <summary> . </summary>
    <Description("Removing: Any -> Removing")> Removing

    ''' <summary> . </summary>
    <Description("Removed: Removing -> Removed")> Removed

    ''' <summary> An enum constant representing the remove exception option. </summary>
    <Description("Remove Exception")> RemoveException

    ''' <summary> An enum constant representing the save option. </summary>
    <Description("Save")> Save

    ''' <summary> An enum constant representing the saving done option. </summary>
    <Description("Saving Done")> SavingDone

    ''' <summary> An enum constant representing the cancel option. </summary>
    <Description("Cancel testing")> Cancel
End Enum

''' <summary> Values that represent the test location statuses. </summary>
''' <remarks> David, 2020-10-01. </remarks>
<Flags> Public Enum TestLocationStatuses
    <Description("Initial")> None
    <Description("Vacant")> Vacant = 1
    <Description("Full")> Full = Vacant << 1
    <Description("Engaged")> Engaged = Full << 1
    <Description("Tested")> Tested = Engaged << 1
    <Description("Removed")> Removed = Tested << 1
    <Description("Removal Exception")> RemovalException = Removed << 1
    <Description("Saved")> Saved = RemovalException << 1
    <Description("Canceled")> Canceled = Saved << 1
End Enum

''' <summary> Information about the test location status. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/19/2017 </para>
''' </remarks>
Public Class TestLocationStatusInfo

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets or sets the statuses. </summary>
    ''' <value> The statuses. </value>
    Public Property Statuses As TestLocationStatuses

    ''' <summary> Query if this object is initial. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if initial; otherwise <c>false</c> </returns>
    Public Function IsInitial() As Boolean
        Return TestLocationStatuses.None = Me.Statuses
    End Function

    ''' <summary> True if the test location is vacant or was vacant when testing was ready. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if vacant; otherwise <c>false</c> </returns>
    Public Function IsVacant() As Boolean
        Return TestLocationStatuses.Vacant = (Me.Statuses And TestLocationStatuses.Vacant)
    End Function

    ''' <summary> True if the test location is Full or was Full when testing was ready. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if Full; otherwise <c>false</c> </returns>
    Public Function IsFull() As Boolean
        Return TestLocationStatuses.Full = (Me.Statuses And TestLocationStatuses.Full)
    End Function

    ''' <summary> True if test location actions canceled. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if canceled; otherwise <c>false</c> </returns>
    Public Function IsCanceled() As Boolean
        Return TestLocationStatuses.Canceled = (Me.Statuses And TestLocationStatuses.Canceled)
    End Function

    ''' <summary>
    ''' True if the test location is engaged in testing, that is if it was full when testing started.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if test location is engaged; otherwise <c>false</c> </returns>
    Public Function IsEngaged() As Boolean
        Return TestLocationStatuses.Engaged = (Me.Statuses And TestLocationStatuses.Engaged)
    End Function

    ''' <summary> Query if a part was removed from the test location. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if part removed; otherwise <c>false</c> </returns>
    Public Function IsRemoved() As Boolean
        Return TestLocationStatuses.Removed = (Me.Statuses And TestLocationStatuses.Removed)
    End Function

    ''' <summary> Query if this object is removal exception. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if removal exception; otherwise <c>false</c> </returns>
    Public Function IsRemovalException() As Boolean
        Return TestLocationStatuses.RemovalException = (Me.Statuses And TestLocationStatuses.RemovalException)
    End Function

    ''' <summary> Query if this object is saved. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if saved; otherwise <c>false</c> </returns>
    Public Function IsSaved() As Boolean
        Return TestLocationStatuses.Saved = (Me.Statuses And TestLocationStatuses.Saved)
    End Function

    ''' <summary> Query if this object is part tested. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if part tested; otherwise <c>false</c> </returns>
    Public Function IsTested() As Boolean
        Return TestLocationStatuses.Tested = (Me.Statuses And TestLocationStatuses.Tested)
    End Function

End Class
