Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for an Open/Close process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class OpenCloseEngine
    Inherits EngineBase(Of OpenCloseState, OpenCloseTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="initialState"> State of the initial. </param>
    Public Sub New(ByVal name As String, ByVal initialState As OpenCloseState)
        MyBase.New(name, New StateMachine(Of OpenCloseState, OpenCloseTrigger)(initialState))

        Dim stateConfiguration As StateMachine(Of OpenCloseState, OpenCloseTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(OpenCloseState.Initial)
        stateConfiguration.Permit(OpenCloseTrigger.Close, OpenCloseState.Close)
        stateConfiguration.Permit(OpenCloseTrigger.Open, OpenCloseState.Open)
        stateConfiguration.Ignore(OpenCloseTrigger.Initial)

        Me.EngagedStates.Add(OpenCloseState.Open)
        stateConfiguration = Me.StateMachine.Configure(OpenCloseState.Open)
        stateConfiguration.Permit(OpenCloseTrigger.Initial, OpenCloseState.Initial)
        stateConfiguration.Permit(OpenCloseTrigger.Close, OpenCloseState.Close)
        stateConfiguration.Ignore(OpenCloseTrigger.Open)

        Me.EngagedStates.Add(OpenCloseState.Close)
        stateConfiguration = Me.StateMachine.Configure(OpenCloseState.Close)
        stateConfiguration.Permit(OpenCloseTrigger.Initial, OpenCloseState.Initial)
        stateConfiguration.Ignore(OpenCloseTrigger.Close)
        stateConfiguration.Permit(OpenCloseTrigger.Open, OpenCloseState.Open)

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        Me.New(name, OpenCloseState.Initial)
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Opens this object. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Open()
        Me.Fire(OpenCloseTrigger.Open)
    End Sub

    ''' <summary> Closes this object. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Close()
        Me.Fire(OpenCloseTrigger.Close)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Initial()
        Me.Fire(OpenCloseTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent open close states. </summary>
''' <remarks> David, 2020-08-22. </remarks>
Public Enum OpenCloseState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the open] option. </summary>
    <Description("Open")> [Open]

    ''' <summary> An enum constant representing the close option. </summary>
    <Description("Close")> Close
End Enum

''' <summary> Values that represent open close triggers. </summary>
''' <remarks> David, 2020-08-22. </remarks>
Public Enum OpenCloseTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the open option. </summary>
    <Description("Open")> Open

    ''' <summary> An enum constant representing the close option. </summary>
    <Description("Close")> Close
End Enum
