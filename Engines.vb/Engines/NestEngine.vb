Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for monitoring part presence or absence. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class NestEngine
    Inherits EngineBase(Of NestState, NestTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of NestState, NestTrigger)(NestState.Initial))

        Dim stateConfiguration As StateMachine(Of NestState, NestTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(NestState.Initial)
        stateConfiguration.PermitReentry(NestTrigger.Initial)
        stateConfiguration.Permit(NestTrigger.Insert, NestState.Occupied)
        stateConfiguration.Permit(NestTrigger.Remove, NestState.Empty)

        Me.EngagedStates.Add(NestState.Empty)
        stateConfiguration = Me.StateMachine.Configure(NestState.Empty)
        stateConfiguration.Permit(NestTrigger.Initial, NestState.Initial)
        stateConfiguration.Permit(NestTrigger.Insert, NestState.Occupied)
        stateConfiguration.Ignore(NestTrigger.Remove)

        Me.EngagedStates.Add(NestState.Occupied)
        stateConfiguration = Me.StateMachine.Configure(NestState.Occupied)
        stateConfiguration.Permit(NestTrigger.Initial, NestState.Initial)
        stateConfiguration.Ignore(NestTrigger.Insert)
        stateConfiguration.Permit(NestTrigger.Remove, NestState.Emptied)

        stateConfiguration = Me.StateMachine.Configure(NestState.Emptied)
        stateConfiguration.Permit(NestTrigger.Initial, NestState.Initial)
        stateConfiguration.Ignore(NestTrigger.Insert)
        stateConfiguration.Ignore(NestTrigger.Remove)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Inserts this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Insert()
        Me.Fire(NestTrigger.Insert)
    End Sub

    ''' <summary> Removes the part. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Remove()
        Me.Fire(NestTrigger.Remove)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(NestTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent nest states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum NestState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the empty option. </summary>
    <Description("Empty")> Empty

    ''' <summary> An enum constant representing the occupied option. </summary>
    <Description("Occupied")> Occupied

    ''' <summary> An enum constant representing the emptied option. Nest was full and was emptied. </summary>
    <Description("Emptied")> Emptied
End Enum

''' <summary> Values that represent nest triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum NestTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the insert option. </summary>
    <Description("Insert")> Insert

    ''' <summary> An enum constant representing the remove option. </summary>
    <Description("Remove")> Remove
End Enum
