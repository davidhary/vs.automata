Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a Present/Absent process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class PresentAbsentEngine
    Inherits EngineBase(Of PresentAbsentState, PresentAbsentTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="initialState"> State of the initial. </param>
    Public Sub New(ByVal name As String, ByVal initialState As PresentAbsentState)
        MyBase.New(name, New StateMachine(Of PresentAbsentState, PresentAbsentTrigger)(initialState))

        Dim stateConfiguration As StateMachine(Of PresentAbsentState, PresentAbsentTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(PresentAbsentState.Initial)
        stateConfiguration.Permit(PresentAbsentTrigger.Remove, PresentAbsentState.Absent)
        stateConfiguration.Permit(PresentAbsentTrigger.Insert, PresentAbsentState.Present)
        stateConfiguration.Ignore(PresentAbsentTrigger.Initial)

        Me.EngagedStates.Add(PresentAbsentState.Present)
        stateConfiguration = Me.StateMachine.Configure(PresentAbsentState.Present)
        stateConfiguration.Permit(PresentAbsentTrigger.Initial, PresentAbsentState.Initial)
        stateConfiguration.Permit(PresentAbsentTrigger.Remove, PresentAbsentState.Absent)
        stateConfiguration.Ignore(PresentAbsentTrigger.Insert)

        Me.EngagedStates.Add(PresentAbsentState.Absent)
        stateConfiguration = Me.StateMachine.Configure(PresentAbsentState.Absent)
        stateConfiguration.Permit(PresentAbsentTrigger.Initial, PresentAbsentState.Initial)
        stateConfiguration.Ignore(PresentAbsentTrigger.Remove)
        stateConfiguration.Permit(PresentAbsentTrigger.Insert, PresentAbsentState.Present)

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        Me.New(name, PresentAbsentState.Initial)
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Signal a move to the Present state. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Insert()
        Me.Fire(PresentAbsentTrigger.Insert)
    End Sub

    ''' <summary> Signal a move to the Absent state. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Remove()
        Me.Fire(PresentAbsentTrigger.Remove)
    End Sub

    ''' <summary> Signal a move to the Initial state. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub Initial()
        Me.Fire(PresentAbsentTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent present absent states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum PresentAbsentState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the present option. </summary>
    <Description("Present")> Present

    ''' <summary> An enum constant representing the absent option. </summary>
    <Description("Absent")> Absent
End Enum

''' <summary> Values that represent present absent triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum PresentAbsentTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the insert option. </summary>
    <Description("Insert")> Insert

    ''' <summary> An enum constant representing the remove option. </summary>
    <Description("Remove")> Remove
End Enum
