Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a Load-Test-Unload process. </summary>
''' <remarks>
''' This engine can be used for sequencing tests of parts on a test station where parts are
''' loaded and unloaded. (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/26/2017 </para>
''' </remarks>
Public Class LoadTestUnloadEngine
    Inherits EngineBase(Of LoadTestUnloadState, LoadTestUnloadTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of LoadTestUnloadState, LoadTestUnloadTrigger)(LoadTestUnloadState.Initial))

        Dim stateConfiguration As StateMachine(Of LoadTestUnloadState, LoadTestUnloadTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        ' required in order to stop testing from the initial state.
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Initial)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.Estop)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.PermitReentry(LoadTestUnloadTrigger.Estop)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.ReadyToLoad)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToTest)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.ReadyToTest)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.ReadyToTest)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Permit(LoadTestUnloadTrigger.TestingStarting, LoadTestUnloadState.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.TestingStarting)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Permit(LoadTestUnloadTrigger.TestingActive, LoadTestUnloadState.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.TestingActive)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Permit(LoadTestUnloadTrigger.SelectNext, LoadTestUnloadState.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.SelectNext)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.TestComplete, LoadTestUnloadState.TestingCompleted)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.PermitReentry(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.TestingCompleted)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.TestingCompleted)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToUnload)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.ReadyToUnload)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.ReadyToUnload)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadBad, LoadTestUnloadState.UnloadingBad)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadGood, LoadTestUnloadState.UnloadingGood)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.UnloadingBad)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.UnloadingBad)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadGood, LoadTestUnloadState.UnloadingGood)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.UnloadingGood)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.UnloadingGood)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Permit(LoadTestUnloadTrigger.UnloadNameless, LoadTestUnloadState.UnloadingNameless)

        Me.EngagedStates.Add(LoadTestUnloadState.UnloadingNameless)
        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.UnloadingNameless)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Alert, LoadTestUnloadState.UnloadingAlert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.UnloadingAlert)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Ready, LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Load, LoadTestUnloadState.ReadyToLoad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.FinishUnloading)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.Canceling)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceled, LoadTestUnloadState.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

        stateConfiguration = Me.StateMachine.Configure(LoadTestUnloadState.Canceled)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Alert)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Canceled)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Canceling, LoadTestUnloadState.Canceling)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Estop, LoadTestUnloadState.Estop)
        stateConfiguration.Permit(LoadTestUnloadTrigger.Initial, LoadTestUnloadState.Initial)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestComplete)
        stateConfiguration.Permit(LoadTestUnloadTrigger.FinishUnloading, LoadTestUnloadState.UnloadingDone)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Load)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.Ready)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.SelectNext)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingStarting)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.TestingActive)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadBad)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadGood)
        stateConfiguration.Ignore(LoadTestUnloadTrigger.UnloadNameless)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Alerts this object. </summary>
    ''' <remarks>
    ''' UInvoke by an Part test which state entered an
    ''' <see cref="TestLocationState.RemoveException"/>.
    ''' </remarks>
    Public Sub Alert()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.Alert)
        End If
    End Sub

    ''' <summary> E stops this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Estop()
        Me.Fire(LoadTestUnloadTrigger.Estop)
    End Sub

    ''' <summary> Canceling the test. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Canceling()
        Me.Fire(LoadTestUnloadTrigger.Canceling)
    End Sub

    ''' <summary> Test canceled. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Canceled()
        Me.Fire(LoadTestUnloadTrigger.Canceled)
    End Sub

    ''' <summary> Proceed to next state. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Ready()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.Ready)
        End If
    End Sub

    ''' <summary> Loads this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Load()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.Load)
        End If
    End Sub

    ''' <summary> Testing starting. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub TestingStarting()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.TestingStarting)
        End If
    End Sub

    ''' <summary> Testing active. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub TestingActive()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.TestingActive)
        End If
    End Sub

    ''' <summary> Sends the select next trigger to select the next test location. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub SelectNext()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.SelectNext)
        End If
    End Sub

    ''' <summary> Tests complete. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub TestComplete()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.TestComplete)
        End If
    End Sub

    ''' <summary> Unload bad. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UnloadBad()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.UnloadBad)
        End If
    End Sub

    ''' <summary> Unload good. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UnloadGood()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.UnloadGood)
        End If
    End Sub

    ''' <summary> Unload nameless. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UnloadNameless()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.UnloadGood)
        End If
    End Sub

    ''' <summary> Finishes an unloading. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub FinishUnloading()
        If Me.StopRequested Then
            Me.Fire(LoadTestUnloadTrigger.Canceled)
        Else
            Me.Fire(LoadTestUnloadTrigger.FinishUnloading)
        End If
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(LoadTestUnloadTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent Load Test Unload states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum LoadTestUnloadState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the ready to load option. </summary>
    <Description("Ready to load")> ReadyToLoad

    ''' <summary> An enum constant representing the ready to test option. </summary>
    <Description("Ready to test; Door Closed")> ReadyToTest

    ''' <summary> An enum constant representing the testing starting option. </summary>
    <Description("Testing Started")> TestingStarting

    ''' <summary> An enum constant representing the testing active option. </summary>
    <Description("Testing Active")> TestingActive

    ''' <summary> An enum constant representing the select next option. </summary>
    <Description("Select Next")> SelectNext

    ''' <summary> An enum constant representing the testing completed option. </summary>
    <Description("Testing Completed")> TestingCompleted

    ''' <summary> An enum constant representing the ready to unload option. </summary>
    <Description("Ready to unload; Door open")> ReadyToUnload

    ''' <summary> An enum constant representing the unloading bad option. </summary>
    <Description("Unloading Bad")> UnloadingBad

    ''' <summary> An enum constant representing the unloading good option. </summary>
    <Description("Unloading Good")> UnloadingGood

    ''' <summary> An enum constant representing the unloading nameless option. </summary>
    <Description("Unloading Nameless")> UnloadingNameless

    ''' <summary> An enum constant representing the unloading alert option. </summary>
    <Description("Unloading Alert")> UnloadingAlert

    ''' <summary> An enum constant representing the unloading done option. </summary>
    <Description("Unloading Done")> UnloadingDone

    ''' <summary> Test canceling; removing parts. </summary>
    <Description("Testing Canceling")> Canceling

    ''' <summary> Test canceled. </summary>
    <Description("Testing Canceled")> Canceled
    <Description("Emergency Stop")> [Estop]
End Enum

''' <summary> Values that represent Load Test Unload triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum LoadTestUnloadTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the estop option. </summary>
    <Description("Emergency Stop")> Estop

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the load option. </summary>
    <Description("Load")> Load

    ''' <summary> An enum constant representing the testing starting option. </summary>
    <Description("Testing Starting")> TestingStarting

    ''' <summary> An enum constant representing the testing active option. </summary>
    <Description("Testing Active")> TestingActive

    ''' <summary> An enum constant representing the select next option. </summary>
    <Description("SelectNext")> SelectNext

    ''' <summary> An enum constant representing the test complete option. </summary>
    <Description("Test Complete")> TestComplete

    ''' <summary> An enum constant representing the unload bad option. </summary>
    <Description("Unload Bad")> UnloadBad

    ''' <summary> An enum constant representing the unload good option. </summary>
    <Description("Unload Good")> UnloadGood

    ''' <summary> An enum constant representing the unload nameless option. </summary>
    <Description("Unload Nameless")> UnloadNameless

    ''' <summary> An enum constant representing the finish unloading option. </summary>
    <Description("Done Unloading")> FinishUnloading

    ''' <summary> . </summary>
    <Description("Alert: Unloading -> Unloading Alert")> Alert

    ''' <summary> An enum constant representing the canceling option. </summary>
    <Description("Canceling testing")> Canceling

    ''' <summary> An enum constant representing the canceled option. </summary>
    <Description("Testing canceled")> Canceled
End Enum
