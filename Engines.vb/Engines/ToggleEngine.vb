Imports Stateless

''' <summary> Implements a finite automata for a simple On/Off toggle process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class ToggleEngine
    Inherits EngineBase(Of ToggleState, Byte)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="initialState"> State of the initial. </param>
    Public Sub New(ByVal name As String, ByVal initialState As ToggleState)
        MyBase.New(name, New StateMachine(Of ToggleState, Byte)(initialState))
        Me.StateMachine.Configure(ToggleState.OffState).Permit(ToggleEngine._ToggleTrigger, ToggleState.OnState)
        Me.StateMachine.Configure(ToggleState.OnState).Permit(ToggleEngine._ToggleTrigger, ToggleState.OffState)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        Me.New(name, ToggleState.OffState)
    End Sub

#End Region

#Region " TRIGGERS and STATES "

    ''' <summary> The toggle trigger. </summary>
    Private Const _ToggleTrigger As Byte = 0

#End Region

#Region " COMMANDs "

    ''' <summary> Toggles the state. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Toggle()
        Me.Fire(ToggleEngine._ToggleTrigger)
    End Sub

    ''' <summary> Turn on. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub TurnOn()
        If Me.CurrentState = ToggleState.OffState Then Me.Toggle()
    End Sub

    ''' <summary> Turn off. </summary>
    ''' <remarks> David, 2020-08-22. </remarks>
    Public Sub TurnOff()
        If Me.CurrentState = ToggleState.OnState Then Me.Toggle()
    End Sub

#End Region

End Class

''' <summary> Values that represent toggle states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum ToggleState

    ''' <summary> An enum constant representing the off state option. </summary>
    <ComponentModel.Description("Off")> OffState

    ''' <summary> An enum constant representing the on state option. </summary>
    <ComponentModel.Description("On")> OnState
End Enum



