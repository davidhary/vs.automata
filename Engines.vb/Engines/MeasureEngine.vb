Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a single measurement. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class MeasureEngine
    Inherits EngineBase(Of MeasureState, MeasureTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of MeasureState, MeasureTrigger)(MeasureState.Initial))

        Dim stateConfiguration As StateMachine(Of MeasureState, MeasureTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(MeasureState.Initial)
        stateConfiguration.Ignore(MeasureTrigger.Initial)
        stateConfiguration.Ignore(MeasureTrigger.Cancel)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Permit(MeasureTrigger.Prime, MeasureState.Priming)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Ignore(MeasureTrigger.Start)

        Me.EngagedStates.Add(MeasureState.Priming)
        stateConfiguration = Me.StateMachine.Configure(MeasureState.Priming)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Permit(MeasureTrigger.Cancel, MeasureState.Canceled)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Permit(MeasureTrigger.Ready, MeasureState.Primed)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Start)

        Me.EngagedStates.Add(MeasureState.Primed)
        stateConfiguration = Me.StateMachine.Configure(MeasureState.Primed)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Permit(MeasureTrigger.Cancel, MeasureState.Canceled)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Permit(MeasureTrigger.Start, MeasureState.Busy)

        Me.EngagedStates.Add(MeasureState.Busy)
        stateConfiguration = Me.StateMachine.Configure(MeasureState.Busy)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Permit(MeasureTrigger.Cancel, MeasureState.Canceled)
        stateConfiguration.Permit(MeasureTrigger.Complete, MeasureState.Completed)
        stateConfiguration.Permit(MeasureTrigger.Fail, MeasureState.Failed)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Ignore(MeasureTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(MeasureState.Failed)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Permit(MeasureTrigger.Cancel, MeasureState.Canceled)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Ignore(MeasureTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(MeasureState.Completed)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Ignore(MeasureTrigger.Cancel)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Ignore(MeasureTrigger.Start)

        stateConfiguration = Me.StateMachine.Configure(MeasureState.Canceled)
        stateConfiguration.Permit(MeasureTrigger.Initial, MeasureState.Initial)
        stateConfiguration.Ignore(MeasureTrigger.Cancel)
        stateConfiguration.Ignore(MeasureTrigger.Complete)
        stateConfiguration.Ignore(MeasureTrigger.Fail)
        stateConfiguration.Ignore(MeasureTrigger.Prime)
        stateConfiguration.Ignore(MeasureTrigger.Ready)
        stateConfiguration.Ignore(MeasureTrigger.Start)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Cancels this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Cancel()
        Me.Fire(MeasureTrigger.Cancel)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(MeasureTrigger.Initial)
    End Sub

    ''' <summary> Completes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Complete()
        If Me.StopRequested Then
            Me.Fire(MeasureTrigger.Cancel)
        Else
            Me.Fire(MeasureTrigger.Complete)
        End If
    End Sub

    ''' <summary> Primes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Prime()
        If Me.StopRequested Then
            Me.Fire(MeasureTrigger.Cancel)
        Else
            Me.Fire(MeasureTrigger.Prime)
        End If
    End Sub

    ''' <summary> Readies this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Ready()
        If Me.StopRequested Then
            Me.Fire(MeasureTrigger.Cancel)
        Else
            Me.Fire(MeasureTrigger.Ready)
        End If
    End Sub

    ''' <summary> Starts this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Start()
        If Me.StopRequested Then
            Me.Fire(MeasureTrigger.Cancel)
        Else
            Me.Fire(MeasureTrigger.Start)
        End If
    End Sub

    ''' <summary> Measure failed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fail()
        Me.Fire(MeasureTrigger.Fail)
    End Sub

#End Region

End Class

''' <summary> Values that represent measure states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum MeasureState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the priming option. </summary>
    <Description("Priming")> Priming

    ''' <summary> An enum constant representing the primed option. </summary>
    <Description("Primed")> Primed

    ''' <summary> An enum constant representing the busy option. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the completed option. </summary>
    <Description("Completed")> Completed

    ''' <summary> An enum constant representing the canceled option. </summary>
    <Description("Canceled")> Canceled
End Enum

''' <summary> Values that represent measure triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum MeasureTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the prime option. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the start option. </summary>
    <Description("Start")> Start

    ''' <summary> An enum constant representing the complete option. </summary>
    <Description("Complete")> Complete

    ''' <summary> An enum constant representing the fail option. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the cancel option. </summary>
    <Description("Cancel")> Cancel
End Enum
