Imports System.ComponentModel

Imports Stateless

''' <summary>
''' Implements a finite automata for a Platform capable of accessing databases and instruments
''' and selecting a lot for testing.
''' </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class PlatformEngine
    Inherits EngineBase(Of PlatformState, PlatformTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of PlatformState, PlatformTrigger)(PlatformState.Initial))

        Dim stateConfiguration As StateMachine(Of PlatformState, PlatformTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(PlatformState.Initial)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closed)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Ignore(PlatformTrigger.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        stateConfiguration = Me.StateMachine.Configure(PlatformState.Closed)
        stateConfiguration.Ignore(PlatformTrigger.Close)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Permit(PlatformTrigger.Open, PlatformState.Opening)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Closing)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Closing)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closed)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        stateConfiguration = Me.StateMachine.Configure(PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closed)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connecting)
        stateConfiguration.Permit(PlatformTrigger.Disconnect, PlatformState.Disconnecting)
        stateConfiguration.Permit(PlatformTrigger.Engage, PlatformState.Busy)
        stateConfiguration.PermitReentry(PlatformTrigger.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Permit(PlatformTrigger.Open, PlatformState.Opening)
        stateConfiguration.Permit(PlatformTrigger.Ready, PlatformState.Ready)
        stateConfiguration.Permit(PlatformTrigger.SelectLot, PlatformState.Selecting)

        Me.EngagedStates.Add(PlatformState.Opening)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Opening)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closed)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Permit(PlatformTrigger.Open, PlatformState.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Open)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Open)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closing)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connecting)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Connecting)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Connecting)
        stateConfiguration.Ignore(PlatformTrigger.Close)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connected)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Connected)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Connected)
        stateConfiguration.Ignore(PlatformTrigger.Close)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Permit(PlatformTrigger.Disconnect, PlatformState.Disconnecting)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Permit(PlatformTrigger.SelectLot, PlatformState.Selecting)

        Me.EngagedStates.Add(PlatformState.Disconnecting)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Disconnecting)
        stateConfiguration.Ignore(PlatformTrigger.Close)
        stateConfiguration.Ignore(PlatformTrigger.Connect)
        stateConfiguration.Permit(PlatformTrigger.Disconnect, PlatformState.Disconnected)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Disconnected)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Disconnected)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closing)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connecting)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Permit(PlatformTrigger.Open, PlatformState.Open)
        stateConfiguration.Ignore(PlatformTrigger.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Selecting)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Selecting)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closing)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connected)
        stateConfiguration.Permit(PlatformTrigger.Disconnect, PlatformState.Disconnected)
        stateConfiguration.Ignore(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Permit(PlatformTrigger.Ready, PlatformState.Ready)
        stateConfiguration.PermitReentry(PlatformTrigger.SelectLot)

        Me.EngagedStates.Add(PlatformState.Ready)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Ready)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connected)
        stateConfiguration.Permit(PlatformTrigger.Close, PlatformState.Closing)
        stateConfiguration.Permit(PlatformTrigger.Disconnect, PlatformState.Disconnecting)
        stateConfiguration.Permit(PlatformTrigger.Engage, PlatformState.Busy)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.PermitReentry(PlatformTrigger.Ready)
        stateConfiguration.Permit(PlatformTrigger.SelectLot, PlatformState.Selecting)

        Me.EngagedStates.Add(PlatformState.Busy)
        stateConfiguration = Me.StateMachine.Configure(PlatformState.Busy)
        stateConfiguration.Permit(PlatformTrigger.Connect, PlatformState.Connected)
        stateConfiguration.Ignore(PlatformTrigger.Close)
        stateConfiguration.Ignore(PlatformTrigger.Disconnect)
        stateConfiguration.PermitReentry(PlatformTrigger.Engage)
        stateConfiguration.Permit(PlatformTrigger.Estop, PlatformState.Estop)
        stateConfiguration.Permit(PlatformTrigger.Initial, PlatformState.Initial)
        stateConfiguration.Ignore(PlatformTrigger.Open)
        stateConfiguration.Permit(PlatformTrigger.Ready, PlatformState.Ready)
        stateConfiguration.Ignore(PlatformTrigger.SelectLot)

    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> E stops this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Estop()
        Me.Fire(PlatformTrigger.Estop)
    End Sub

    ''' <summary> Closes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Close()
        Me.Fire(PlatformTrigger.Close)
    End Sub

    ''' <summary> Opens this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Open()
        Me.Fire(PlatformTrigger.Open)
    End Sub

    ''' <summary> Connects this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Connect()
        Me.Fire(PlatformTrigger.Connect)
    End Sub

    ''' <summary> Disconnects this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Disconnect()
        Me.Fire(PlatformTrigger.Disconnect)
    End Sub

    ''' <summary> Select lot. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub SelectLot()
        Me.Fire(PlatformTrigger.SelectLot)
    End Sub

    ''' <summary> Engages this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Engage()
        Me.Fire(PlatformTrigger.Engage)
    End Sub

    ''' <summary> Readies this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Ready()
        Me.Fire(PlatformTrigger.Ready)
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(PlatformTrigger.Initial)
    End Sub

#End Region

End Class

''' <summary> Values that represent platform states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum PlatformState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the closed option. </summary>
    <Description("Closed")> Closed

    ''' <summary> An enum constant representing the closing option. </summary>
    <Description("Closing")> Closing

    ''' <summary> An enum constant representing the estop] option. </summary>
    <Description("Emergency Stop")> [Estop]

    ''' <summary> An enum constant representing the opening option. </summary>
    <Description("Opening")> Opening

    ''' <summary> An enum constant representing the open option. </summary>
    <Description("Open")> Open

    ''' <summary> An enum constant representing the connecting option. </summary>
    <Description("Connecting")> Connecting

    ''' <summary> An enum constant representing the connected option. </summary>
    <Description("Connected")> Connected

    ''' <summary> An enum constant representing the disconnecting option. </summary>
    <Description("Disconnecting")> Disconnecting

    ''' <summary> An enum constant representing the disconnected option. </summary>
    <Description("Disconnected")> Disconnected

    ''' <summary> An enum constant representing the selecting option. </summary>
    <Description("Selecting")> Selecting

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the busy option. </summary>
    <Description("Busy")> Busy
End Enum

''' <summary> Values that represent platform triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum PlatformTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the estop option. </summary>
    <Description("Emergency Stop")> Estop

    ''' <summary> An enum constant representing the close option. </summary>
    <Description("Close")> Close

    ''' <summary> An enum constant representing the open option. </summary>
    <Description("Open")> Open

    ''' <summary> An enum constant representing the connect option. </summary>
    <Description("Connect")> Connect

    ''' <summary> An enum constant representing the disconnect option. </summary>
    <Description("Disconnect")> Disconnect

    ''' <summary> An enum constant representing the select lot option. </summary>
    <Description("Select Lot")> SelectLot

    ''' <summary> An enum constant representing the engage option. </summary>
    <Description("Engage")> Engage

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready
End Enum
