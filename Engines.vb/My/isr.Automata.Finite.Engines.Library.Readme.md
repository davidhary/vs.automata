## ISR Automata Finite Engines<sub>&trade;</sub>: Finite State Machine Engines Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*6.0.6426 08/05/17*  
Adds Present/Absent engine. Entity engine: Add
finding, creating, creating, fetching and engaged states.

*6.0.6332 05/03/17*  
Adds initial state and trigger to all engines.

*6.0.6325 04/26/17*  
Created from Finite.

\(C\) 2017 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Automata Libraries](https://bitbucket.org/davidhary/vs.automata.moore)  
[Moore State Machine in C\#](http://www.codeproject.com/KB/recipes/MooreMachine.aspx)  
[Stateless](https://github.com/dotnet-state-machine/stateless.git)
