Imports isr.Core
Imports isr.Core.ExceptionExtensions
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-01. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Automata + &H1

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Finite Automata Engines Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Finite Automata Engines Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Automata.Finite.Engines"

        ''' <summary> Name of the test assembly strong. </summary>
        Public Const TestAssemblyStrongName As String = "isr.Automata.FiniteTests,PublicKey=" & My.SolutionInfo.PublicKey

    End Class

End Namespace

