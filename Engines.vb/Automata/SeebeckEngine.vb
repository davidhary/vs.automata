Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a soaking meter device. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/28/2019 </para>
''' </remarks>
Public Class SeebeckEngine
    Inherits EngineBase(Of SeebeckState, SeebeckTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of SeebeckState, SeebeckTrigger)(SeebeckState.Initial))

        Dim stateConfiguration As StateMachine(Of SeebeckState, SeebeckTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Initial)
        stateConfiguration.PermitReentry(SeebeckTrigger.Initial)
        stateConfiguration.Permit(SeebeckTrigger.Insert, SeebeckState.Inserting)
        stateConfiguration.Permit(SeebeckTrigger.Soak, SeebeckState.Soaking)
        stateConfiguration.Ignore(SeebeckTrigger.Contact)
        stateConfiguration.Ignore(SeebeckTrigger.Measure)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.Ignore(SeebeckTrigger.Remove)

        Me.EngagedStates.Add(SeebeckState.Inserting)
        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Inserting)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        stateConfiguration.PermitReentry(SeebeckTrigger.Insert)
        stateConfiguration.Permit(SeebeckTrigger.Contact, SeebeckState.Contacting)
        stateConfiguration.Permit(SeebeckTrigger.Soak, SeebeckState.Soaking)
        stateConfiguration.Ignore(SeebeckTrigger.Measure)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.Permit(SeebeckTrigger.Remove, SeebeckState.Removing)

        Me.EngagedStates.Add(SeebeckState.Soaking)
        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Soaking)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        stateConfiguration.Ignore(SeebeckTrigger.Insert)
        stateConfiguration.PermitReentry(SeebeckTrigger.Soak)
        stateConfiguration.Permit(SeebeckTrigger.Contact, SeebeckState.Contacting)
        stateConfiguration.Ignore(SeebeckTrigger.Measure)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.Permit(SeebeckTrigger.Remove, SeebeckState.Removing)

        Me.EngagedStates.Add(SeebeckState.Contacting)
        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Contacting)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        stateConfiguration.Ignore(SeebeckTrigger.Insert)
        stateConfiguration.Ignore(SeebeckTrigger.Soak)
        stateConfiguration.PermitReentry(SeebeckTrigger.Contact)
        stateConfiguration.Permit(SeebeckTrigger.Measure, SeebeckState.Measuring)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.Permit(SeebeckTrigger.Remove, SeebeckState.Removing)

        Me.EngagedStates.Add(SeebeckState.Measuring)
        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Measuring)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        stateConfiguration.Ignore(SeebeckTrigger.Insert)
        stateConfiguration.Ignore(SeebeckTrigger.Soak)
        stateConfiguration.Ignore(SeebeckTrigger.Contact)
        stateConfiguration.PermitReentry(SeebeckTrigger.Measure)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.Permit(SeebeckTrigger.Remove, SeebeckState.Removing)

        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Failed)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        stateConfiguration.Ignore(SeebeckTrigger.Insert)
        stateConfiguration.Permit(SeebeckTrigger.Soak, SeebeckState.Soaking)
        ' stateConfiguration.Ignore(SeebeckTrigger.Contact)
        ' stateConfiguration.Permit(SeebeckTrigger.Measure, SeebeckState.Contacting)
        stateConfiguration.Permit(SeebeckTrigger.Contact, SeebeckState.Contacting)
        stateConfiguration.Permit(SeebeckTrigger.Measure, SeebeckState.Measuring)
        stateConfiguration.PermitReentry(SeebeckTrigger.Fail)
        stateConfiguration.Permit(SeebeckTrigger.Remove, SeebeckState.Removing)

        stateConfiguration = Me.StateMachine.Configure(SeebeckState.Removing)
        stateConfiguration.Permit(SeebeckTrigger.Initial, SeebeckState.Initial)
        ' stateConfiguration.Ignore(SeebeckTrigger.Insert)
        stateConfiguration.Permit(SeebeckTrigger.Soak, SeebeckState.Soaking)
        stateConfiguration.Ignore(SeebeckTrigger.Contact)
        stateConfiguration.Ignore(SeebeckTrigger.Measure)
        stateConfiguration.Permit(SeebeckTrigger.Fail, SeebeckState.Failed)
        stateConfiguration.PermitReentry(SeebeckTrigger.Remove)

    End Sub

#End Region

#Region " PROGRESS "

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / SeebeckState.Removing)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> SeebeckState.Initial Then
            Me.PercentProgress = Me.StateProgress
        End If
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub ProcessStopRequest()
        If Me.StoppingTimeoutElapsed Then
            Me.Fire(SeebeckTrigger.Initial)
        Else
            Select Case Me.CurrentState
                Case SeebeckState.Contacting
                    Me.Fire(SeebeckTrigger.Initial)
                Case SeebeckState.Failed
                    Me.Fire(SeebeckTrigger.Initial)
                Case SeebeckState.Initial
                    Me.Fire(SeebeckTrigger.Initial)
                Case SeebeckState.Inserting
                    Me.Fire(SeebeckTrigger.Remove)
                Case SeebeckState.Measuring
                    Me.Fire(SeebeckTrigger.Remove)
                Case SeebeckState.Removing
                    Me.Fire(SeebeckTrigger.Initial)
                Case SeebeckState.Soaking
                    Me.Fire(SeebeckTrigger.Remove)
                Case Else
                    Throw New InvalidOperationException($"Unhanded {Me.StateMachine.State} state processing stop")
            End Select
        End If
    End Sub

    ''' <summary> Await part present. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub AwaitPartPresent()
        Me.Fire(SeebeckTrigger.Insert)
    End Sub

    ''' <summary> Part inserted; soak. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub SoakPart()
        Me.Fire(SeebeckTrigger.Soak)
    End Sub

    ''' <summary> Part removed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(SeebeckTrigger.Initial)
    End Sub

    ''' <summary> Part soaked; connect. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub ConnectPart()
        Me.Fire(SeebeckTrigger.Contact)
    End Sub

    ''' <summary> Part contacted, measure. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub MeasurePart()
        Me.Fire(SeebeckTrigger.Measure)
    End Sub

    ''' <summary> Part measured remove. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub RemovePart()
        Me.Fire(SeebeckTrigger.Remove)
    End Sub

    ''' <summary> Seebeck failed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fail()
        Me.Fire(SeebeckTrigger.Fail)
    End Sub

#End Region

End Class

''' <summary> Values that represent Seebeck states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum SeebeckState

    ''' <summary> An enum constant representing the initial state; Part not present. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the initial state; Awaiting for part present. </summary>
    <Description("Inserting")> Inserting

    ''' <summary> An enum constant representing the soaking state; part present, temperature monitoring is active. </summary>
    <Description("Soaking")> Soaking

    ''' <summary> An enum constant representing the contacting state; part at temperature establishing contact. </summary>
    <Description("Contacting")> Contacting

    ''' <summary> An enum constant representing the measuring state; Measuring Seebeck voltage. </summary>
    <Description("Measuring")> Measuring

    ''' <summary> An enum constant representing the failed state. Measurement failed. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the removing state. Removing the part. </summary>
    <Description("Removing")> Removing

End Enum

''' <summary> Values that represent Seebeck triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum SeebeckTrigger

    ''' <summary> An enum constant representing the initial trigger. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the insert trigger. Waiting for part present; go to the inserting state. </summary>
    <Description("Insert")> Insert

    ''' <summary> An enum constant representing the Soak trigger. Part inserted; start monitoring temperature. </summary>
    <Description("Part inserted; Soak")> Soak

    ''' <summary> An enum constant representing the Contact trigger command; part soaked, start establishing contact. </summary>
    <Description("Contact")> Contact

    ''' <summary> An enum constant representing the measure trigger; Part contact made, start measuring Seebeck voltage. </summary>
    <Description("Measure")> Measure

    ''' <summary> An enum constant representing the Remove trigger. Part measured or failed, remove the part. </summary>
    <Description("Remove")> Remove

    ''' <summary> An enum constant representing the fail option. Measurement failed. </summary>
    <Description("Fail")> Fail

End Enum
