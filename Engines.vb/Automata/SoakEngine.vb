Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for the soak process. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/23/16, 1.0.5956.x. </para>
''' </remarks>
Public Class SoakEngine
    Inherits EngineBase(Of SoakState, SoakTrigger)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of SoakState, SoakTrigger)(SoakState.Idle))

        ' start the sampling interval stopwatch.
        Me.SampleIntervalStopwatch = Stopwatch.StartNew
        Me.SoakStopwatch = New Stopwatch
        Me.RampStopwatch = New Stopwatch
        Me.OffStateStopwatch = New Stopwatch

        Dim stateConfiguration As StateMachine(Of SoakState, SoakTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(SoakState.Idle)
        stateConfiguration.PermitReentry(SoakTrigger.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Ramp)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))
        ' activation actions are executed after the state entry actions and whenever the state Activate function is called.
        ' .OnActivate(Sub() Me.OnActivate())

        stateConfiguration = Me.StateMachine.Configure(SoakState.Engaged)
        stateConfiguration.PermitReentry(SoakTrigger.Engage)
        stateConfiguration.Permit(SoakTrigger.HasSetpoint, SoakState.HasSetpoint)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Ramp)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.HasSetpoint)
        stateConfiguration.PermitReentry(SoakTrigger.HasSetpoint)
        stateConfiguration.Permit(SoakTrigger.Ramp, SoakState.Ramp)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.Ramp)
        stateConfiguration.PermitReentry(SoakTrigger.Ramp)
        stateConfiguration.Permit(SoakTrigger.HasSetpoint, SoakState.HasSetpoint)
        stateConfiguration.Permit(SoakTrigger.Timeout, SoakState.Timeout)
        stateConfiguration.Permit(SoakTrigger.Soak, SoakState.Soak)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.OffSoak)
        stateConfiguration.PermitReentry(SoakTrigger.OffSoak)
        stateConfiguration.Permit(SoakTrigger.Ramp, SoakState.Ramp)
        stateConfiguration.Permit(SoakTrigger.Soak, SoakState.Soak)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.Soak)
        stateConfiguration.PermitReentry(SoakTrigger.Soak)
        stateConfiguration.Permit(SoakTrigger.Timeout, SoakState.Timeout)
        stateConfiguration.Permit(SoakTrigger.OffSoak, SoakState.OffSoak)
        stateConfiguration.Permit(SoakTrigger.AtTemp, SoakState.AtTemp)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.OffTemp)
        stateConfiguration.PermitReentry(SoakTrigger.OffTemp)
        stateConfiguration.Permit(SoakTrigger.Ramp, SoakState.Ramp)
        stateConfiguration.Permit(SoakTrigger.AtTemp, SoakState.AtTemp)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.AtTemp)
        stateConfiguration.PermitReentry(SoakTrigger.AtTemp)
        stateConfiguration.Permit(SoakTrigger.OffTemp, SoakState.OffTemp)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.Ramp)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.Timeout)
        stateConfiguration.PermitReentry(SoakTrigger.Timeout)
        stateConfiguration.Permit(SoakTrigger.HasSetpoint, SoakState.HasSetpoint)
        stateConfiguration.Permit(SoakTrigger.Ramp, SoakState.Ramp)
        stateConfiguration.Permit(SoakTrigger.Soak, SoakState.Soak)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Permit(SoakTrigger.Terminate, SoakState.Terminal)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.Idle)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

        stateConfiguration = Me.StateMachine.Configure(SoakState.Terminal)
        stateConfiguration.PermitReentry(SoakTrigger.Terminate)
        stateConfiguration.Permit(SoakTrigger.Idle, SoakState.Idle)
        stateConfiguration.Permit(SoakTrigger.Engage, SoakState.Engaged)
        stateConfiguration.Ignore(SoakTrigger.AtTemp)
        stateConfiguration.Ignore(SoakTrigger.HasSetpoint)
        stateConfiguration.Ignore(SoakTrigger.OffSoak)
        stateConfiguration.Ignore(SoakTrigger.OffTemp)
        stateConfiguration.Ignore(SoakTrigger.Ramp)
        stateConfiguration.Ignore(SoakTrigger.Soak)
        stateConfiguration.Ignore(SoakTrigger.Timeout)
        stateConfiguration.OnEntry(Sub(t) Me.OnEntryAction(t))

    End Sub

#End Region

#Region " STATE TRANSITION HANDLERS "

    ''' <summary> Executes the entry action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    Private Sub OnEntryAction(ByVal transition As StateMachine(Of SoakState, SoakTrigger).Transition)
        If transition Is Nothing Then Return
        Select Case transition.Destination
            Case SoakState.Idle
            Case SoakState.Engaged
            Case SoakState.HasSetpoint
            Case SoakState.Ramp
                If Not transition.IsReentry Then
                    Me._SoakStopwatch.Reset()
                End If
            Case SoakState.Soak
                If transition.IsReentry Then
                    Me.SoakCountElapsed += 1
                Else
                    Me.SoakCountElapsed = 1
                    Me._SoakStopwatch.Restart()
                    Me._OffStateStopwatch.Reset()
                End If
            Case SoakState.OffSoak
                If transition.IsReentry Then
                    Me.OffStateCountElapsed += 1
                Else
                    Me.OffStateCountElapsed = 1
                    Me._OffStateStopwatch.Restart()
                End If
            Case SoakState.AtTemp
                If Not transition.IsReentry Then
                    Me._OffStateStopwatch.Reset()
                End If
            Case SoakState.OffTemp
                If transition.IsReentry Then
                    Me.OffStateCountElapsed += 1
                Else
                    Me.OffStateCountElapsed = 1
                    Me._OffStateStopwatch.Restart()
                End If
            Case SoakState.Timeout
                Me.Fire(SoakTrigger.Engage)
                Me.Fire(SoakTrigger.HasSetpoint)
        End Select
        Me.UpdatePercentProgress()
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Terminates this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Terminate()
        Me.Fire(SoakTrigger.Terminate)
    End Sub

    ''' <summary> Idles (disconnects) the automaton. </summary>
    ''' <remarks> Fires the idle trigger when the controller is disconnected. </remarks>
    Public Sub Disengage()
        Me.Fire(SoakTrigger.Idle)
    End Sub

    ''' <summary> Engages (connects) the automaton. </summary>
    ''' <remarks> Fires the Engage trigger when the controller is connected. </remarks>
    Public Sub Engage()
        Me.Fire(SoakTrigger.Engage)
    End Sub

    ''' <summary> Applies the setpoint described by value. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub ApplySetpoint(ByVal value As Double)
        If Me.CurrentState = SoakState.Engaged Then
            Me.Setpoint = value
            Me.Fire(SoakTrigger.HasSetpoint)
        Else
            Me.ChangeSetpoint(value)
        End If
    End Sub

    ''' <summary> Change setpoint. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="value"> The value. </param>
    Public Sub ChangeSetpoint(ByVal value As Double)
        Select Case Me.CurrentState
            Case SoakState.Idle
                Throw New InvalidOperationException($"Failed changing setpoint because the state machine is {SoakState.Idle}; the controller must be connected first.")
            Case SoakState.AtTemp
                Me.Fire(SoakTrigger.OffTemp)
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.Terminal, SoakState.Timeout
                Me.Fire(SoakTrigger.Engage)
                Me.Fire(SoakTrigger.HasSetpoint)
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.Engaged
                Me.Fire(SoakTrigger.HasSetpoint)
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.OffSoak, SoakState.OffTemp
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.HasSetpoint
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.Ramp
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.Soak
                Me.Fire(SoakTrigger.OffSoak)
                Me.Fire(SoakTrigger.Ramp)
            Case Else
                Throw New InvalidOperationException($"Failed changing setpoint due to an unhandled state {Me.CurrentState}")
        End Select
        Me.Setpoint = value
    End Sub

    ''' <summary>
    ''' Applies the temperature described by value until the state machine settles at a new state.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="value"> The value. </param>
    Public Sub ApplyTemperature(ByVal value As Double)
        Dim trigger As SoakTrigger = Me.QueryNextTrigger(value)
        If trigger = SoakTrigger.Idle Then
            Throw New InvalidOperationException($"Failed changing temperature because the state machine is {SoakState.Idle}; the controller must be connected first.")
        Else
            Me.ApplyTemperature(trigger)
        End If
    End Sub

    ''' <summary> Queries next trigger. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="temperature"> The temperature. </param>
    ''' <returns> The next trigger. </returns>
    Private Function QueryNextTrigger(ByVal temperature As Double) As SoakTrigger
        Dim trigger As SoakTrigger = SoakTrigger.Engage
        Select Case Me.CurrentState
            Case SoakState.Idle
                trigger = SoakTrigger.Idle
            Case SoakState.Terminal, SoakState.Timeout
                Me.Fire(SoakTrigger.Engage)
                Me.Fire(SoakTrigger.HasSetpoint)
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.Engaged
                Me.Fire(SoakTrigger.HasSetpoint)
                Me.Fire(SoakTrigger.Ramp)
            Case SoakState.HasSetpoint
                Me.Fire(SoakTrigger.Ramp)
        End Select
        Me.Temperature = temperature
        Select Case Me.CurrentState
            Case SoakState.Ramp
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.Soak
                ElseIf Me.IsRampTimeout Then
                    ' Move to ramp timeout state and display a pop-up dialog.
                    trigger = SoakTrigger.Timeout
                Else
                    trigger = SoakTrigger.Ramp
                End If
            Case SoakState.Soak
                If Me.IsSoakElapsed Then
                    trigger = SoakTrigger.AtTemp
                ElseIf Me.IsOffTemp() Then
                    trigger = SoakTrigger.OffSoak
                Else
                    trigger = SoakTrigger.Soak
                End If
            Case SoakState.OffSoak
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.Soak
                ElseIf Me.IsOffStateDurationElapsed Then
                    trigger = SoakTrigger.Ramp
                Else
                    trigger = SoakTrigger.OffSoak
                End If
            Case SoakState.AtTemp
                trigger = If(Me.IsOffTemp(), SoakTrigger.OffTemp, SoakTrigger.AtTemp)
            Case SoakState.OffTemp
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.AtTemp
                ElseIf Me.IsOffStateDurationElapsed Then
                    trigger = SoakTrigger.Ramp
                Else
                    trigger = SoakTrigger.OffTemp
                End If
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled case")
        End Select
        Return trigger
    End Function

    ''' <summary>
    ''' Applies the temperature described by value until the state machine settles at a new state.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="trigger"> The trigger. </param>
    Private Sub ApplyTemperature(ByVal trigger As SoakTrigger)
        Me.Fire(trigger)
        Select Case Me.CurrentState
            Case SoakState.Ramp
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.Soak
                    Me.ApplyTemperature(trigger)
                ElseIf Me.IsRampTimeout Then
                    ' Move to ramp timeout state and display a pop-up dialog.
                    trigger = SoakTrigger.Timeout
                    Me.Fire(trigger)
                End If
            Case SoakState.Soak
                If Me.IsSoakElapsed Then
                    trigger = SoakTrigger.AtTemp
                    Me.ApplyTemperature(trigger)
                ElseIf Me.IsOffTemp() Then
                    trigger = SoakTrigger.OffSoak
                    Me.Fire(trigger)
                End If
            Case SoakState.OffSoak
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.Soak
                    Me.ApplyTemperature(trigger)
                ElseIf Me.IsOffStateDurationElapsed Then
                    trigger = SoakTrigger.Ramp
                    Me.ApplyTemperature(trigger)
                End If
            Case SoakState.AtTemp
                If Me.IsOffTemp() Then
                    trigger = SoakTrigger.OffTemp
                    Me.ApplyTemperature(trigger)
                End If
            Case SoakState.OffTemp
                If Me.IsAtTemp() Then
                    trigger = SoakTrigger.AtTemp
                    Me.ApplyTemperature(trigger)
                ElseIf Me.IsOffStateDurationElapsed Then
                    trigger = SoakTrigger.Ramp
                    Me.ApplyTemperature(trigger)
                End If
            Case Else
        End Select
    End Sub

#End Region

#Region " STATE AUTOMATION CONDITIONS "

    ''' <summary> The temperature. </summary>
    Private _Temperature As Double

    ''' <summary> Gets or sets the temperature. </summary>
    ''' <value> The temperature. </value>
    Public Property Temperature As Double
        Get
            Return Me._Temperature
        End Get
        Set(value As Double)
            If value <> Me.Temperature Then
                Me._Temperature = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The temperature resolution. </summary>
    Private _TemperatureResolution As Double

    ''' <summary> Gets or sets the temperature resolution. </summary>
    ''' <value> The temperature resolution. </value>
    Public Property TemperatureResolution As Double
        Get
            Return Me._TemperatureResolution
        End Get
        Set(value As Double)
            If value <> Me.TemperatureResolution Then
                Me._TemperatureResolution = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The setpoint. </summary>
    Private _Setpoint As Double

    ''' <summary> Gets or sets the setpoint. </summary>
    ''' <value> The setpoint. </value>
    Public Property Setpoint As Double
        Get
            Return Me._Setpoint
        End Get
        Set(value As Double)
            If value <> Me.Setpoint Then
                Me._Setpoint = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The window. </summary>
    Private _Window As Double

    ''' <summary> Gets or sets the window. </summary>
    ''' <value> The window. </value>
    Public Property Window As Double
        Get
            Return Me._Window
        End Get
        Set(value As Double)
            If value <> Me.Window Then
                Me._Window = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The ramp timeout. </summary>
    Private _RampTimeout As TimeSpan

    ''' <summary> Gets or sets the ramp timeout. </summary>
    ''' <value> The ramp timeout. </value>
    Public Property RampTimeout As TimeSpan
        Get
            Return Me._RampTimeout
        End Get
        Set(value As TimeSpan)
            If value <> Me.RampTimeout Then
                Me._RampTimeout = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The hysteresis. </summary>
    Private _Hysteresis As Double

    ''' <summary> Gets or sets the Hysteresis. </summary>
    ''' <value> The Hysteresis. </value>
    Public Property Hysteresis As Double
        Get
            Return Me._Hysteresis
        End Get
        Set(value As Double)
            If value <> Me.Hysteresis Then
                Me._Hysteresis = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The soak count elapsed. </summary>
    Private _SoakCountElapsed As Integer

    ''' <summary> Gets or sets the number of counts elapsed during the soak. </summary>
    ''' <value> The Soak Count Elapsed. </value>
    Public Property SoakCountElapsed As Integer
        Get
            Return Me._SoakCountElapsed
        End Get
        Set(value As Integer)
            If value <> Me.SoakCountElapsed Then
                Me._SoakCountElapsed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Number of soaks. </summary>
    Private _SoakCount As Integer

    ''' <summary> Gets or sets the number of times the soak was queried. </summary>
    ''' <remarks>
    ''' Soak lasts either for a <see cref="SoakDuration"/> specified time or <see cref="SoakCount"/>
    ''' specified number of readings.
    ''' </remarks>
    ''' <value> The Soak Duration Count. </value>
    Public Property SoakCount As Integer
        Get
            Return Me._SoakCount
        End Get
        Set(value As Integer)
            If value <> Me.SoakCount Then
                Me._SoakCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Duration of the soak. </summary>
    Private _SoakDuration As TimeSpan

    ''' <summary> Gets or sets the duration of the soak. </summary>
    ''' <value> The soak duration. </value>
    Public Property SoakDuration As TimeSpan
        Get
            Return Me._SoakDuration
        End Get
        Set(value As TimeSpan)
            If value <> Me.SoakDuration Then
                Me._SoakDuration = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The off state count elapsed. </summary>
    Private _OffStateCountElapsed As Integer

    ''' <summary>
    ''' Gets or sets the number of sample times elapsed during either the
    ''' <see cref="SoakState.OffSoak"/> or
    ''' <see cref="SoakState.OffTemp"/> states .
    ''' </summary>
    ''' <value> The OffState Duration Count Elapsed. </value>
    Public Property OffStateCountElapsed As Integer
        Get
            Return Me._OffStateCountElapsed
        End Get
        Set(value As Integer)
            If value <> Me.OffStateCountElapsed Then
                Me._OffStateCountElapsed = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Number of allowed off states. </summary>
    Private _AllowedOffStateCount As Integer

    ''' <summary>
    ''' Gets or sets the number of sample times allowed in either the <see cref="SoakState.OffSoak"/>
    ''' or
    ''' <see cref="SoakState.OffTemp"/> states before triggering back to the
    ''' <see cref="SoakState.Ramp"/> state.
    ''' </summary>
    ''' <value> The allowed off-state Count. </value>
    Public Property AllowedOffStateCount As Integer
        Get
            Return Me._AllowedOffStateCount
        End Get
        Set(value As Integer)
            If value <> Me.AllowedOffStateCount Then
                Me._AllowedOffStateCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Duration of the allowed off state. </summary>
    Private _AllowedOffStateDuration As TimeSpan

    ''' <summary>
    ''' Gets or sets the time allowed in either the <see cref="SoakState.OffSoak"/> or
    ''' <see cref="SoakState.OffTemp"/> states before triggering back to the
    ''' <see cref="SoakState.Ramp"/> state.
    ''' </summary>
    ''' <value> The allowed off-state duration. </value>
    Public Property AllowedOffStateDuration As TimeSpan
        Get
            Return Me._AllowedOffStateDuration
        End Get
        Set(value As TimeSpan)
            If value <> Me.AllowedOffStateDuration Then
                Me._AllowedOffStateDuration = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The sample interval. </summary>
    Private _SampleInterval As TimeSpan

    ''' <summary> Gets or sets the sample interval. </summary>
    ''' <value> The sample interval. </value>
    Public Property SampleInterval As TimeSpan
        Get
            Return Me._SampleInterval
        End Get
        Set(value As TimeSpan)
            If value <> Me.SampleInterval Then
                Me._SampleInterval = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Query if  <paramref name="value"/> is within <see cref="TemperatureResolution"/> of the
    ''' <see cref="Setpoint"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if at setpoint; otherwise <c>false</c> </returns>
    Public Function IsAtSetpoint(ByVal value As Double) As Boolean
        Return Math.Abs(value - Me.Setpoint) <= Me.TemperatureResolution
    End Function

    ''' <summary> Query if the soak automation is at temp and staying at temp. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns>
    ''' <c>true</c> if the soak automation is at temp and staying at temp; otherwise <c>false</c>
    ''' </returns>
    Public Function IsStayingAtTemp() As Boolean
        Dim nextSymbol As SoakTrigger = Me.QueryNextTrigger(Me.Temperature)
        Return Me.LastTransition.Destination = SoakState.AtTemp AndAlso
               (SoakTrigger.AtTemp = nextSymbol OrElse Me.LastTransition.Trigger = nextSymbol)
    End Function

#End Region

#Region " STATE AUTOMATION PROGRESS "

    ''' <summary>
    ''' Query if  <see cref="Temperature"/> is within <see cref="Window"/> of the
    ''' <see cref="Setpoint"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if at temperature; otherwise <c>false</c> </returns>
    Public Function IsAtTemp() As Boolean
        Return Math.Abs(Me.Temperature - Me.Setpoint) <= 0.5 * Me.Window
    End Function

    ''' <summary>
    ''' Query if  <see cref="Temperature"/> is outside <see cref="Window"/>+<see cref="Hysteresis"/>
    ''' of the <see cref="Setpoint"/>.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if off temporary; otherwise <c>false</c> </returns>
    Public Function IsOffTemp() As Boolean
        Return Math.Abs(Me.Temperature - Me.Setpoint) > (0.5 * Me.Window + Me.Hysteresis)
    End Function

    ''' <summary> Gets the soak progress. </summary>
    ''' <value> The soak progress. </value>
    Public ReadOnly Property SoakProgress As Integer
        Get
            If Me.SoakDuration > TimeSpan.Zero AndAlso
                (Me.CurrentState = SoakState.Soak OrElse Me.CurrentState = SoakState.OffSoak) Then
                Dim value As Integer = CInt(100 * Me.SoakStopwatch.Elapsed.TotalSeconds / Me.SoakDuration.TotalSeconds)
                Return If(value < 0, 0, If(value > 100, 100, value))
            Else
                Return Me.StateProgress
            End If
        End Get
    End Property

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / SoakState.Terminal)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> SoakState.Idle Then
            Me.PercentProgress = Me.SoakProgress
        End If
    End Sub

    ''' <summary> Gets or sets the ramp stopwatch. </summary>
    ''' <value> The ramp stopwatch. </value>
    Public ReadOnly Property RampStopwatch As Stopwatch

    ''' <summary> Query if this object is ramp timeout. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if ramp timeout; otherwise <c>false</c> </returns>
    Public Function IsRampTimeout() As Boolean
        Return Me.RampTimeout > TimeSpan.Zero AndAlso Me.RampStopwatch.Elapsed > Me.RampTimeout
    End Function

    ''' <summary> Gets or sets the soak stopwatch. </summary>
    ''' <value> The soak stopwatch. </value>
    Public ReadOnly Property SoakStopwatch As Stopwatch

    ''' <summary> Query if the soak time or count elapsed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if soak elapsed; otherwise <c>false</c> </returns>
    Public Function IsSoakElapsed() As Boolean
        Return (Me.SoakCount = 0 AndAlso Me.SoakDuration = TimeSpan.Zero) OrElse
               (Me.SoakDuration > TimeSpan.Zero AndAlso Me.SoakStopwatch.Elapsed > Me.SoakDuration) OrElse
               (Me.SoakCount > 0 AndAlso Me.SoakCountElapsed >= Me.SoakCount)
    End Function

    ''' <summary> Gets or sets the sample interval stopwatch. </summary>
    ''' <remarks> This stopwatch runs continuously. </remarks>
    ''' <value> The sample interval stopwatch. </value>
    Public ReadOnly Property SampleIntervalStopwatch As Stopwatch

    ''' <summary> Executes the sample handled action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub OnSampleHandled()
        Me.SampleIntervalStopwatch.Restart()
    End Sub

    ''' <summary> Query if the sample interval elapsed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <returns> <c>true</c> if sample interval elapsed; otherwise <c>false</c> </returns>
    Public Function IsSampleIntervalElapsed() As Boolean
        Return Me.IsSampleIntervalElapsed(Me.SampleInterval)
    End Function

    ''' <summary> Query if the sample interval elapsed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="interval"> The interval. </param>
    ''' <returns> <c>true</c> if sample interval elapsed; otherwise <c>false</c> </returns>
    Public Function IsSampleIntervalElapsed(ByVal interval As TimeSpan) As Boolean
        Return Me.SampleIntervalStopwatch.Elapsed < interval
    End Function

    ''' <summary> Gets or sets the 'off' state duration stopwatch. </summary>
    ''' <value> The off' state duration stopwatch. </value>
    Public ReadOnly Property OffStateStopwatch As Stopwatch

    ''' <summary> Query if the allowed 'off' state duration elapsed. </summary>
    ''' <remarks>
    ''' The automata will resume from an off-soak to soak or off-temp to temp if the 'off' state
    ''' lasts less than the allowed <see cref="AllowedOffStateDuration">duration</see> or
    ''' <see cref="AllowedOffStateCount">count</see> .
    ''' </remarks>
    ''' <returns> <c>true</c> if soak reset delay elapsed; otherwise <c>false</c> </returns>
    Public Function IsOffStateDurationElapsed() As Boolean
        Return (Me.AllowedOffStateCount = 0 AndAlso Me.AllowedOffStateDuration = TimeSpan.Zero) OrElse
               (Me.AllowedOffStateDuration > TimeSpan.Zero AndAlso Me.OffStateStopwatch.Elapsed > Me.AllowedOffStateDuration) OrElse
               (Me.AllowedOffStateCount > 0 AndAlso Me.OffStateCountElapsed >= Me.AllowedOffStateCount)
    End Function

#End Region

End Class

''' <summary>
''' Enumerates the state transition triggers for the
''' <see cref="SoakEngine">soak</see> automaton.
''' </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum SoakTrigger

    ''' <summary> An enum constant representing the idle option. </summary>
    <Description("Idle (disconnect): Idle; ")> Idle

    ''' <summary> An enum constant representing the engage option. </summary>
    <Description("Engage (connect): Engaged; ")> Engage

    ''' <summary> . </summary>
    <Description("Ready: Initial -> Ready")> HasSetpoint

    ''' <summary> . </summary>
    <Description("Ramp: Ready -> Ramp")> Ramp

    ''' <summary> . </summary>
    <Description("Soak: Ramp, OffSoak -> Soak")> Soak

    ''' <summary> . </summary>
    <Description("OffSoak: Soak -> OffSoak")> OffSoak

    ''' <summary> . </summary>
    <Description("AtTemp: Soak; OffTemp -> AtTemp")> AtTemp

    ''' <summary> . </summary>
    <Description("OffTemp: AtTemp -> OffTemp")> OffTemp

    ''' <summary> . </summary>
    <Description("Timeout: Ramp; Soak -> Timeout")> Timeout

    ''' <summary> . </summary>
    <Description("Terminate (stop): Any state -> Terminal")> Terminate
End Enum

''' <summary> Values that represent Soak automaton states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum SoakState

    ''' <summary> An enum constant representing the idle option. </summary>
    <Description("Idle")> Idle

    ''' <summary> An enum constant representing the engaged option. </summary>
    <Description("Engaged")> Engaged

    ''' <summary> An enum constant representing the has setpoint option. </summary>
    <Description("Setpoint")> HasSetpoint

    ''' <summary> An enum constant representing the ramp option. </summary>
    <Description("Ramp")> Ramp

    ''' <summary> An enum constant representing the off soak option. </summary>
    <Description("Off-Soak")> OffSoak

    ''' <summary> An enum constant representing the soak option. </summary>
    <Description("Soak")> Soak

    ''' <summary> An enum constant representing the off Temporary option. </summary>
    <Description("Off-Temp")> OffTemp

    ''' <summary> An enum constant representing at Temporary option. </summary>
    <Description("At-Temp")> AtTemp

    ''' <summary> An enum constant representing the timeout option. </summary>
    <Description("Timeout")> Timeout

    ''' <summary> An enum constant representing the terminal option. </summary>
    <Description("Terminal")> Terminal
End Enum

