Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a Process. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-08-20 </para>
''' </remarks>
Public Class ProcessEngine
    Inherits EngineBase(Of ProcessState, ProcessTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of ProcessState, ProcessTrigger)(ProcessState.Initial))

        Dim stateConfiguration As StateMachine(Of ProcessState, ProcessTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(ProcessState.Initial)
        stateConfiguration.PermitReentry(ProcessTrigger.Initial)
        stateConfiguration.Permit(ProcessTrigger.Prime, ProcessState.Prime)
        stateConfiguration.Permit(ProcessTrigger.Ready, ProcessState.Ready)
        stateConfiguration.Ignore(ProcessTrigger.Busy)
        stateConfiguration.Permit(ProcessTrigger.Fail, ProcessState.Failed)
        stateConfiguration.Ignore(ProcessTrigger.Complete)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Prime)

        Me.EngagedStates.Add(ProcessState.Prime)
        stateConfiguration = Me.StateMachine.Configure(ProcessState.Prime)
        stateConfiguration.Permit(ProcessTrigger.Initial, ProcessState.Initial)
        stateConfiguration.PermitReentry(ProcessTrigger.Prime)
        stateConfiguration.Permit(ProcessTrigger.Ready, ProcessState.Ready)
        stateConfiguration.Ignore(ProcessTrigger.Busy)
        stateConfiguration.Permit(ProcessTrigger.Fail, ProcessState.Failed)
        stateConfiguration.Permit(ProcessTrigger.Complete, ProcessState.Completed)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Ready)

        Me.EngagedStates.Add(ProcessState.Ready)
        stateConfiguration = Me.StateMachine.Configure(ProcessState.Ready)
        stateConfiguration.Permit(ProcessTrigger.Initial, ProcessState.Initial)
        stateConfiguration.Ignore(ProcessTrigger.Prime)
        stateConfiguration.PermitReentry(ProcessTrigger.Ready)
        stateConfiguration.Permit(ProcessTrigger.Busy, ProcessState.Busy)
        stateConfiguration.Permit(ProcessTrigger.Fail, ProcessState.Failed)
        stateConfiguration.Permit(ProcessTrigger.Complete, ProcessState.Completed)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Busy)

        Me.EngagedStates.Add(ProcessState.Busy)
        stateConfiguration = Me.StateMachine.Configure(ProcessState.Busy)
        stateConfiguration.Permit(ProcessTrigger.Initial, ProcessState.Initial)
        stateConfiguration.Ignore(ProcessTrigger.Prime)
        stateConfiguration.Permit(ProcessTrigger.Ready, ProcessState.Ready)
        stateConfiguration.PermitReentry(ProcessTrigger.Busy)
        stateConfiguration.Permit(ProcessTrigger.Fail, ProcessState.Failed)
        stateConfiguration.Permit(ProcessTrigger.Complete, ProcessState.Completed)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Completed)

        stateConfiguration = Me.StateMachine.Configure(ProcessState.Failed)
        stateConfiguration.Permit(ProcessTrigger.Initial, ProcessState.Initial)
        stateConfiguration.Ignore(ProcessTrigger.Prime)
        stateConfiguration.Permit(ProcessTrigger.Ready, ProcessState.Ready)
        stateConfiguration.Permit(ProcessTrigger.Busy, ProcessState.Busy)
        stateConfiguration.PermitReentry(ProcessTrigger.Fail)
        stateConfiguration.Permit(ProcessTrigger.Complete, ProcessState.Completed)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Initial)

        stateConfiguration = Me.StateMachine.Configure(ProcessState.Completed)
        stateConfiguration.Permit(ProcessTrigger.Initial, ProcessState.Initial)
        stateConfiguration.Permit(ProcessTrigger.Prime, ProcessState.Prime)
        stateConfiguration.Permit(ProcessTrigger.Ready, ProcessState.Ready)
        stateConfiguration.Permit(ProcessTrigger.Busy, ProcessState.Busy)
        stateConfiguration.Permit(ProcessTrigger.Fail, ProcessState.Failed)
        stateConfiguration.PermitReentry(ProcessTrigger.Complete)
        stateConfiguration.Permit(ProcessTrigger.Proceed, ProcessState.Initial)

    End Sub

#End Region

#Region " PROGRESS "

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / ProcessState.Completed)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> ProcessState.Initial Then
            Me.PercentProgress = Me.StateProgress
        End If
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub ProcessStopRequest()
        If Me.StoppingTimeoutElapsed Then
            Me.Fire(ProcessTrigger.Initial)
        Else
            Select Case Me.CurrentState
                Case ProcessState.Failed
                    Me.Fire(ProcessTrigger.Initial)
                Case ProcessState.Initial
                    Me.Fire(ProcessTrigger.Initial)
                Case ProcessState.Prime
                    Me.Fire(ProcessTrigger.Complete)
                Case ProcessState.Busy
                    Me.Fire(ProcessTrigger.Complete)
                Case ProcessState.Completed
                    Me.Fire(ProcessTrigger.Initial)
                Case ProcessState.Ready
                    Me.Fire(ProcessTrigger.Complete)
                Case Else
                    Throw New InvalidOperationException($"Unhanded {Me.StateMachine.State} state processing stop")
            End Select
        End If
    End Sub

#End Region

End Class

''' <summary> Values that represent Process states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum ProcessState

    ''' <summary> An enum constant representing the initial state; Part not present. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the Prime state; Prepare the process. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the Ready state; Process is Ready to start. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the Busy state; Process is active. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the failed state. Process failed. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the Completed state. Process completed. </summary>
    <Description("Completed")> Completed

End Enum

''' <summary> Values that represent Process triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum ProcessTrigger

    ''' <summary> An enum constant representing the initial trigger. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the Prime trigger to prepare the process; go to the Prime state. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the Ready trigger to go to the ready state. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the Busy trigger to activate the Process. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the Complete trigger to complete the process. </summary>
    <Description("Complete")> Complete

    ''' <summary> An enum constant representing the fail option. process failed. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
    <Description("Proceed")> Proceed

End Enum
