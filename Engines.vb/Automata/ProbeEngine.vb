Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a probe device. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/28/2019 </para>
''' </remarks>
Public Class ProbeEngine
    Inherits EngineBase(Of ProbeState, ProbeTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of ProbeState, ProbeTrigger)(ProbeState.Initial))

        Dim stateConfiguration As StateMachine(Of ProbeState, ProbeTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(ProbeState.Initial)
        stateConfiguration.Permit(ProbeTrigger.Fail, ProbeState.Failed)
        stateConfiguration.PermitReentry(ProbeTrigger.Initial)
        stateConfiguration.Ignore(ProbeTrigger.Lift)
        stateConfiguration.Permit(ProbeTrigger.Lower, ProbeState.Lowering)
        stateConfiguration.Ignore(ProbeTrigger.Probe)

        Me.EngagedStates.Add(ProbeState.Lowering)
        stateConfiguration = Me.StateMachine.Configure(ProbeState.Lowering)
        stateConfiguration.Permit(ProbeTrigger.Fail, ProbeState.Failed)
        stateConfiguration.Permit(ProbeTrigger.Initial, ProbeState.Initial)
        stateConfiguration.Ignore(ProbeTrigger.Lift)
        stateConfiguration.PermitReentry(ProbeTrigger.Lower)
        stateConfiguration.Permit(ProbeTrigger.Probe, ProbeState.Probing)

        Me.EngagedStates.Add(ProbeState.Probing)
        stateConfiguration = Me.StateMachine.Configure(ProbeState.Probing)
        stateConfiguration.Permit(ProbeTrigger.Fail, ProbeState.Failed)
        stateConfiguration.Permit(ProbeTrigger.Initial, ProbeState.Initial)
        stateConfiguration.Permit(ProbeTrigger.Lift, ProbeState.Lifting)
        stateConfiguration.Ignore(ProbeTrigger.Lower)
        stateConfiguration.PermitReentry(ProbeTrigger.Probe)

        stateConfiguration = Me.StateMachine.Configure(ProbeState.Failed)
        stateConfiguration.Permit(ProbeTrigger.Initial, ProbeState.Initial)
        stateConfiguration.Ignore(ProbeTrigger.Lift)
        stateConfiguration.PermitReentry(ProbeTrigger.Fail)
        stateConfiguration.Permit(ProbeTrigger.Lower, ProbeState.Lowering)
        stateConfiguration.Ignore(ProbeTrigger.Probe)

        stateConfiguration = Me.StateMachine.Configure(ProbeState.Lifting)
        stateConfiguration.Permit(ProbeTrigger.Fail, ProbeState.Failed)
        stateConfiguration.Permit(ProbeTrigger.Initial, ProbeState.Initial)
        stateConfiguration.PermitReentry(ProbeTrigger.Lift)
        stateConfiguration.Permit(ProbeTrigger.Lower, ProbeState.Lowering)
        stateConfiguration.Ignore(ProbeTrigger.Probe)

    End Sub

#End Region

#Region " PROGRESS "

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / ProbeState.Lifting)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> SeebeckState.Initial Then
            Me.PercentProgress = Me.StateProgress
        End If
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub ProcessStopRequest()
        If Me.StoppingTimeoutElapsed Then
            Me.Fire(ProbeTrigger.Initial)
        Else
            Select Case Me.CurrentState
                Case ProbeState.Failed
                    Me.Fire(ProbeTrigger.Initial)
                Case ProbeState.Initial
                    Me.Fire(ProbeTrigger.Initial)
                Case ProbeState.Lifting
                    Me.Fire(ProbeTrigger.Initial)
                Case ProbeState.Lowering
                    Me.Fire(ProbeTrigger.Initial)
                Case ProbeState.Probing
                    Me.Fire(ProbeTrigger.Lift)
                Case Else
                    Throw New InvalidOperationException($"Unhanded {Me.StateMachine.State} state processing stop")
            End Select
        End If
    End Sub

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(ProbeTrigger.Initial)
    End Sub

    ''' <summary> Starts probe lifting. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Lift()
        Me.Fire(ProbeTrigger.Lift)
    End Sub

    ''' <summary> Starts probe lowering. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Lower()
        Me.Fire(ProbeTrigger.Lower)
    End Sub

    ''' <summary> Starts making measurements. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Probe()
        Me.Fire(ProbeTrigger.Probe)
    End Sub

    ''' <summary> Probe failed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fail()
        Me.Fire(ProbeTrigger.Fail)
    End Sub

#End Region

End Class

''' <summary> Values that represent probe states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum ProbeState

    ''' <summary> An enum constant representing the initial state. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the lowering state. </summary>
    <Description("Lowering")> Lowering

    ''' <summary> An enum constant representing the probing state. </summary>
    <Description("Probing")> Probing

    ''' <summary> An enum constant representing the failed state. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the lifting state. </summary>
    <Description("Lifting")> Lifting
End Enum

''' <summary> Values that represent probe triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum ProbeTrigger

    ''' <summary> An enum constant representing the initial trigger. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the lower probe trigger. </summary>
    <Description("Lower")> Lower

    ''' <summary> An enum constant representing the probe measure trigger. </summary>
    <Description("Prob")> Probe

    ''' <summary> An enum constant representing the fail probe trigger. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the lift probe trigger. </summary>
    <Description("Lift")> Lift
End Enum
