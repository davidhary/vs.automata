Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a stream reading device. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-08-20 </para>
''' </remarks>
Public Class StreamReadingEngine
    Inherits EngineBase(Of StreamReadingState, StreamReadingTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of StreamReadingState, StreamReadingTrigger)(StreamReadingState.Initial))

        Dim stateConfiguration As StateMachine(Of StreamReadingState, StreamReadingTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Initial)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Initial)
        stateConfiguration.Permit(StreamReadingTrigger.Prime, StreamReadingState.Prime)
        stateConfiguration.Ignore(StreamReadingTrigger.Starting)
        stateConfiguration.Permit(StreamReadingTrigger.Ready, StreamReadingState.Ready)
        stateConfiguration.Ignore(StreamReadingTrigger.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.Ignore(StreamReadingTrigger.Complete)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Prime)

        Me.EngagedStates.Add(StreamReadingState.Prime)
        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Prime)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Prime)
        stateConfiguration.Permit(StreamReadingTrigger.Ready, StreamReadingState.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Starting, StreamReadingState.Starting)
        stateConfiguration.Ignore(StreamReadingTrigger.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.Permit(StreamReadingTrigger.Complete, StreamReadingState.Completed)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Ready)

        Me.EngagedStates.Add(StreamReadingState.Ready)
        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.Ignore(StreamReadingTrigger.Prime)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Starting, StreamReadingState.Starting)
        stateConfiguration.Ignore(StreamReadingTrigger.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.Permit(StreamReadingTrigger.Complete, StreamReadingState.Completed)

        Me.EngagedStates.Add(StreamReadingState.Starting)
        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Starting)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.Ignore(StreamReadingTrigger.Prime)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Starting)
        stateConfiguration.Ignore(StreamReadingTrigger.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Busy, StreamReadingState.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.Permit(StreamReadingTrigger.Complete, StreamReadingState.Completed)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Busy)

        Me.EngagedStates.Add(StreamReadingState.Busy)
        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.Ignore(StreamReadingTrigger.Prime)
        stateConfiguration.Ignore(StreamReadingTrigger.Starting)
        stateConfiguration.Ignore(StreamReadingTrigger.Ready)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Busy)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.Permit(StreamReadingTrigger.Complete, StreamReadingState.Completed)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Completed)

        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Failed)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.Ignore(StreamReadingTrigger.Prime)
        stateConfiguration.Ignore(StreamReadingTrigger.Starting)
        stateConfiguration.Permit(StreamReadingTrigger.Ready, StreamReadingState.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Busy, StreamReadingState.Starting)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Fail)
        stateConfiguration.Permit(StreamReadingTrigger.Complete, StreamReadingState.Completed)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Initial)

        stateConfiguration = Me.StateMachine.Configure(StreamReadingState.Completed)
        stateConfiguration.Permit(StreamReadingTrigger.Initial, StreamReadingState.Initial)
        stateConfiguration.Permit(StreamReadingTrigger.Prime, StreamReadingState.Prime)
        stateConfiguration.Permit(StreamReadingTrigger.Ready, StreamReadingState.Ready)
        stateConfiguration.Permit(StreamReadingTrigger.Starting, StreamReadingState.Starting)
        stateConfiguration.Permit(StreamReadingTrigger.Busy, StreamReadingState.Starting)
        stateConfiguration.Permit(StreamReadingTrigger.Fail, StreamReadingState.Failed)
        stateConfiguration.PermitReentry(StreamReadingTrigger.Complete)
        stateConfiguration.Permit(StreamReadingTrigger.Proceed, StreamReadingState.Initial)

    End Sub

#End Region

#Region " PROGRESS "

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / StreamReadingState.Completed)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> StreamReadingState.Initial Then
            Me.PercentProgress = Me.StateProgress
        End If
    End Sub

#End Region

#Region " COMMANDS "

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub ProcessStopRequest()
        If Me.StoppingTimeoutElapsed Then
            Me.Fire(StreamReadingTrigger.Initial)
        Else
            Select Case Me.CurrentState
                Case StreamReadingState.Starting
                    Me.Fire(StreamReadingTrigger.Initial)
                Case StreamReadingState.Failed
                    Me.Fire(StreamReadingTrigger.Initial)
                Case StreamReadingState.Initial
                    Me.Fire(StreamReadingTrigger.Initial)
                Case StreamReadingState.Prime
                    Me.Fire(StreamReadingTrigger.Complete)
                Case StreamReadingState.Busy
                    Me.Fire(StreamReadingTrigger.Complete)
                Case StreamReadingState.Completed
                    Me.Fire(StreamReadingTrigger.Initial)
                Case StreamReadingState.Ready
                    Me.Fire(StreamReadingTrigger.Complete)
                Case Else
                    Throw New InvalidOperationException($"Unhanded {Me.StateMachine.State} state processing stop")
            End Select
        End If
    End Sub

#End Region

End Class

''' <summary> Values that represent Stream Reading states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum StreamReadingState

    ''' <summary> An enum constant representing the initial state; Part not present. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the Prime state; Prepare streaming. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the Ready state; Ready to stream. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the Starting state; Stream is starting. </summary>
    <Description("Starting")> Starting

    ''' <summary> An enum constant representing the Busy state; Streaming is active; TO_DO: Rename to Streaming. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the failed state. Stream failed. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the Completed state. Stream completed. </summary>
    <Description("Completed")> Completed

End Enum

''' <summary> Values that represent Stream Reading triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum StreamReadingTrigger

    ''' <summary> An enum constant representing the initial trigger. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the Prime trigger to Prepare streaming; go to the Prime state. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the Ready trigger to get Ready to stream; go to the ready state. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the Starting trigger to start streaming; go to starting state. </summary>
    <Description("Starting")> Starting

    ''' <summary> An enum constant representing the Busy trigger to activate streaming; go to the Busy state. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the Complete trigger to complete streaming; Move to Completed state. </summary>
    <Description("Complete")> Complete

    ''' <summary> An enum constant representing the fail option. Streaming failed. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
    <Description("Proceed")> Proceed

End Enum
