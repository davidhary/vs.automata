Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for the Annealing process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class AnnealingEngine
    Inherits EngineBase(Of AnnealingState, AnnealingTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of AnnealingState, AnnealingTrigger)(AnnealingState.Initial))

        Dim stateConfiguration As StateMachine(Of AnnealingState, AnnealingTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(AnnealingState.Initial)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.PermitReentry(AnnealingTrigger.Initial)
        stateConfiguration.Ignore(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Permit(AnnealingTrigger.LoweringProbe, AnnealingState.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Permit(AnnealingTrigger.SeebeckPartInserted, AnnealingState.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)

        Me.EngagedStates.Add(AnnealingState.SeebeckPartInserted)
        stateConfiguration = Me.StateMachine.Configure(AnnealingState.SeebeckPartInserted)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Ignore(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Ignore(AnnealingTrigger.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.PermitReentry(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Permit(AnnealingTrigger.TemperatureMonitoringStarted, AnnealingState.TemperatureMonitoringStarted)

        Me.EngagedStates.Add(AnnealingState.TemperatureMonitoringStarted)
        stateConfiguration = Me.StateMachine.Configure(AnnealingState.TemperatureMonitoringStarted)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Ignore(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Ignore(AnnealingTrigger.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.Permit(AnnealingTrigger.SeebeckMeasured, AnnealingState.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.PermitReentry(AnnealingTrigger.TemperatureMonitoringStarted)

        Me.EngagedStates.Add(AnnealingState.SeebeckMeasured)
        stateConfiguration = Me.StateMachine.Configure(AnnealingState.SeebeckMeasured)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Ignore(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Permit(AnnealingTrigger.LoweringProbe, AnnealingState.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.PermitReentry(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)

        Me.EngagedStates.Add(AnnealingState.LoweringProbe)
        stateConfiguration = Me.StateMachine.Configure(AnnealingState.LoweringProbe)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Permit(AnnealingTrigger.LiftingProbe, AnnealingState.LiftingProbe)
        stateConfiguration.PermitReentry(AnnealingTrigger.LoweringProbe)
        stateConfiguration.Permit(AnnealingTrigger.Probing, AnnealingState.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)

        stateConfiguration = Me.StateMachine.Configure(AnnealingState.Failed)
        stateConfiguration.PermitReentry(AnnealingTrigger.Fail)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Ignore(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Ignore(AnnealingTrigger.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)

        stateConfiguration = Me.StateMachine.Configure(AnnealingState.Probing)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.Permit(AnnealingTrigger.LiftingProbe, AnnealingState.LiftingProbe)
        stateConfiguration.Ignore(AnnealingTrigger.LoweringProbe)
        stateConfiguration.PermitReentry(AnnealingTrigger.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)

        stateConfiguration = Me.StateMachine.Configure(AnnealingState.LiftingProbe)
        stateConfiguration.Permit(AnnealingTrigger.Fail, AnnealingState.Failed)
        stateConfiguration.Permit(AnnealingTrigger.Initial, AnnealingState.Initial)
        stateConfiguration.PermitReentry(AnnealingTrigger.LiftingProbe)
        stateConfiguration.Ignore(AnnealingTrigger.LoweringProbe)
        stateConfiguration.Ignore(AnnealingTrigger.Probing)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckMeasured)
        stateConfiguration.Ignore(AnnealingTrigger.SeebeckPartInserted)
        stateConfiguration.Ignore(AnnealingTrigger.TemperatureMonitoringStarted)
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Initials this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Initial()
        Me.Fire(AnnealingTrigger.Initial)
    End Sub

    ''' <summary> Inserts this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Insert()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.SeebeckPartInserted)
        End If
    End Sub

    ''' <summary> Removes this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Remove()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.Initial)
        End If
    End Sub

    ''' <summary> Probings this object. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Probing()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.Probing)
        End If
    End Sub

    ''' <summary> Starts temperature monitoring. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub StartTemperatureMonitoring()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.TemperatureMonitoringStarted)
        End If
    End Sub

    ''' <summary> Seebeck measured. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub SeebeckMeasured()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.SeebeckMeasured)
        End If
    End Sub

    ''' <summary> Lifting probe. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub LiftingProbe()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.LiftingProbe)
        End If
    End Sub

    ''' <summary> Lowering probe. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub LoweringProbe()
        If Me.StopRequested Then
            Me.Fire(AnnealingTrigger.Initial)
        Else
            Me.Fire(AnnealingTrigger.LoweringProbe)
        End If
    End Sub

    ''' <summary> Annealing failed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub Fail()
        Me.Fire(AnnealingTrigger.Fail)
    End Sub

#End Region

End Class

''' <summary> Values that represent annealing states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum AnnealingState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the seebeck part inserted option. </summary>
    <Description("Seebeck part inserted")> SeebeckPartInserted

    ''' <summary> An enum constant representing the temperature monitoring started option. </summary>
    <Description("Temperature Monitoring Started")> TemperatureMonitoringStarted

    ''' <summary> An enum constant representing the seebeck measured option. </summary>
    <Description("Seebeck Measured")> SeebeckMeasured

    ''' <summary> An enum constant representing the lowering probe option. </summary>
    <Description("Lowering the probe")> LoweringProbe

    ''' <summary> An enum constant representing the probing option. </summary>
    <Description("Probing")> Probing

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the lifting probe option. </summary>
    <Description("Lifting the probe")> LiftingProbe
End Enum

''' <summary> Values that represent annealing triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum AnnealingTrigger

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the seebeck part inserted option. </summary>
    <Description("Seebeck part inserted")> SeebeckPartInserted

    ''' <summary> An enum constant representing the temperature monitoring started option. </summary>
    <Description("Temperature Monitoring Started")> TemperatureMonitoringStarted

    ''' <summary> An enum constant representing the seebeck measured option. </summary>
    <Description("Seebeck Measured")> SeebeckMeasured

    ''' <summary> An enum constant representing the lowering probe option. </summary>
    <Description("Lowering the probe")> LoweringProbe

    ''' <summary> An enum constant representing the probing option. </summary>
    <Description("Probing")> Probing

    ''' <summary> An enum constant representing the fail option. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the lifting probe option. </summary>
    <Description("Lifting the probe")> LiftingProbe
End Enum
