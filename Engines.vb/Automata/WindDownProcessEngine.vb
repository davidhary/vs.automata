Imports System.ComponentModel

Imports Stateless

''' <summary> Implements a finite automata for a Wind-Down process. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class WindDownProcessEngine
    Inherits EngineBase(Of WindDownProcessState, WindDownProcessTrigger)

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name, New StateMachine(Of WindDownProcessState, WindDownProcessTrigger)(WindDownProcessState.Initial))

        Dim stateConfiguration As StateMachine(Of WindDownProcessState, WindDownProcessTrigger).StateConfiguration

        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Initial)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Initial)
        stateConfiguration.Permit(WindDownProcessTrigger.Prime, WindDownProcessState.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.Permit(WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Prime)

        Me.EngagedStates.Add(WindDownProcessState.Prime)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Prime)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Prime)
        stateConfiguration.Permit(WindDownProcessTrigger.Ready, WindDownProcessState.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Permit(WindDownProcessTrigger.Busy, WindDownProcessState.WindDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Permit(WindDownProcessTrigger.WindDown, WindDownProcessState.WindDown)
        stateConfiguration.Permit(WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Ready)

        Me.EngagedStates.Add(WindDownProcessState.Ready)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Ready)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Permit(WindDownProcessTrigger.Prime, WindDownProcessState.Prime)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Ready)
        stateConfiguration.Permit(WindDownProcessTrigger.Starting, WindDownProcessState.Starting)
        stateConfiguration.Permit(WindDownProcessTrigger.Busy, WindDownProcessState.Finished)
        stateConfiguration.Permit(WindDownProcessTrigger.Finish, WindDownProcessState.Finished)
        stateConfiguration.Permit(WindDownProcessTrigger.WindDown, WindDownProcessState.WindDown)
        stateConfiguration.Permit(WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Starting)

        Me.EngagedStates.Add(WindDownProcessState.Starting)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Starting)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Ignore(WindDownProcessTrigger.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Starting)
        stateConfiguration.Permit(WindDownProcessTrigger.Busy, WindDownProcessState.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Busy)

        Me.EngagedStates.Add(WindDownProcessState.Busy)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Busy)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Ignore(WindDownProcessTrigger.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Busy)
        stateConfiguration.Permit(WindDownProcessTrigger.Finish, WindDownProcessState.Finished)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Finished)

        Me.EngagedStates.Add(WindDownProcessState.Finished)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Finished)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Ignore(WindDownProcessTrigger.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Finish)
        stateConfiguration.Permit(WindDownProcessTrigger.WindDown, WindDownProcessState.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.WindDown)

        Me.EngagedStates.Add(WindDownProcessState.WindDown)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.WindDown)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Ignore(WindDownProcessTrigger.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.WindDown)
        stateConfiguration.Permit(WindDownProcessTrigger.WindingDown, WindDownProcessState.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.WindingDown)

        Me.EngagedStates.Add(WindDownProcessState.WindingDown)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.WindingDown)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Permit(WindDownProcessTrigger.Prime, WindDownProcessState.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.WindingDown)
        stateConfiguration.Permit(WindDownProcessTrigger.Complete, WindDownProcessState.WoundDown)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.WoundDown)

        Me.EngagedStates.Add(WindDownProcessState.Failed)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.Failed)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Initial)
        stateConfiguration.Permit(WindDownProcessTrigger.Prime, WindDownProcessState.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Ignore(WindDownProcessTrigger.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindingDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.Complete)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Fail)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Initial)

        Me.EngagedStates.Add(WindDownProcessState.WoundDown)
        stateConfiguration = Me.StateMachine.Configure(WindDownProcessState.WoundDown)
        stateConfiguration.Permit(WindDownProcessTrigger.Initial, WindDownProcessState.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Prime)
        stateConfiguration.Permit(WindDownProcessTrigger.Prime, WindDownProcessState.Prime)
        stateConfiguration.Ignore(WindDownProcessTrigger.Ready)
        stateConfiguration.Permit(WindDownProcessTrigger.Starting, WindDownProcessState.Starting)
        stateConfiguration.Ignore(WindDownProcessTrigger.Busy)
        stateConfiguration.Ignore(WindDownProcessTrigger.Finish)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindDown)
        stateConfiguration.Ignore(WindDownProcessTrigger.WindingDown)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Complete)
        stateConfiguration.Permit(WindDownProcessTrigger.Fail, WindDownProcessState.Failed)
        stateConfiguration.PermitReentry(WindDownProcessTrigger.Cancel)
        stateConfiguration.Permit(WindDownProcessTrigger.Proceed, WindDownProcessState.Initial)

    End Sub

#End Region

#Region " PROGRESS "

    ''' <summary> Gets the state progress. </summary>
    ''' <value> The state progress. </value>
    Public ReadOnly Property StateProgress As Integer
        Get
            Return CInt(100 * Me.CurrentState / WindDownProcessState.WoundDown)
        End Get
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> The percent progress. </summary>
    ''' <value> The percent progress. </value>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Set(value As Integer)
            value = Math.Min(100, Math.Max(0, value))
            If value <> Me.PercentProgress Then
                Me._PercentProgress = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Updates the percent progress. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub UpdatePercentProgress()
        If Me.CurrentState <> WindDownProcessState.Initial Then
            Me.PercentProgress = Me.StateProgress
        End If
    End Sub

#End Region

#Region " COMMANDs "

    ''' <summary> Process the stop request. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Overrides Sub ProcessStopRequest()
        If Me.StoppingTimeoutElapsed Then
            Me.Fire(WindDownProcessTrigger.Initial)
        Else
            Select Case Me.CurrentState
                Case WindDownProcessState.Initial
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.Prime
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.Ready
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.Starting
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.Busy
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.Finished
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.WindDown
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case WindDownProcessState.WindingDown
                    Me.Fire(WindDownProcessTrigger.Complete)
                Case WindDownProcessState.WoundDown
                    Me.Fire(WindDownProcessTrigger.Complete)
                Case WindDownProcessState.Failed
                    Me.Fire(WindDownProcessTrigger.Initial)
                Case Else
                    Throw New InvalidOperationException($"Unhanded {Me.StateMachine.State} state processing stop")
            End Select
        End If
    End Sub

#End Region

End Class

''' <summary> Values that represent wind-down process states. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum WindDownProcessState

    ''' <summary> An enum constant representing the initial option. </summary>
    <Description("Initial")> Initial

    ''' <summary> An enum constant representing the prime option. </summary>
    <Description("Prime")> Prime

    ''' <summary> An enum constant representing the ready option. </summary>
    <Description("Ready")> Ready

    ''' <summary> An enum constant representing the starting option. </summary>
    <Description("Starting")> Starting

    ''' <summary> An enum constant representing the busy option. </summary>
    <Description("Busy")> Busy

    ''' <summary> An enum constant representing the finished option. </summary>
    <Description("Finished")> Finished

    ''' <summary> An enum constant representing the wind down option. </summary>
    <Description("Wind Down")> WindDown

    ''' <summary> An enum constant representing the winding down option. </summary>
    <Description("Winding Down")> WindingDown

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Failed")> Failed

    ''' <summary> An enum constant representing the wound down option. </summary>
    <Description("Wound Down")> WoundDown
End Enum

''' <summary> Values that represent wind-down process triggers. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Enum WindDownProcessTrigger

    ''' <summary> . </summary>
    <Description("Initial: Any -> Initial")> Initial

    ''' <summary> . </summary>
    <Description("Prime: Idle -> Prime")> Prime

    ''' <summary> . </summary>
    <Description("Ready: Prime -> Test Ready")> Ready

    ''' <summary> . </summary>
    <Description("Starting: Ready -> Starting")> Starting

    ''' <summary> . </summary>
    <Description("Busy: Starting -> Busy")> Busy

    ''' <summary> . </summary>
    <Description("Finish: Busy --> Finished")> Finish

    ''' <summary> . </summary>
    <Description("Wind Down: Any State -> Wind Down")> WindDown

    ''' <summary> . </summary>
    <Description("Winding Down: Wind Down -> Winding Down")> WindingDown

    ''' <summary> . </summary>
    <Description("Complete: Finished, Wind Down, Winding down -> Wound Down")> Complete

    ''' <summary> An enum constant representing the cancel option. </summary>
    <Description("Cancel testing")> Cancel

    ''' <summary> An enum constant representing the fail option. Finite state machine failed. </summary>
    <Description("Fail")> Fail

    ''' <summary> An enum constant representing the Proceed trigger to move the process to the next state. </summary>
    <Description("Proceed")> Proceed
End Enum

