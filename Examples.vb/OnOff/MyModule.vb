Imports Stateless
Imports OnOff.ExceptionExtensions

Friend Module MyModule

    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Main()
        Try
            Dim [on] As String = "On", [off] As String = "Off"
            Dim space As Char = " "c

            Dim onOffSwitch As New StateMachine(Of String, Char)([off])

            onOffSwitch.Configure([off]).Permit(space, [on])
            onOffSwitch.Configure([on]).Permit(space, [off])

            Console.WriteLine("Press <space> to toggle the switch. Any other key will raise an error.")

            Do
                Console.WriteLine("Switch is in state: " & onOffSwitch.State)
                Dim pressed As Char = Console.ReadKey(True).KeyChar
                onOffSwitch.Fire(pressed)
            Loop
        Catch ex As Exception
            Console.WriteLine($"Exception: {ex.ToFullBlownString}")
            Console.WriteLine("Press any key to continue...")
            Console.ReadKey(True)
        End Try
    End Sub

End Module

