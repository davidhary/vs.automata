Imports Stateless

Friend Class Program

    Private Enum Trigger
        CallDialed
        HungUp
        CallConnected
        LeftMessage
        PlacedOnHold
        TakenOffHold
        PhoneHurledAgainstWall
        MuteMicrophone
        UnmuteMicrophone
        SetVolume
    End Enum

    Private Enum State
        OffHook
        Ringing
        Connected
        OnHold
        PhoneDestroyed
    End Enum

    Private Shared _SetVolumeTrigger As StateMachine(Of State, Trigger).TriggerWithParameters(Of Integer)

    ''' <summary> Main entry-point for this application. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="args"> An array of command-line argument strings. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    Public Shared Sub Main(ByVal args() As String)

        Dim phoneCall As New StateMachine(Of State, Trigger)(State.OffHook)

        _SetVolumeTrigger = phoneCall.SetTriggerParameters(Of Integer)(Trigger.SetVolume)

        Dim stateConfiguration As StateMachine(Of State, Trigger).StateConfiguration

        stateConfiguration = phoneCall.Configure(State.OffHook)
        stateConfiguration.Permit(Trigger.CallDialed, State.Ringing)

        stateConfiguration = phoneCall.Configure(State.Ringing)
        stateConfiguration.Permit(Trigger.HungUp, State.OffHook)
        stateConfiguration.Permit(Trigger.CallConnected, State.Connected)

        stateConfiguration = phoneCall.Configure(State.Connected)
        stateConfiguration.OnEntry(Sub(t) StartCallTimer())
        stateConfiguration.OnExit(Sub(t) StopCallTimer())
        stateConfiguration.InternalTransition(Trigger.MuteMicrophone, Sub(t) OnMute())
        stateConfiguration.InternalTransition(Trigger.UnmuteMicrophone, Sub(t) OnUnmute())
        stateConfiguration.InternalTransition(Of Integer)(_SetVolumeTrigger, Sub(volume, t) OnSetVolume(volume))
        stateConfiguration.Permit(Trigger.LeftMessage, State.OffHook)
        stateConfiguration.Permit(Trigger.HungUp, State.OffHook)
        stateConfiguration.Permit(Trigger.PlacedOnHold, State.OnHold)
        stateConfiguration.OnEntry(Sub(t) StateConnected())

        stateConfiguration = phoneCall.Configure(State.OnHold)
        stateConfiguration.SubstateOf(State.Connected)
        stateConfiguration.Permit(Trigger.TakenOffHold, State.Connected)
        stateConfiguration.Permit(Trigger.HungUp, State.OffHook)
        stateConfiguration.Permit(Trigger.PhoneHurledAgainstWall, State.PhoneDestroyed)

        Print(phoneCall)
        Fire(phoneCall, Trigger.CallDialed)
        Print(phoneCall)
        Fire(phoneCall, Trigger.CallConnected)
        Print(phoneCall)
        SetVolume(phoneCall, 2)
        Print(phoneCall)
        Fire(phoneCall, Trigger.PlacedOnHold)
        Print(phoneCall)
        Fire(phoneCall, Trigger.MuteMicrophone)
        Print(phoneCall)
        Fire(phoneCall, Trigger.UnmuteMicrophone)
        Print(phoneCall)
        Fire(phoneCall, Trigger.TakenOffHold)
        Print(phoneCall)
        SetVolume(phoneCall, 11)
        Print(phoneCall)
        Fire(phoneCall, Trigger.HungUp)
        Print(phoneCall)

        Console.WriteLine("Press any key...")
        Console.ReadKey(True)
    End Sub

    Private Shared Sub OnSetVolume(ByVal volume As Integer)
        Console.WriteLine("Volume set to " & volume & "!")
    End Sub

    Private Shared Sub OnUnmute()
        Console.WriteLine("Microphone muted!")
    End Sub

    Private Shared Sub OnMute()
        Console.WriteLine("Microphone muted!")
    End Sub

    Private Shared Sub StartCallTimer()
        Console.WriteLine("[Timer:] Call started at {0}", DateTimeOffset.Now)
    End Sub
    Private Shared Sub StateConnected()
        Console.WriteLine("[Action:] we can add a second handler")
    End Sub


    Private Shared Sub StopCallTimer()
        Console.WriteLine("[Timer:] Call ended at {0}", DateTimeOffset.Now)
    End Sub

    Private Shared Sub Fire(ByVal phoneCall As StateMachine(Of State, Trigger), ByVal trigger As Trigger)
        Console.WriteLine("[Firing:] {0}", trigger)

        phoneCall.Fire(trigger)
    End Sub

    Private Shared Sub SetVolume(ByVal phoneCall As StateMachine(Of State, Trigger), ByVal volume As Integer)
        phoneCall.Fire(_SetVolumeTrigger, volume)
    End Sub

    Private Shared Sub Print(ByVal phoneCall As StateMachine(Of State, Trigger))
        Console.WriteLine("[Status:] {0}", phoneCall.State.ToString)
    End Sub

End Class
