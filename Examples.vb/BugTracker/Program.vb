
Friend Class Program
    Public Shared Sub Main()
        Dim bug As New Bug("Incorrect stock count")
        bug.Assign("Joe")
        bug.Defer()
        bug.Assign("Harry")
        bug.Assign("Fred")
        bug.Close()
        Console.WriteLine()
        Console.WriteLine("State machine:")
        Console.WriteLine(bug.ToDotGraph())
        Console.ReadKey(False)
    End Sub
End Class
