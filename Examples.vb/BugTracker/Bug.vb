Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Stateless
Imports Stateless.Graph

Public Class Bug

    Private Enum State
        Open
        Assigned
        Deferred
        Resolved
        Closed
    End Enum

    Private Enum Trigger
        Assign
        Defer
        Resolve
        Close
    End Enum

    Private _State As State = State.Open

    Private ReadOnly _Machine As StateMachine(Of State, Trigger)

    Private ReadOnly _AssignTrigger As StateMachine(Of State, Trigger).TriggerWithParameters(Of String)

    Private ReadOnly _Title As String

    Private _Assignee As String

    Public Sub New(ByVal title As String)

        Me._Title = title

        Me._Machine = New StateMachine(Of State, Trigger)(Function() Me._State, Sub(s) Me._State = s)

        Me._AssignTrigger = Me._Machine.SetTriggerParameters(Of String)(Trigger.Assign)

        Me._Machine.Configure(State.Open).Permit(Trigger.Assign, State.Assigned)

        Dim stateConfiguration As StateMachine(Of State, Trigger).StateConfiguration
        Dim substateConfiguration As StateMachine(Of State, Trigger).StateConfiguration

        stateConfiguration = Me._Machine.Configure(State.Assigned)
        substateConfiguration = stateConfiguration.SubstateOf(State.Open)
        substateConfiguration.OnEntryFrom(Me._AssignTrigger, Sub(assignee) Me.OnAssigned(assignee))
        substateConfiguration.PermitReentry(Trigger.Assign)
        substateConfiguration.Permit(Trigger.Close, State.Closed)
        substateConfiguration.Permit(Trigger.Defer, State.Deferred)
        substateConfiguration.OnExit(Sub() Me.OnDeassigned())

        stateConfiguration = Me._Machine.Configure(State.Deferred)
        stateConfiguration.OnEntry(Sub() Me._Assignee = Nothing)
        stateConfiguration.Permit(Trigger.Assign, State.Assigned)

    End Sub

    Public Sub Close()
        Me._Machine.Fire(Trigger.Close)
    End Sub

    Public Sub Assign(ByVal assignee As String)
        Me._Machine.Fire(Me._AssignTrigger, assignee)
    End Sub

    Public ReadOnly Property CanAssign() As Boolean
        Get
            Return Me._Machine.CanFire(Trigger.Assign)
        End Get
    End Property

    Public Sub Defer()
        Me._Machine.Fire(Trigger.Defer)
    End Sub

    Private Sub OnAssigned(ByVal assignee As String)
        If Me._Assignee IsNot Nothing AndAlso assignee <> Me._Assignee Then
            Me.SendEmailToAssignee("Don't forget to help the new employee!")
        End If

        Me._Assignee = assignee
        Me.SendEmailToAssignee("You own it.")
    End Sub

    Private Sub OnDeassigned()
        Me.SendEmailToAssignee("You're off the hook.")
    End Sub

    Private Sub SendEmailToAssignee(ByVal message As String)
        Console.WriteLine("{0}, RE {1}: {2}", Me._Assignee, Me._Title, message)
    End Sub

    Public Function ToDotGraph() As String
        Return Stateless.Graph.UmlDotGraph.Format(Me._Machine.GetInfo())
    End Function

End Class
