Imports isr.Core
Imports isr.Finite.Automata.Engines.ExceptionExtensions
Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    Partial Friend NotInheritable Class MyApplication

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Finite Automata Bimanual Toggle"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Finite Automata Bimanual Toggle"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Automata.Finite.Bimanual.Toggle"

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Automata + &H4

        ''' <summary> Gets or sets the unpublished trace messages. </summary>
        ''' <value> The unpublished trace messages. </value>
        Public Shared ReadOnly Property UnpublishedTraceMessages As TraceMessagesQueue = New TraceMessagesQueue

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 2020-10-01. </remarks>
        ''' <param name="activity">  The activity. </param>
        ''' <param name="exception"> The exception. </param>
        Public Shared Sub LogUnpublishedException(ByVal activity As String, ByVal exception As Exception)
            MyApplication.LogUnpublishedException(New TraceMessage(TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity};
logged to: {My.Application.Log.DefaultFileLogWriter.FullLogFileName};
{exception.ToFullBlownString}"))
        End Sub

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 2020-10-01. </remarks>
        ''' <param name="message"> The message. </param>
        Public Shared Sub LogUnpublishedException(message As TraceMessage)
            If message IsNot Nothing Then
                MyApplication.UnpublishedTraceMessages.Enqueue(message)
                My.Application.Log.DefaultFileLogWriter.WriteLine(message.ToString)
                My.Application.Log.DefaultFileLogWriter.Flush()
            End If
        End Sub

    End Class

End Namespace

