﻿// General Information about an assembly is controlled through the following 
using System.Reflection;

// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyCompany( "Integrated Scientific Resources" )]
[assembly: AssemblyCopyright( "(c) 2006 Scientific Resources, Inc. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: System.Resources.NeutralResourcesLanguage( "en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly )]
[assembly: AssemblyCulture( "" )]
