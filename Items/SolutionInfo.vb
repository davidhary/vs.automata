' General Information about an assembly is controlled through the following 
Imports System.Reflection

' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyCopyright("(c) 2006 Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")>

