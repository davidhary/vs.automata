Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Automata.Finite.Forms.ExceptionExtensions
Imports Stateless.Graph

''' <summary> A Engine monitor. </summary>
''' <remarks> David, 2020-10-01. </remarks>
Public Class EngineMonitor(Of TState, TTrigger)
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTORES "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._DefaultInfoProviderButton = Me._EngineInfoButton

        Me.Enabled = False
        ' https://stackoverflow.com/questions/2646606/how-do-i-reclaim-the-space-from-the-grip
        ' The padding is broken. MS won't fix this.
        Me._TriggerStatusStrip.SizingGrip = False
        Me._TriggerStatusStrip.Padding = New Padding(Me._TriggerStatusStrip.Padding.Left, Me._TriggerStatusStrip.Padding.Top,
                                                     Me._TriggerStatusStrip.Padding.Left, Me._TriggerStatusStrip.Padding.Bottom)
        Me._NameStatusStrip.SizingGrip = False
        Me._NameStatusStrip.Padding = New Padding(Me._NameStatusStrip.Padding.Left, Me._NameStatusStrip.Padding.Top,
                                                      Me._NameStatusStrip.Padding.Left, Me._NameStatusStrip.Padding.Bottom)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._Engine = Nothing
                If Me.components IsNot Nothing Then Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " ENGINE LISTENER "

    ''' <summary> Gets or sets the Engine. </summary>
    ''' <value> The Engine. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), EditorBrowsable(EditorBrowsableState.Never)>
    Public ReadOnly Property Engine As EngineBase(Of TState, TTrigger)

    ''' <summary> Assigns an engine. </summary>
    ''' <remarks> David, 2020-08-26. </remarks>
    ''' <param name="engine"> The Engine. </param>
    Public Sub AssignEngine(ByVal engine As EngineBase(Of TState, TTrigger))
        If Me._Engine IsNot Nothing Then
            Me.Engine.UnregisterEngineFailureAction(Sub(e) Me.Annunciate(e))
            Me._Engine.Dispose()
            Me._Engine = Nothing
        End If
        Me._Engine = engine
        Me.Enabled = engine IsNot Nothing
        If engine IsNot Nothing Then
            Me.Engine.StateMachine.OnTransitionedAsync(Function(t As Stateless.StateMachine(Of TState, TTrigger).Transition)
                                                           Return Task.Run(Sub() Me.OnStateTransitioned(t))
                                                       End Function)
            engine.RegisterEngineFailureAction(Sub(e) Me.Annunciate(e))
            If Me.Engine.LastTransition Is Nothing Then
                Me._EngineInfoButton.Text = Me.Engine.CurrentState.ToString
            Else
                Me.OnStateTransitioned(Me.Engine.LastTransition)
            End If
            Me._EngineNameLabel.Text = Me.Engine.Name
        End If
    End Sub

    ''' <summary> Handles the Stream Reading Engine state transition actions. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnStateTransitioned(ByVal transition As Stateless.StateMachine(Of TState, TTrigger).Transition)
        Dim activity As String = String.Empty
        Try
            If transition Is Nothing Then
                Me._LastTriggerLabel.Text = "<trigger>"
                Me._PreviousStateTextBox.Text = "<previous state>"
                Me._EngineInfoButton.Text = "<state>"
            Else
                Me._LastTriggerLabel.Text = transition.Trigger.ToString
                Me._PreviousStateTextBox.Text = transition.Source.ToString
                Me._EngineInfoButton.Text = transition.Destination.ToString
            End If
        Catch ex As Exception
            Me.Annunciate(Core.Forma.InfoProviderLevel.Error, $"Exception {activity};. {ex.ToFullBlownString}")
        End Try
    End Sub

    ''' <summary> Gets or sets the default information provider button. </summary>
    ''' <value> The default information provider button. </value>
    Private ReadOnly Property DefaultInfoProviderButton As System.Windows.Forms.ToolStripItem

    ''' <summary> Refresh display. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Private Sub RefreshDisplay()
        Me.DisplayTransitionInfo()
    End Sub

    ''' <summary> Displays a transition information. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Private Sub DisplayTransitionInfo()
        If Me.Engine?.LastTransition Is Nothing Then
            Me._LastTriggerLabel.Text = "<trigger>"
            Me._PreviousStateTextBox.Text = "<previous state>"
            Me._EngineInfoButton.Text = "<state>"
        Else
            Me._LastTriggerLabel.Text = Me.Engine.LastTransition.Trigger.ToString
            Me._PreviousStateTextBox.Text = Me.Engine.LastTransition.Source.ToString
            Me._EngineInfoButton.Text = Me.Engine.LastTransition.Destination.ToString
        End If
    End Sub

    ''' <summary> The nothing to report. </summary>
    Private Const _NothingToReport As String = "Nothing to report"

    ''' <summary> Message describing the annunciated. </summary>
    Private _AnnunciatedMessage As String

    ''' <summary> Annunciates. </summary>
    ''' <remarks> David, 2020-08-26. </remarks>
    ''' <param name="infoLevel"> The information level. </param>
    ''' <param name="message">   The message. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub Annunciate(ByVal infoLevel As Core.Forma.InfoProviderLevel, ByVal message As String)
        Try
            If Not String.Equals(message, Me._AnnunciatedMessage) Then
                Me._AnnunciatedMessage = message
                Me.InfoProvider.Annunciate(Me.DefaultInfoProviderButton, infoLevel, message)
                Me.DefaultInfoProviderButton.ToolTipText = "Observed"
                If Not String.Equals(message, _NothingToReport) Then
                    Clipboard.SetText(message)
                End If
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Annunciates. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="e"> Error event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub Annunciate(ByVal e As System.IO.ErrorEventArgs)
        If e Is Nothing OrElse e.GetException Is Nothing Then Return
        Try
            If Not String.Equals(e.GetException.Message, Me._AnnunciatedMessage) Then
                Me._AnnunciatedMessage = e.GetException.ToFullBlownString
                Me.InfoProvider.Annunciate(Me.DefaultInfoProviderButton, Core.Forma.InfoProviderLevel.Error, Me._AnnunciatedMessage)
                Me.DefaultInfoProviderButton.ToolTipText = "Observed"
                If Not String.Equals(Me._AnnunciatedMessage, _NothingToReport) Then
                    Clipboard.SetText(Me._AnnunciatedMessage)
                End If
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Annunciates. </summary>
    ''' <remarks> David, 2020-08-26. </remarks>
    ''' <param name="infoLevel"> The information level. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub Annunciate(ByVal infoLevel As Core.Forma.InfoProviderLevel, ByVal format As String, ByVal ParamArray args() As Object)
        Try
            Me.Annunciate(infoLevel, String.Format(format, args))
        Catch
        End Try
    End Sub

#End Region

End Class
