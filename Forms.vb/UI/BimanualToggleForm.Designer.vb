<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BimanualToggleForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BimanualToggle1 = New isr.Automata.Finite.Forms.BimanualToggle()
        Me.SuspendLayout()
        '
        'BimanualToggle1
        '
        Me.BimanualToggle1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BimanualToggle1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BimanualToggle1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BimanualToggle1.Location = New System.Drawing.Point(0, 0)
        Me.BimanualToggle1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.BimanualToggle1.Name = "BimanualToggle1"
        Me.BimanualToggle1.Size = New System.Drawing.Size(406, 411)
        Me.BimanualToggle1.TabIndex = 0
        '
        'BimanualToggleForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 411)
        Me.Controls.Add(Me.BimanualToggle1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "BimanualToggleForm"
        Me.Text = "BimanualToggleForm"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents BimanualToggle1 As BimanualToggle
End Class
