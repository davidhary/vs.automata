<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EngineMonitor(Of TState, TTrigger)

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._TriggerStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._LastTriggerLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._EngineToolStrip = New System.Windows.Forms.ToolStrip()
        Me._EngineInfoButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._PreviousStateTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._NameStatusStrip = New System.Windows.Forms.StatusStrip()
        Me._EngineNameLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._TriggerStatusStrip.SuspendLayout()
        Me._EngineToolStrip.SuspendLayout()
        Me._NameStatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_TriggerStatusStrip
        '
        Me._TriggerStatusStrip.AutoSize = False
        Me._TriggerStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._TriggerStatusStrip.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerStatusStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._TriggerStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._LastTriggerLabel})
        Me._TriggerStatusStrip.Location = New System.Drawing.Point(0, 16)
        Me._TriggerStatusStrip.Name = "_TriggerStatusStrip"
        Me._TriggerStatusStrip.ShowItemToolTips = True
        Me._TriggerStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._TriggerStatusStrip.SizingGrip = False
        Me._TriggerStatusStrip.TabIndex = 1
        Me._TriggerStatusStrip.Text = "Trigger Status Strip"
        '
        '_LastTriggerLabel
        '
        Me._LastTriggerLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._LastTriggerLabel.Name = "_LastTriggerLabel"
        Me._LastTriggerLabel.Size = New System.Drawing.Size(148, 16)
        Me._LastTriggerLabel.Spring = True
        Me._LastTriggerLabel.Text = "<trigger>"
        Me._LastTriggerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._LastTriggerLabel.ToolTipText = "Last trigger"
        '
        '_EngineToolStrip
        '
        Me._EngineToolStrip.AutoSize = False
        Me._EngineToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._EngineToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._EngineToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EngineInfoButton})
        Me._EngineToolStrip.Location = New System.Drawing.Point(0, 33)
        Me._EngineToolStrip.Name = "_EngineToolStrip"
        Me._EngineToolStrip.Padding = New System.Windows.Forms.Padding(0)
        Me._EngineToolStrip.Size = New System.Drawing.Size(163, 20)
        Me._EngineToolStrip.TabIndex = 2
        Me._EngineToolStrip.Text = "Engine Tool Strip"
        '
        '_EngineInfoButton
        '
        Me._EngineInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._EngineInfoButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PreviousStateTextBox})
        Me._EngineInfoButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EngineInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._EngineInfoButton.Margin = New System.Windows.Forms.Padding(1)
        Me._EngineInfoButton.Name = "_EngineInfoButton"
        Me._EngineInfoButton.Size = New System.Drawing.Size(26, 18)
        Me._EngineInfoButton.Text = "i"
        Me._EngineInfoButton.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me._EngineInfoButton.ToolTipText = "Current state + Additional info"
        '
        '_PreviousStateTextBox
        '
        Me._PreviousStateTextBox.Name = "_PreviousStateTextBox"
        Me._PreviousStateTextBox.Size = New System.Drawing.Size(100, 23)
        '
        '_NameStatusStrip
        '
        Me._NameStatusStrip.AutoSize = False
        Me._NameStatusStrip.Dock = System.Windows.Forms.DockStyle.Top
        Me._NameStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EngineNameLabel})
        Me._NameStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me._NameStatusStrip.Name = "_NameStatusStrip"
        Me._NameStatusStrip.ShowItemToolTips = True
        Me._NameStatusStrip.Size = New System.Drawing.Size(163, 16)
        Me._NameStatusStrip.SizingGrip = False
        Me._NameStatusStrip.TabIndex = 3
        Me._NameStatusStrip.Text = "Name Status Strip"
        '
        '_EngineNameLabel
        '
        Me._EngineNameLabel.Margin = New System.Windows.Forms.Padding(0)
        Me._EngineNameLabel.Name = "_EngineNameLabel"
        Me._EngineNameLabel.Size = New System.Drawing.Size(148, 16)
        Me._EngineNameLabel.Spring = True
        Me._EngineNameLabel.Text = "<name>"
        Me._EngineNameLabel.ToolTipText = "Automaton name"
        '
        'EngineMonitor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._EngineToolStrip)
        Me.Controls.Add(Me._TriggerStatusStrip)
        Me.Controls.Add(Me._NameStatusStrip)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "EngineMonitor"
        Me.Size = New System.Drawing.Size(163, 53)
        Me._TriggerStatusStrip.ResumeLayout(False)
        Me._TriggerStatusStrip.PerformLayout()
        Me._EngineToolStrip.ResumeLayout(False)
        Me._EngineToolStrip.PerformLayout()
        Me._NameStatusStrip.ResumeLayout(False)
        Me._NameStatusStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _TriggerStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _EngineToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _EngineInfoButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _LastTriggerLabel As Windows.Forms.ToolStripStatusLabel
    Private WithEvents _PreviousStateTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _NameStatusStrip As Windows.Forms.StatusStrip
    Private WithEvents _EngineNameLabel As Windows.Forms.ToolStripStatusLabel
End Class
