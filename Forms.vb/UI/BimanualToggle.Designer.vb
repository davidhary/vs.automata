<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BimanualToggle
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ErrorLable = New System.Windows.Forms.Label()
        Me._InfoLabel = New System.Windows.Forms.Label()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me._SetStateLabelLabel = New System.Windows.Forms.Label()
        Me._OffStateLabelLabel = New System.Windows.Forms.Label()
        Me._OnStateLabel = New System.Windows.Forms.Label()
        Me._ResponseTimeLabelLabel = New System.Windows.Forms.Label()
        Me._OnStateLabelLabel = New System.Windows.Forms.Label()
        Me._GoStateLabeLabel = New System.Windows.Forms.Label()
        Me._ResponseTimeLabel = New System.Windows.Forms.Label()
        Me._AcknowledgeButton = New System.Windows.Forms.Button()
        Me._EStopStateLabel = New System.Windows.Forms.Label()
        Me._RightPushButton = New System.Windows.Forms.CheckBox()
        Me._LeftPushButton = New System.Windows.Forms.CheckBox()
        Me._GoStateLabel = New System.Windows.Forms.Label()
        Me._SetStateLabel = New System.Windows.Forms.Label()
        Me._OffStateLabel = New System.Windows.Forms.Label()
        Me._EStopStateLabelLabel = New System.Windows.Forms.Label()
        Me._CurrentStateLabel = New System.Windows.Forms.Label()
        Me._EStopButton = New System.Windows.Forms.Button()
        Me._Layout.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ErrorLable
        '
        Me._ErrorLable.BackColor = System.Drawing.SystemColors.Control
        Me._ErrorLable.Cursor = System.Windows.Forms.Cursors.Default
        Me._ErrorLable.Dock = System.Windows.Forms.DockStyle.Left
        Me._ErrorLable.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ErrorLable.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ErrorLable.Location = New System.Drawing.Point(19, 286)
        Me._ErrorLable.Name = "_ErrorLable"
        Me._ErrorLable.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ErrorLable.Size = New System.Drawing.Size(368, 82)
        Me._ErrorLable.TabIndex = 35
        Me._ErrorLable.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_InfoLabel
        '
        Me._InfoLabel.BackColor = System.Drawing.SystemColors.Control
        Me._InfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._InfoLabel.Dock = System.Windows.Forms.DockStyle.Left
        Me._InfoLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._InfoLabel.Location = New System.Drawing.Point(19, 368)
        Me._InfoLabel.Name = "_InfoLabel"
        Me._InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InfoLabel.Size = New System.Drawing.Size(368, 62)
        Me._InfoLabel.TabIndex = 28
        Me._InfoLabel.Text = "An OSHA-compliant Bi-Manual Enabler control with E-Stop.  This control is enabled" &
    " upon receiving an acknowledge signal after both push buttons are pressed within" &
    " a preset time interval."
        Me._InfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me.Panel1, 1, 1)
        Me._Layout.Controls.Add(Me._ErrorLable, 1, 2)
        Me._Layout.Controls.Add(Me._InfoLabel, 1, 3)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(0, 0)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 5
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(406, 447)
        Me._Layout.TabIndex = 36
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me._SetStateLabelLabel)
        Me.Panel1.Controls.Add(Me._OffStateLabelLabel)
        Me.Panel1.Controls.Add(Me._ResponseTimeLabelLabel)
        Me.Panel1.Controls.Add(Me._OnStateLabelLabel)
        Me.Panel1.Controls.Add(Me._GoStateLabeLabel)
        Me.Panel1.Controls.Add(Me._ResponseTimeLabel)
        Me.Panel1.Controls.Add(Me._AcknowledgeButton)
        Me.Panel1.Controls.Add(Me._RightPushButton)
        Me.Panel1.Controls.Add(Me._LeftPushButton)
        Me.Panel1.Controls.Add(Me._GoStateLabel)
        Me.Panel1.Controls.Add(Me._SetStateLabel)
        Me.Panel1.Controls.Add(Me._OffStateLabel)
        Me.Panel1.Controls.Add(Me._EStopStateLabelLabel)
        Me.Panel1.Controls.Add(Me._CurrentStateLabel)
        Me.Panel1.Controls.Add(Me._EStopButton)
        Me.Panel1.Controls.Add(Me._OnStateLabel)
        Me.Panel1.Controls.Add(Me._EStopStateLabel)
        Me.Panel1.Location = New System.Drawing.Point(19, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(368, 264)
        Me.Panel1.TabIndex = 37
        '
        '_SetStateLabelLabel
        '
        Me._SetStateLabelLabel.AutoSize = True
        Me._SetStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._SetStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._SetStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SetStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._SetStateLabelLabel.Location = New System.Drawing.Point(308, 115)
        Me._SetStateLabelLabel.Name = "_SetStateLabelLabel"
        Me._SetStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._SetStateLabelLabel.Size = New System.Drawing.Size(26, 14)
        Me._SetStateLabelLabel.TabIndex = 42
        Me._SetStateLabelLabel.Text = "SET"
        Me._SetStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_OffStateLabelLabel
        '
        Me._OffStateLabelLabel.AutoSize = True
        Me._OffStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._OffStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._OffStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OffStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._OffStateLabelLabel.Location = New System.Drawing.Point(227, 68)
        Me._OffStateLabelLabel.Name = "_OffStateLabelLabel"
        Me._OffStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._OffStateLabelLabel.Size = New System.Drawing.Size(27, 14)
        Me._OffStateLabelLabel.TabIndex = 41
        Me._OffStateLabelLabel.Text = "OFF"
        Me._OffStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_OnStateLabel
        '
        Me._OnStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._OnStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._OnStateLabel.Location = New System.Drawing.Point(119, 101)
        Me._OnStateLabel.Name = "_OnStateLabel"
        Me._OnStateLabel.Size = New System.Drawing.Size(75, 42)
        Me._OnStateLabel.TabIndex = 34
        '
        '_ResponseTimeLabelLabel
        '
        Me._ResponseTimeLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._ResponseTimeLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResponseTimeLabelLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResponseTimeLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResponseTimeLabelLabel.Location = New System.Drawing.Point(6, 54)
        Me._ResponseTimeLabelLabel.Name = "_ResponseTimeLabelLabel"
        Me._ResponseTimeLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResponseTimeLabelLabel.Size = New System.Drawing.Size(122, 17)
        Me._ResponseTimeLabelLabel.TabIndex = 48
        Me._ResponseTimeLabelLabel.Text = "Bi-Manual Timeout"
        '
        '_OnStateLabelLabel
        '
        Me._OnStateLabelLabel.AutoSize = True
        Me._OnStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._OnStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._OnStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OnStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._OnStateLabelLabel.Location = New System.Drawing.Point(148, 115)
        Me._OnStateLabelLabel.Name = "_OnStateLabelLabel"
        Me._OnStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._OnStateLabelLabel.Size = New System.Drawing.Size(22, 14)
        Me._OnStateLabelLabel.TabIndex = 45
        Me._OnStateLabelLabel.Text = "ON"
        Me._OnStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_GoStateLabeLabel
        '
        Me._GoStateLabeLabel.AutoSize = True
        Me._GoStateLabeLabel.BackColor = System.Drawing.Color.Transparent
        Me._GoStateLabeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._GoStateLabeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GoStateLabeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._GoStateLabeLabel.Location = New System.Drawing.Point(229, 162)
        Me._GoStateLabeLabel.Name = "_GoStateLabeLabel"
        Me._GoStateLabeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._GoStateLabeLabel.Size = New System.Drawing.Size(23, 14)
        Me._GoStateLabeLabel.TabIndex = 43
        Me._GoStateLabeLabel.Text = "GO"
        Me._GoStateLabeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_ResponseTimeLabel
        '
        Me._ResponseTimeLabel.BackColor = System.Drawing.SystemColors.Control
        Me._ResponseTimeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._ResponseTimeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._ResponseTimeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResponseTimeLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._ResponseTimeLabel.Location = New System.Drawing.Point(7, 73)
        Me._ResponseTimeLabel.Name = "_ResponseTimeLabel"
        Me._ResponseTimeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._ResponseTimeLabel.Size = New System.Drawing.Size(51, 18)
        Me._ResponseTimeLabel.TabIndex = 49
        Me._ResponseTimeLabel.Text = "0.2500"
        '
        '_AcknowledgeButton
        '
        Me._AcknowledgeButton.BackColor = System.Drawing.SystemColors.Control
        Me._AcknowledgeButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._AcknowledgeButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AcknowledgeButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._AcknowledgeButton.Location = New System.Drawing.Point(8, 159)
        Me._AcknowledgeButton.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me._AcknowledgeButton.Name = "_AcknowledgeButton"
        Me._AcknowledgeButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._AcknowledgeButton.Size = New System.Drawing.Size(102, 39)
        Me._AcknowledgeButton.TabIndex = 50
        Me._AcknowledgeButton.Text = "&ACKNOWLEDGE"
        Me._ToolTip.SetToolTip(Me._AcknowledgeButton, "Must acknowledge (handshake) to move on.")
        Me._AcknowledgeButton.UseVisualStyleBackColor = False
        '
        '_EStopStateLabel
        '
        Me._EStopStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._EStopStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._EStopStateLabel.Location = New System.Drawing.Point(287, 7)
        Me._EStopStateLabel.Name = "_EStopStateLabel"
        Me._EStopStateLabel.Size = New System.Drawing.Size(75, 42)
        Me._EStopStateLabel.TabIndex = 38
        '
        '_RightPushButton
        '
        Me._RightPushButton.Appearance = System.Windows.Forms.Appearance.Button
        Me._RightPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me._RightPushButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._RightPushButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RightPushButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._RightPushButton.Location = New System.Drawing.Point(247, 206)
        Me._RightPushButton.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me._RightPushButton.Name = "_RightPushButton"
        Me._RightPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._RightPushButton.Size = New System.Drawing.Size(108, 52)
        Me._RightPushButton.TabIndex = 47
        Me._RightPushButton.Text = "&RIGHT"
        Me._RightPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._RightPushButton, "Check left and right buttons quickly to enable.")
        Me._RightPushButton.UseVisualStyleBackColor = False
        '
        '_LeftPushButton
        '
        Me._LeftPushButton.Appearance = System.Windows.Forms.Appearance.Button
        Me._LeftPushButton.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me._LeftPushButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._LeftPushButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._LeftPushButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._LeftPushButton.Location = New System.Drawing.Point(125, 206)
        Me._LeftPushButton.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me._LeftPushButton.Name = "_LeftPushButton"
        Me._LeftPushButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._LeftPushButton.Size = New System.Drawing.Size(108, 52)
        Me._LeftPushButton.TabIndex = 46
        Me._LeftPushButton.Text = "&LEFT"
        Me._LeftPushButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._LeftPushButton, "Check left and right buttons quickly to enable.")
        Me._LeftPushButton.UseVisualStyleBackColor = False
        '
        '_GoStateLabel
        '
        Me._GoStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._GoStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._GoStateLabel.Location = New System.Drawing.Point(201, 147)
        Me._GoStateLabel.Name = "_GoStateLabel"
        Me._GoStateLabel.Size = New System.Drawing.Size(75, 42)
        Me._GoStateLabel.TabIndex = 35
        '
        '_SetStateLabel
        '
        Me._SetStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._SetStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._SetStateLabel.Location = New System.Drawing.Point(281, 102)
        Me._SetStateLabel.Name = "_SetStateLabel"
        Me._SetStateLabel.Size = New System.Drawing.Size(80, 40)
        Me._SetStateLabel.TabIndex = 36
        '
        '_OffStateLabel
        '
        Me._OffStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._OffStateLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._OffStateLabel.Location = New System.Drawing.Point(201, 54)
        Me._OffStateLabel.Name = "_OffStateLabel"
        Me._OffStateLabel.Size = New System.Drawing.Size(75, 42)
        Me._OffStateLabel.TabIndex = 37
        '
        '_EStopStateLabelLabel
        '
        Me._EStopStateLabelLabel.AutoSize = True
        Me._EStopStateLabelLabel.BackColor = System.Drawing.Color.Transparent
        Me._EStopStateLabelLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._EStopStateLabelLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EStopStateLabelLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EStopStateLabelLabel.Location = New System.Drawing.Point(305, 21)
        Me._EStopStateLabelLabel.Name = "_EStopStateLabelLabel"
        Me._EStopStateLabelLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EStopStateLabelLabel.Size = New System.Drawing.Size(40, 14)
        Me._EStopStateLabelLabel.TabIndex = 40
        Me._EStopStateLabelLabel.Text = "ESTOP"
        Me._EStopStateLabelLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_CurrentStateLabel
        '
        Me._CurrentStateLabel.BackColor = System.Drawing.Color.Transparent
        Me._CurrentStateLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._CurrentStateLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CurrentStateLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._CurrentStateLabel.Location = New System.Drawing.Point(5, 6)
        Me._CurrentStateLabel.Name = "_CurrentStateLabel"
        Me._CurrentStateLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CurrentStateLabel.Size = New System.Drawing.Size(129, 27)
        Me._CurrentStateLabel.TabIndex = 39
        Me._CurrentStateLabel.Text = "<state>"
        Me._ToolTip.SetToolTip(Me._CurrentStateLabel, "Current State")
        '
        '_EStopButton
        '
        Me._EStopButton.BackColor = System.Drawing.Color.Red
        Me._EStopButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._EStopButton.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EStopButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._EStopButton.Location = New System.Drawing.Point(8, 204)
        Me._EStopButton.Margin = New System.Windows.Forms.Padding(3, 5, 3, 5)
        Me._EStopButton.Name = "_EStopButton"
        Me._EStopButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._EStopButton.Size = New System.Drawing.Size(100, 59)
        Me._EStopButton.TabIndex = 44
        Me._EStopButton.Text = "&E-STOP"
        Me._ToolTip.SetToolTip(Me._EStopButton, "Click to go to the EStop state")
        Me._EStopButton.UseVisualStyleBackColor = False
        '
        'BimanualToggle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "BimanualToggle"
        Me.Size = New System.Drawing.Size(406, 447)
        Me._Layout.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _ErrorLable As Windows.Forms.Label
    Private WithEvents _InfoLabel As Windows.Forms.Label
    Private WithEvents _ToolTip As Windows.Forms.ToolTip
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents Panel1 As Windows.Forms.Panel
    Private WithEvents _SetStateLabelLabel As Windows.Forms.Label
    Private WithEvents _OffStateLabelLabel As Windows.Forms.Label
    Private WithEvents _ResponseTimeLabelLabel As Windows.Forms.Label
    Private WithEvents _OnStateLabelLabel As Windows.Forms.Label
    Private WithEvents _GoStateLabeLabel As Windows.Forms.Label
    Private WithEvents _ResponseTimeLabel As Windows.Forms.Label
    Private WithEvents _AcknowledgeButton As Windows.Forms.Button
    Private WithEvents _RightPushButton As Windows.Forms.CheckBox
    Private WithEvents _LeftPushButton As Windows.Forms.CheckBox
    Private WithEvents _GoStateLabel As Windows.Forms.Label
    Private WithEvents _SetStateLabel As Windows.Forms.Label
    Private WithEvents _OffStateLabel As Windows.Forms.Label
    Private WithEvents _EStopStateLabelLabel As Windows.Forms.Label
    Private WithEvents _CurrentStateLabel As Windows.Forms.Label
    Private WithEvents _EStopButton As Windows.Forms.Button
    Private WithEvents _OnStateLabel As Windows.Forms.Label
    Private WithEvents _EStopStateLabel As Windows.Forms.Label
End Class
