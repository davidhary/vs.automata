Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Stateless
Imports isr.Automata.Finite.Engines
Imports isr.Automata.Finite.Forms.ExceptionExtensions

''' <summary> A bi-manual toggle switch implementation. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 9/14/2016 </para>
''' </remarks>
Public Class BimanualToggle
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        'This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        ' Create a new state machine
        Me.Engine = New isr.Automata.Finite.Engines.BimanualToggleEngine("Bi-Manual Toggle") With {
            .ResponseTimeout = TimeSpan.FromSeconds(0.25)
        }
        Me._ResponseTimeLabel.Text = Me.Engine.ResponseTimeout.TotalSeconds.ToString("#.00#\s", Globalization.CultureInfo.CurrentCulture)
        Windows.Forms.Application.DoEvents()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-08-19. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
                If Me._Engine IsNot Nothing Then Me._Engine.Dispose() : Me._Engine = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Control.KeyPress" /> event. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="e"> A <see cref="T:System.Windows.Forms.KeyPressEventArgs" /> that contains the
    '''                  event data. </param>
    Protected Overrides Sub OnKeyPress(e As KeyPressEventArgs)
        Dim keyAscii As Integer = Asc(e.KeyChar)
        Select Case keyAscii

            Case System.Windows.Forms.Keys.A, System.Windows.Forms.Keys.A + 32

                System.Windows.Forms.SendKeys.Send("%A")

            Case System.Windows.Forms.Keys.E, System.Windows.Forms.Keys.E + 32

                System.Windows.Forms.SendKeys.Send("%E")

            Case System.Windows.Forms.Keys.L, System.Windows.Forms.Keys.L + 32

                System.Windows.Forms.SendKeys.Send("%L")

            Case System.Windows.Forms.Keys.R, System.Windows.Forms.Keys.R + 32

                System.Windows.Forms.SendKeys.Send("%R")

            Case System.Windows.Forms.Keys.S, System.Windows.Forms.Keys.S + 32

                System.Windows.Forms.SendKeys.Send("%S")

            Case System.Windows.Forms.Keys.S, System.Windows.Forms.Keys.O + 32

                System.Windows.Forms.SendKeys.Send("%O")

            Case System.Windows.Forms.Keys.Y, System.Windows.Forms.Keys.Y + 32

                System.Windows.Forms.SendKeys.Send("%Y")

        End Select

        e.KeyChar = Chr(keyAscii)
        If keyAscii = 0 Then e.Handled = True

        MyBase.OnKeyPress(e)
    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            Me.Engine.StateMachine.Activate()
        Catch
        Finally
            MyBase.OnLoad(e)
        End Try

    End Sub

    ''' <summary> Executes the 'form shown' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Private Sub OnFormShown()

        ' Set the caption to the Application title
        Me.Text = Core.ApplicationInfo.BuildApplicationTitleCaption(Me.Engine.Name)

        ' allow form rendering time to complete: process all messages currently in the queue.
        Windows.Forms.Application.DoEvents()

    End Sub


#End Region

#Region " MACHINE BEHAVIOR "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary>
    ''' Instance of the State Machine class.
    ''' </summary>
    Private WithEvents _Engine As isr.Automata.Finite.Engines.BimanualToggleEngine
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the engine. </summary>
    ''' <value> The engine. </value>
    Private Property Engine As isr.Automata.Finite.Engines.BimanualToggleEngine
        Get
            Return Me._Engine
        End Get
        Set(value As isr.Automata.Finite.Engines.BimanualToggleEngine)
            If Me._Engine IsNot Nothing Then
                RemoveHandler Me._Engine.PropertyChanged, AddressOf Me.BimanualEnabler_PropertyChanged
            End If
            Me._Engine = value
            If value Is Nothing Then
            Else
                AddHandler Me._Engine.PropertyChanged, AddressOf Me.BimanualEnabler_PropertyChanged
                Dim stateConfiguration As StateMachine(Of BimanualToggleState, BimanualToggleTrigger).StateConfiguration
                stateConfiguration = Me._Engine.StateMachine.Configure(BimanualToggleState.Off)
                stateConfiguration.OnEntry(Sub(t) Me.OnEntry(t))
                stateConfiguration.OnActivate(Sub() Me.OnOffActivate())
            End If
        End Set
    End Property

    ''' <summary> Executes the 'property changed' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As BimanualToggleEngine, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Engines.BimanualToggleEngine.LastTransition)
                Me._CurrentStateLabel.Text = sender.LastTransition?.Destination.ToString
                If sender.LastTransition IsNot Nothing Then
                    Me.SetStateColor(sender.LastTransition.Source, SystemColors.InactiveBorder)
                    Me.SetStateColor(sender.LastTransition.Destination, SystemColors.ActiveBorder)
                End If
        End Select
    End Sub

    ''' <summary> Platform property changed. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BimanualEnabler_PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.BimanualEnabler_PropertyChanged), New Object() {sender, e})
        Else
            Dim automaton As BimanualToggleEngine = TryCast(sender, BimanualToggleEngine)
            If automaton Is Nothing OrElse e Is Nothing Then Return
            Dim activity As String = $"handling {automaton.Name}.{e.PropertyName} change"
            Try
                Me.OnPropertyChanged(automaton, e.PropertyName)
            Catch ex As Exception
                Me._ErrorLable.Text = $"{Me.Name} exception {activity} at {automaton.Name}.{automaton.LastTransition?.Destination} state;. {ex.ToFullBlownString}"

            End Try
        End If
    End Sub

    ''' <summary> Executes the 'off activate' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    Private Sub OnOffActivate()
        Me._LeftPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
        Me._RightPushButton.CheckState = System.Windows.Forms.CheckState.Unchecked
        Me.SetStateColor(BimanualToggleState.Off, SystemColors.ActiveBorder)
    End Sub

    ''' <summary> Executes the 'entry' action. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="transition"> The transition. </param>
    Private Sub OnEntry(ByVal transition As StateMachine(Of BimanualToggleState, BimanualToggleTrigger).Transition)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of StateMachine(Of BimanualToggleState, BimanualToggleTrigger).Transition)(AddressOf Me.OnEntry), New Object() {transition})
        ElseIf transition IsNot Nothing Then
            If transition.Destination = BimanualToggleState.Off Then
                Me.OnOffActivate()
            ElseIf transition.Destination = BimanualToggleState.Set Then
            End If
        End If
    End Sub

#End Region

#Region " MACHINE APPERARANCE "

    ''' <summary> Gets state color. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="stateId"> Identifier for the state. </param>
    ''' <returns> The state color. </returns>
    Private Function GetStateColor(ByVal stateId As BimanualToggleState) As Color
        Return Me.StateShape(stateId).BackColor
    End Function

    ''' <summary> Sets state color. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="stateId"> Identifier for the state. </param>
    ''' <param name="value">   The value. </param>
    Private Sub SetStateColor(ByVal stateId As BimanualToggleState, ByVal value As Color)
        Me.StateShape(stateId).BackColor = value
        Me.StateLabel(stateId).BackColor = value
    End Sub

    ''' <summary> Selects the state shape. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="stateId"> Identifier for the state. </param>
    ''' <returns> A Windows.Forms.Label. </returns>
    Private Function StateLabel(ByVal stateId As BimanualToggleState) As Windows.Forms.Label
        Select Case stateId
            Case BimanualToggleState.On
                Return Me._OnStateLabelLabel
            Case BimanualToggleState.Estop
                Return Me._EStopStateLabelLabel
            Case BimanualToggleState.Off
                Return Me._OffStateLabelLabel
            Case BimanualToggleState.Set
                Return Me._SetStateLabelLabel
            Case BimanualToggleState.Go
                Return Me._GoStateLabeLabel
            Case Else
                Debug.Assert(False)
                Return Me._OffStateLabelLabel
        End Select
    End Function

    ''' <summary> Selects the state shape. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="stateId"> Identifier for the state. </param>
    ''' <returns> A Windows.Forms.Label. </returns>
    Private Function StateShape(ByVal stateId As BimanualToggleState) As Windows.Forms.Label
        Select Case stateId
            Case BimanualToggleState.On
                Return Me._OnStateLabel
            Case BimanualToggleState.Estop
                Return Me._EStopStateLabel
            Case BimanualToggleState.Off
                Return Me._OffStateLabel
            Case BimanualToggleState.Set
                Return Me._SetStateLabel
            Case BimanualToggleState.Go
                Return Me._GoStateLabel
            Case Else
                Debug.Assert(False)
                Return Me._OffStateLabel
        End Select
    End Function

#End Region

#Region " CONTROL EVENTS HANDLERS "

    ''' <summary> Sets the acknowledge input. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="eventSender"> . </param>
    ''' <param name="eventArgs">   . </param>
    Private Sub AcknowledgeButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _AcknowledgeButton.Click

        If Me._AcknowledgeButton.Enabled AndAlso Me.Engine IsNot Nothing Then
            ' Acknowledge receipt of the go signal.
            Me.Engine.Acknowledge()
        End If

    End Sub

    ''' <summary> Sends an EStop signal. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="eventSender"> . </param>
    ''' <param name="eventArgs">   . </param>
    Private Sub EStopButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _EStopButton.Click
        If Me._EStopButton.Enabled AndAlso Me.Engine IsNot Nothing Then
            ' Send the state machine to its idle state
            Me.Engine.EstopPressed()
        End If
    End Sub

    ''' <summary> Inputs the left button state to the state machine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="eventSender"> . </param>
    ''' <param name="eventArgs">   . </param>
    Private Sub PushButton_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _LeftPushButton.Click, _RightPushButton.Click
        If Me.InitializingComponents Then Return
        Dim button As CheckBox = CType(eventSender, CheckBox)
        If button Is Nothing Then Return
        If button.Checked Then
            ' calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonPressed))
            thread.Start()

            button.BackColor = SystemColors.GradientActiveCaption
        Else
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonReleased))
            thread.Start()
            button.BackColor = SystemColors.GradientInactiveCaption
        End If
    End Sub

    ''' <summary> Inputs the left button state to the state machine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="eventSender"> . </param>
    ''' <param name="eventArgs">   . </param>
    Private Sub LeftPushButton_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If Me.InitializingComponents Then Return
        If Me._LeftPushButton.Checked Then
            ' calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonPressed))
            thread.Start()
            Me._LeftPushButton.BackColor = SystemColors.GradientActiveCaption


        Else
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonReleased))
            thread.Start()
            Me._LeftPushButton.BackColor = SystemColors.GradientInactiveCaption
        End If
    End Sub

    ''' <summary> Inputs the right button state to the state machine. </summary>
    ''' <remarks> David, 2020-10-01. </remarks>
    ''' <param name="eventSender"> . </param>
    ''' <param name="eventArgs">   . </param>
    Private Sub RightPushButton_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        If Me.InitializingComponents Then Return
        If Me._RightPushButton.Checked Then
            ' calls on a thread are required otherwise the buttons are not released in timeout because the ready enter occurs within the check state change event.
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonPressed))
            thread.Start()

            Me._RightPushButton.BackColor = SystemColors.GradientActiveCaption
        Else
            Dim thread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.Engine.ButtonReleased))
            thread.Start()
            Me._RightPushButton.BackColor = SystemColors.GradientInactiveCaption
        End If
    End Sub

#End Region

End Class
